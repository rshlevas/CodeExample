<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\SellLink::class, function (Faker $faker) {
    return [
        'product_profile_id' => 1,
        'link' => $faker->url,
    ];
});
