<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\PaymentTerm::class, function (Faker $faker) {
    return [
        'name' => 'Same Day'
    ];
});
