<?php

use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->word,
        'model' => $faker->word,
        'check_name' => $faker->unique()->word,
        'url_name' => $faker->unique()->word,
        'brand_id' => 1,
    ];
});
