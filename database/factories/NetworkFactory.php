<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Network::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'alias' => $faker->word,
        'lang' => 'en',
        'key' => 1,
    ];
});
