<?php

use App\Models\Site;
use Faker\Generator as Faker;

$factory->define(Site::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'parsing_name' => $faker->word,
        'path' => $faker->url,
        'api_link' => $faker->url,
        'group_id' => 1,
    ];
});
