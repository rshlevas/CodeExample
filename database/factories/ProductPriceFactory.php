<?php
use App\Models\ProductPrice;
use Faker\Generator as Faker;

$factory->define(ProductPrice::class, function (Faker $faker) {
    return [
        'product_profile_id' => $faker->numberBetween(1, 50),
        'price' => $faker->numberBetween(5, 200),
        'created_at' => new \Carbon\Carbon(),
        'currency' => 'GBP',
    ];
});
