<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->string('parsing_name')->index()->nullable();
            $table->string('url_name')->index()->unique();
            $table->string('path');
            $table->longText('api_link')->nullable();
            $table->integer('is_returnable')->default(0);
            $table->integer('is_free_post')->default(0);
            $table->decimal('trust_point')->nullable();
            $table->string('trustpilot_link')->nullable();
            $table->integer('is_active')->default(1);
            $table->integer('payment_term_id')->unsigned()->nullable();
            $table->foreign('payment_term_id')->references('id')->on('payment_term')->onDelete('cascade');
            $table->integer('group_id')->unsigned()->nullable();
            $table->foreign('group_id')->references('id')->on('group')->onDelete('cascade');
            $table->date('last_parsing')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites');
    }


}
