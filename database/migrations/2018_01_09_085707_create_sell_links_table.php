<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sell_links', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_profile_id')->unsigned();
            $table->foreign('product_profile_id')->references('id')->on('product_profile')->onDelete('cascade');
            $table->string('link');
            $table->integer('count')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sell_links');
    }
}
