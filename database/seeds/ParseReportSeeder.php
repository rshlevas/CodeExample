<?php

use Illuminate\Database\Seeder;

class ParseReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dates = [
          \Carbon\Carbon::today(),
          \Carbon\Carbon::tomorrow(),
          \Carbon\Carbon::yesterday(),
        ];

        foreach ($dates as $date) {
            for ($i = 1; $i< 6; $i++) {
                factory(\App\Models\NegativeReport::class)->create([
                    'site_id' => $i,
                    'created_at' => $date,
                ]);
                factory(\App\Models\PositiveReport::class)->create([
                    'site_id' => $i,
                    'created_at' => $date,
                ]);
            }
        }
    }
}
