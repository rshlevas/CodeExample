<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = ['SuperAdmin', 'Admin', 'Editor'];
        foreach ($roles as $role) {
            factory(\App\Models\Role::class)->create(['name' => $role]);
        }

    }
}
