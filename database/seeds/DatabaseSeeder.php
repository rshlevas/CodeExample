<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mainSeeders = [
            RoleSeeder::class,
            UserSeeder::class,
            PaymentSeeder::class,
            PaymentTermSeeder::class,
            GroupWithSitesSeeder::class,
            CategorySeeder::class,
            ConditionSeeder::class,
            NetworkSeeder::class,
            QuizSeeder::class,
        ];

        $testSeeders = [
            BrandSeeder::class,
            SiteSeeder::class,
            ProductProfileSeeder::class,
            PricesSeeder::class,
            ParseReportSeeder::class,
        ];

        if (env('APP_ENV') === 'testing') {
            $mainSeeders = array_merge($mainSeeders, $testSeeders);
        }

        $this->call($mainSeeders);
    }
}
