<?php

use Illuminate\Database\Seeder;

class NetworkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $networks = config('filter.seeding.network');
        foreach ($networks as $key => $condition) {
            factory(\App\Models\Network::class)->create([
                'name' => $condition['name'],
                'alias' => $condition['alias'],
                'key' => $key,
                'lang' => 'en',
                'order' => $key,
            ]);
        }

    }
}
