<?php

use Illuminate\Database\Seeder;

class ProductProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 4; $i++) {
            for ($j = 1; $j < 16; $j++) {
                factory(\App\Models\ProductProfile::class)->create([
                    'site_id' => $i,
                    'product_id' => $j,
                ]);
            }
        }
    }
}
