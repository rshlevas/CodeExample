<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            ['name' => 'Phone', 'url_name' => 'phone'],
            ['name' => 'Tablets', 'url_name' => 'tablets'],
            ['name' => 'Watches', 'url_name' => 'watches'],
        ];

        foreach ($categories as $category) {
            factory(\App\Models\Category::class)->create($category);
        }
    }
}
