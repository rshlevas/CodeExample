<?php

use Illuminate\Database\Seeder;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = ['Bank Transfer', 'PayPal', 'Cheque'];
        foreach ($names as $name) {
            factory(\App\Models\Payment::class)->create(['name' => $name]);
        }
    }
}
