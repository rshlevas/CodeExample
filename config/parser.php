<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Queue Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of queues you want parsers to be pushed in.
    |
    */
    'queue' => 'parsing',

    /*
    |--------------------------------------------------------------------------
    | Default Job Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which job should be used for pushing parsers
    | into queue.
    |
    */
    'job' => \App\Jobs\ParseJob::class,

    /*
    |--------------------------------------------------------------------------
    | Site Name - Parser Class Relations
    |--------------------------------------------------------------------------
    |
    | Here you may specify which parser class should be used for site. You should
    | use correct site name, that described at the database, and full parser class
    | name with all namespaces:
    | 'parsers' => [
    |     'site_name' => '\Some\Name\Space\ExampleParser::class'
    ]
    |
    */
    'parsers' => [
        'mobile_phone_xchange' => \App\Parsers\JSONParser\CustomParsers\MobilePhoneXchangeParser::class,
        'fonebank' => \App\Parsers\JSONParser\CustomParsers\FonebankParser::class,
        'tesco_mobile' => \App\Parsers\JSONParser\CustomParsers\TescoMobileParser::class,
        'top_dollar_mobile' => \App\Parsers\JSONParser\CustomParsers\TopDollarMobileParser::class,
        'macback' => \App\Parsers\JSONParser\CustomParsers\MackbackParser::class,
        'giffgaff' => \App\Parsers\JSONParser\CustomParsers\GiffgaffParser::class,
        'vodafone' => \App\Parsers\JSONParser\CustomParsers\VodafoneParser::class,
        'carphone_warehouse' => \App\Parsers\JSONParser\CustomParsers\CarphoneWarehouseParser::class,
        'gecko_mobile' => \App\Parsers\JSONParser\CustomParsers\GeckoMobileParser::class,
        'music_magpie' => \App\Parsers\JSONParser\CustomParsers\MusicMagpieParser::class,
        'o2_recycle' => \App\Parsers\CSVParser\AWINParser\O2RecycleParser::class,
        'simply_drop' => \App\Parsers\CSVParser\WebgainsParser\SimplyDropParser::class,
        'mobile_cash_mate' => \App\Parsers\CSVParser\WebgainsParser\MobileCashParser::class,
        'ee_recycle' => \App\Parsers\CSVParser\WebgainsParser\RecycleParser::class,
        '8_mobile' => \App\Parsers\CSVParser\WebgainsParser\EightMobileParser::class,
        'envirofone' => \App\Parsers\CSVParser\WebgainsParser\EnvirofoneParser::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Services Configs
    |--------------------------------------------------------------------------
    |
    | Here you may specify the configs of services related to the parsing process
    |
    |
    */
    'services' => [
        /*
        |--------------------------------------------------------------------------
        | ModelService Configs
        |--------------------------------------------------------------------------
        |
        | Here you may specify the logic of model service. Here you can display the
        | order of Entities with their aliases, at which they should be checked and
        | then added at DB. The structure of this section should be such:
        |    \Some\Name\Space\ModelService::class => [
        |           'alias' => \Some\Name\Space\Model::class,
        |        ],
        ]
        */
        \App\Parsers\Services\ModelService::class => [
            'brand' => \App\Models\Brand::class,
            'product' => \App\Models\Product::class,
            'productProfile' => \App\Models\ProductProfile::class,
            'price' => \App\Models\ProductPrice::class,
            'link' => \App\Models\SellLink::class,
        ],
    ],
];