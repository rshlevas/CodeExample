<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Image size
    |--------------------------------------------------------------------------
    |
    | Here you may specify the images size. Two types of images are available:
    | general and thumb. You should declare width and height in pixels for both types
    |
    */
    'general' => [
        'width' => 280,
        'height' => 400
    ],

    'thumb' => [
        'width' => 70,
        'height' => 100
    ],
];