<?php
return [

    /*
    |_________________________________________________________________________________________________________________
    |
    |                           IMEI.info API
    |_________________________________________________________________________________________________________________
    |
    | Here specified the connection properties of imei.info api. Values should be specified at .env file
    |
    */

    /*
     |------------------------------
     | www.imei.info api key
     |------------------------------
     */
    'key' => env('IMEI_API_KEY', false),

    /*
     |------------------------------
     | www.imei.info api link
     |------------------------------
     */
    'link' => env('IMEI_API_LINK', 'http://www.imei.info/api/checkimei/'),

];