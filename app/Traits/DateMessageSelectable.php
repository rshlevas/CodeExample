<?php

namespace App\Traits;


use Carbon\Carbon;

trait DateMessageSelectable
{
    /**
     * @param Carbon $date
     * @return string
     */
    protected function selectDatePhrase(Carbon $date)
    {
        $currentDate = Carbon::now();

        return $this->selectPhraseByHours($currentDate->diffInHours($date), $date);
    }

    /**
     * @param $hours
     * @param $date
     * @return string
     */
    protected function selectPhraseByHours($hours, $date)
    {
        if($hours < 1) {
            return 'a few minutes ago';
        } elseif($hours === 1) {
            return 'an hour ago';
        } elseif($hours > 1 && $hours < 24) {
            return sprintf('%d hours age', $hours);
        } elseif($hours >= 24 && $hours < 144) {
            return sprintf('%d days ago', $hours/24 + 1);
        }

        return $date->toFormattedDateString();
    }
}