<?php

namespace App\Traits;

trait ResourceChecker
{
    /**
     * Checks if source exist
     *
     * @param $url
     * @return bool
     */
    protected function isSourceExist($url)
    {
        $headers = @get_headers($url, 1);

        return stripos($headers[0],"200 OK") ? true : false;
    }
}