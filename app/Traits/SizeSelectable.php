<?php

namespace App\Traits;

use App\Models\Product;

trait SizeSelectable
{
    /**
     * Takes sizes from config file
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    protected function getBaseSizes()
    {
        return config('filter.sizes.display');
    }

    /**
     * Return array with sizes of current product and related products
     *
     * @param Product $product
     * @return array|mixed
     */
    protected function getSizes(Product $product)
    {
        return $product->size ? $this->selectSizes($product) : [];
    }

    /**
     * Select necessary sizes an push it into array
     *
     * @param Product $product
     * @return array|mixed
     */
    protected function selectSizes(Product $product)
    {
        $filteredSizes = [];
        $filteredSizes = $this->setSize($product, $filteredSizes);
        foreach ($product->sizeRelated as $related)
        {
            $filteredSizes = $this->setSize($related, $filteredSizes);
        }
        ksort($filteredSizes, SORT_NATURAL);

        return $filteredSizes;
    }

    /**
     * Push size into array
     *
     * @param $product
     * @param $filteredSizes
     * @return mixed
     */
    protected function setSize($product, $filteredSizes)
    {
        $sizes = $this->getBaseSizes();

        if (in_array($product->size, $sizes)) {
            $filteredSizes[$product->size] = $product->url_name;
        }

        return $filteredSizes;
    }
}