<?php

namespace App\Traits;

use Illuminate\Support\Collection;
use App\Services\ProductAttributesPreparator;

trait ProductPreparable
{
    use ProductSortable;

    protected function prepareProducts(Collection $products, ProductAttributesPreparator $service)
    {
        return $this->sortByPriceDesc($service->prepareCollection($products));
    }
}