<?php

namespace App\Observers;

use App\Mail\FailedParsing;
use App\Models\NegativeReport;
use App\User;
use Illuminate\Support\Facades\Mail;

class NegativeReportObserver
{
    /**
     * Listening for NegativeReport creating
     *
     * @param NegativeReport $report
     */
    public function created(NegativeReport $report)
    {
        $users = User::all();

        foreach ($users as $user) {
            Mail::to($user->email)->send(new FailedParsing($report));
        }
    }
}