<?php

namespace App\Observers;


use App\Models\Product;
use App\Services\ProductSizeRelationSetter;

class ProductObserver
{
    /**
     * @var ProductSizeRelationSetter
     */
    protected $setter;

    /**
     * ProductObserver constructor.
     * @param ProductSizeRelationSetter $setter
     */
    public function __construct(ProductSizeRelationSetter $setter)
    {
        $this->setter = $setter;
    }

    /**
     * Listening for Product events
     *
     * @param Product $product
     */
    public function created(Product $product)
    {
        if ($product->size) {
            $this->setter->set($product);
        }
    }
}