<?php

namespace App\Observers;

use App\Mail\FailedParsing;
use App\Models\Image;
use App\Models\NegativeReport;
use App\User;
use Illuminate\Support\Facades\Mail;

class ImageObserver
{
    /**
     * Listening for Image creating
     *
     * @param Image $image
     */
    public function created(Image $image)
    {
        $image->CDN_path = sprintf('%s/%s', env('MAXCDN_LINK'), $image->path);
        $image->save();
    }
}