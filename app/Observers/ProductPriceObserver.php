<?php

namespace App\Observers;

use App\Models\ProductPrice;
use App\Services\BestMonthPriceSetter;
use Carbon\Carbon;

class ProductPriceObserver
{
    /**
     * @var BestMonthPriceSetter
     */
    protected $monthBestPriceSetter;

    /**
     * ProductPriceObserver constructor.
     * @param BestMonthPriceSetter $service
     */
    public function __construct(BestMonthPriceSetter $service)
    {
        $this->monthBestPriceSetter = $service;
    }

    /**
     * Listening for ProductPrice events
     *
     * @param ProductPrice $price
     */
    public function created(ProductPrice $price)
    {
        $updated = ['updated_at' => Carbon::now()];
        $price->productProfile->product->update($updated);
        $price->productProfile->product->brand->update($updated);
        $this->monthBestPriceSetter->set($price);
    }

}