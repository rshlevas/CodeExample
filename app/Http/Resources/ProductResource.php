<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Lang;

class ProductResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->getResource($request);
    }

    /**
     * Forms resource structure
     *
     * @param $request
     * @return array
     */
    protected function getResource($request)
    {
        return [
            'url' => $this->getRoute($request),
            'name' => $this->short_name,
            'title' => $this->name,
            'price' => $this->best_price->price,
            'currency' => strtolower($this->best_price->currency),
            'image' => asset($this->getImagePath()),
            'compare' => $this->getCompare()
        ];
    }

    /**
     * Returns product view route depending on the section
     *
     * @param $request
     * @return string
     */
    protected function getRoute($request)
    {
        return $request->route()->getPrefix() === '/admin' ?
            route('admin_product_view', ['product' => $this->url_name]) :
            route('product_view', [
                'brand' => $this->brand->url_name,
                'category' => $this->category->url_name,
                'product' => $this->url_name
            ]);
    }

    /**
     * Get image path
     *
     * @return string
     */
    protected function getImagePath()
    {
        if ($this->imageThumbnail) {
            return $this->imageThumbnail->source;
        } elseif ($this->image) {
            return $this->image->source;
        } else {
            return 'images/no-image.jpeg';
        }
    }


    /**
     * Get compare phrase according to profiles number
     *
     * @return mixed
     */
    protected function getCompare()
    {
        return $this->profiles_count > 1 ? Lang::get('products.compare.prices.several',
            ['number' => $this->profiles_count
            ]) : Lang::get('products.compare.prices.single');
    }
}
