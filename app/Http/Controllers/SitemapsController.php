<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Product;
use App\Models\Site;
use Watson\Sitemap\Facades\Sitemap;

class SitemapsController extends Controller
{
    /**
     * General Sitemaps file
     *
     * @return mixed
     */
    public function index()
    {
        Sitemap::addSitemap(route('sitemaps_products'));
        Sitemap::addSitemap(route('sitemaps_brands'));
        Sitemap::addSitemap(route('sitemaps_recyclers'));

        return Sitemap::index();
    }

    /**
     * SiteMaps for products
     *
     * @return mixed
     */
    public function products()
    {
        $products = Product::with(['brand', 'category'])->where('parent_id', null)->get();

        foreach ($products as $product) {
            $tag = Sitemap::addTag(
                route('product_view', [
                    'brand' => $product->brand->url_name,
                    'category' => $product->category->url_name,
                    'product' => $product->url_name
                ]),
                $product->updated_at,
                'daily',
                '1.0'
            );
        }

        return Sitemap::render();
    }

    /**
     * SiteMaps for brands
     *
     * @return mixed
     */
    public function brands()
    {
        $brands = Brand::all();

        foreach ($brands as $brand) {
            $tag = Sitemap::addTag(route('brand_view', $brand->url_name), $brand->updated_at, 'daily', '0.5');
        }

        return Sitemap::render();
    }

    /**
     * SiteMaps for recyclers
     *
     * @return mixed
     */
    public function sites()
    {
        $sites = Site::where('is_active', 1)->get();

        foreach ($sites as $site) {
            $tag = Sitemap::addTag(route('recyclers_view', $site->url_name), $site->updated_at, 'daily', '0.5');
        }

        return Sitemap::render();
    }
}
