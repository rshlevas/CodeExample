<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Product;
use App\Models\Site;
use App\Services\ProductEntityService;
use App\Services\ProductAttributesPreparator;
use App\Services\ProductSizePreparator;
use App\Traits\Paginable;
use App\Traits\ProductPreparable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    use ProductPreparable, Paginable;

    /**
     * @var ProductEntityService
     */
    protected $productManager;

    /**
     * @var ProductAttributesPreparator
     */
    protected $productPreparator;

    public function __construct(ProductEntityService $service, ProductAttributesPreparator $preparator)
    {
        $this->productManager = $service;
        $this->productPreparator = $preparator;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::with('image')
            ->take(18)->get();

        $sites = Site::with('image')
            ->take(12)->get();

        $bestPricesProductsIds = DB::table('product_prices as pr')
            ->select('p.id')
            ->join('product_profile as pp', 'pr.product_profile_id', '=', 'pp.id')
            ->join('products as p', 'pp.product_id', '=', 'p.id')
            ->where(['pr.condition' => 1, 'pr.network' => 1, 'p.parent_id' => null])
            ->orderBy('pr.price', 'desc')
            ->limit(100)
            ->get()
            ->unique();

        $bestPricesProducts = $this->productManager->getProductsByIds(
            array_slice(
                $bestPricesProductsIds->map(function ($pr) {
                    return $pr->id;
                })->toArray(),
                0,
                24
            )
        );

        $mostPopular = $this->productManager->getProductByParam('is_popular', 1, 5);
        $iphones = $this->productManager->getProductsLike('iphone', 5);
        $samsung = $this->productManager->getProductsLike('galaxy', 5);

        return view('home', [
            'sites' => $sites,
            'brands' => $brands,
            'bestPrices' => $this->prepareProducts($bestPricesProducts, $this->productPreparator)->chunk(8),
            'mostPopular' => $this->prepareProducts($mostPopular, $this->productPreparator),
            'iphones' => $this->prepareProducts($iphones, $this->productPreparator),
            'samsung' => $this->prepareProducts($samsung, $this->productPreparator),
        ]);
    }

    /**
     * Show the most popular products
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewPopular(Request $request)
    {
        $mostPopular = $this->productManager->getProductByParam('is_popular', 1);
        $paginated = $this->paginate(
            $this->prepareProducts($mostPopular, $this->productPreparator),
            $request,
            20
        );

        return view('site.most_popular', [
           'products' => $paginated,
        ]);
    }

    /**
     * Show the key products products
     *
     * @param Request $request
     * @param ProductSizePreparator $preparator
     * @param $keyPhone
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function viewKeyProducts(Request $request, ProductSizePreparator $preparator, $keyPhone)
    {
        $products = $this->productManager->getProductsLike($keyPhone);

        $paginated = $this->paginate(
            $preparator->prepare($products),
            $request
        );

        return view('site.key_products', [
            'products' => $paginated,
            'keyPhone' => $keyPhone,
        ]);
    }

    /**
     * Show the products with the best prices
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewBestPrices()
    {
        $bestPricesProductsIds = DB::table('product_prices as pr')
            ->select('p.id')
            ->join('product_profile as pp', 'pr.product_profile_id', '=', 'pp.id')
            ->join('products as p', 'pp.product_id', '=', 'p.id')
            ->where(['pr.condition' => 1, 'pr.network' => 1, 'p.parent_id' => null])
            ->orderBy('pr.price', 'desc')
            ->limit(100)
            ->get()
            ->unique();

        $bestPricesProducts = $this->productManager->getProductsByIds(
            array_slice(
                $bestPricesProductsIds->map(function ($pr) {
                    return $pr->id;
                })->toArray(),
                0,
                24
            )
        );

        return view('site.best_prices', [
            'products' => $this->prepareProducts($bestPricesProducts, $this->productPreparator),
        ]);
    }

    public function viewAbout()
    {
        return view('site.about');
    }
}
