<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Product;
use App\Services\ProductAttributesPreparator;
use App\Services\ProductEntityService;
use App\Services\ProductSizePreparator;
use App\Traits\Paginable;
use App\Traits\ProductPreparable;
use App\Traits\ProductSortable;
use Corcel\Model\Post;
use function foo\func;
use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;

class BrandController extends Controller
{
    use Paginable, ProductPreparable;

    /**
     * @var ProductEntityService
     */
    protected $productManager;

    /**
     * @var ProductAttributesPreparator
     */
    protected $productPreparator;

    /**
     * BrandController constructor.
     * @param ProductEntityService $service
     * @param ProductAttributesPreparator $preparator
     */
    public function __construct(ProductEntityService $service, ProductAttributesPreparator $preparator)
    {
        $this->productManager = $service;
        $this->productPreparator = $preparator;
    }

    /**
     * Brands list
     */
    public function index()
    {
        $names = ['Apple', 'Google', 'HTC', 'Samsung', 'Sony'];
        $keyBrands = Brand::whereIn('name', $names)->with('image', 'products')->get();
        $otherBrands = Brand::whereNotIn('name', $names)->with('image', 'products')->orderBy('name')->get();

        return view('brands.index', ['brands' =>$keyBrands->merge($otherBrands)]);
    }

    /**
     * Single brand product view
     *
     * @param Request $request
     * @param ProductSizePreparator $preparator
     * @param Brand $brand
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Request $request, ProductSizePreparator $preparator, Brand $brand)
    {
        $products = $this->productManager->getProductByParam('brand_id', $brand->id);

        $paginated = $this->paginate(
            $preparator->prepare($products),
            $request);

        return view('brands.view', [
            'brand' => $brand,
            'products' => $paginated,
        ]);
    }
}
