<?php

namespace App\Http\Controllers;

use App\Models\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class RecyclerController extends Controller
{
    /**
     * Display recyclers list
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $sites = Site::with('image', 'payments', 'paymentTerm', 'reviews')
            ->where('is_active',1)
            ->get();

        return view('recyclers.index', ['sites' => $sites]);
    }

    /**
     * Display recycler view
     *
     * @param Site $site
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Site $site)
    {
        return view('recyclers.view', ['site' => $site]);
    }
}
