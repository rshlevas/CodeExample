<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use App\Services\QuizServices\QuizService;
use Illuminate\Support\Facades\App;

class QuizController extends Controller
{
    /**
     * Starts the quiz
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        session()->forget('quiz-points');
        session()->forget('quiz-result');

        $question = Question::where('lang', App::getLocale())
            ->orderBy('order')
            ->get()
            ->first();
        if (! $question) {
            return redirect()->route('home');
        }

        return view('quiz.index', [
            'question' => $question,
            'step' => 1,
        ]);
    }

    /**
     * switch the questions and render result if it was the last question
     *
     * @param QuizService $quizService
     * @param Answer $answer
     * @param $step
     * @return \Illuminate\Http\JsonResponse
     */
    public function nextQuestion(QuizService $quizService, Answer $answer, $step)
    {
        session(['quiz-points' => session('quiz-points') + $answer->weight]);
        session(['quiz-step' => $step]);
        $questions = Question::where('lang', App::getLocale())
            ->orderBy('order')
            ->get();
        if ($step < count($questions)) {
           $html = view('quiz.question_content', [
               'question' => $questions[$step],
               'step' => $step + 1,
           ])->render();

           return response()->json(['html' => $html]);
        }

        $result = $quizService->getResult();
        session(['quiz-result' => $result]);
        $html = view('quiz.result', [
            'condition' => $result
        ])->render();

        return response()->json(['html' => $html]);
    }
}
