<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Services\ProductAttributesPreparator;
use App\Services\ProductEntityService;
use App\Services\SearchService;
use App\Traits\Paginable;
use App\Traits\ProductPreparable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class SearchController extends Controller
{
    use Paginable, ProductPreparable;

    /**
     * @var ProductEntityService
     */
    protected $productManager;

    /**
     * @var ProductAttributesPreparator
     */
    protected $productPreparator;

    /**
     * BrandController constructor.
     * @param ProductEntityService $service
     * @param ProductAttributesPreparator $preparator
     */
    public function __construct(ProductEntityService $service, ProductAttributesPreparator $preparator)
    {
        $this->productManager = $service;
        $this->productPreparator = $preparator;
    }

    /**
     * @param SearchRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(SearchRequest $request, SearchService $searchService)
    {
        $param = $request->input('search');
        $products = $searchService->search($param);

        if ($products instanceof Product) {
            return redirect()->route('product_view', [
                'brand' => $products->brand->url_name,
                'category' => $products->category->url_name,
                'product' => $products->url_name
            ]);
        }

        $paginated = $this->paginate(
            $this->prepareProducts($products, $this->productPreparator),
            $request
        );
        $paginated->appends(['search' => $param])->links();

        return view('products.search_result', [
            'products' => $paginated,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function autocomplete(Request $request)
    {
        $param = $request->input('search');
        $products = $this->productManager->getProductsLike($param, 16);

        if (! count($products)) {
           is_numeric($param) && strlen($param) > 4
               ? $result[0]['message'] = Lang::get('products.search.imei')
               : $result[0]['message'] = Lang::get('products.search.failed');
           return response()->json(['data' => $result]);
        }

        return ProductResource::collection($this->productPreparator->prepareCollection($products));
    }
}
