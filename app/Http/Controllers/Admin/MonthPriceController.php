<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\MonthBestPrice;

class MonthPriceController extends Controller
{
    /**
     * @param MonthBestPrice $price
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(MonthBestPrice $price)
    {
        $product = $price->product;
        $price->delete();

        return redirect()->route('admin_product_view', ['product' => $product->url_name]);
    }
}
