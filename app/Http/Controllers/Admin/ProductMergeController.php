<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Services\ProductMergeService;
use Illuminate\Http\Request;

class ProductMergeController extends Controller
{
    /**
     * @var ProductMergeService
     */
    protected $mergeService;

    /**
     * ProductMergeController constructor.
     * @param ProductMergeService $service
     */
    public function __construct(ProductMergeService $service)
    {
        $this->mergeService = $service;
    }

    /**
     * Render product merge page
     *
     * @param Product $product
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Product $product)
    {
        $parent = $this->mergeService->getParent($product);
        $children = $this->mergeService->getChildren($product);
        $potentialSynonyms = $this->mergeService->getSimilar($parent);

        return view('admin.products.merge_index', [
            'baseProduct' => $parent,
            'relatedProducts' => $children,
            'potentialSynonyms' => $potentialSynonyms,
            'message' => session('synonymMessage'),
            'status' => session('synonymStatus'),

        ]);
    }

    /**
     * Change the product children
     *
     * @param Request $request
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, Product $product)
    {
        $this->mergeService->setSynonyms($product, json_decode($request->synonyms, true));

        return response()->json(['data' => 'Success']);
    }

    /**
     * Add child to the base product
     *
     * @param Request $request
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     */
    public function add(Request $request, Product $product)
    {
        $synonym = Product::where('name', $request->search)->doesntHave('children')->get()->first();
        if ($synonym) {
            $this->mergeService->addSynonym($synonym, $product);
            $request->session()->flash('synonymMessage',
                sprintf('%s was connected with %s as base product', $synonym->name, $product->name)
            );
            $request->session()->flash('synonymStatus', true);
        } else {
            $request->session()->flash('synonymMessage',
                sprintf('%s cannot be connected with %s as base product', $request->search, $product->name)
            );
            $request->session()->flash('synonymStatus', false);
        }

        return redirect()->route('admin_product_merge', ['product' => $product]);
    }
}
