<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BrandFormRequest;
use App\Models\Brand;
use App\Models\Image;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $brands = Brand::with('image')->orderBy('name')->get();

        return view('admin.brands.index', [
            'brands' => $brands,
            'message' => session('brandStatus'),
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.brands.create');
    }

    /**
     * @param Brand $brand
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Brand $brand)
    {
        return view('admin.brands.update', ['brand' => $brand]);
    }

    /**
     * @param BrandFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storage(BrandFormRequest $request)
    {
        $input = $request->all();
        if (isset($input['brand_id'])) {
            $brand = Brand::find($input['brand_id']);
            $brand->update($input);
        } else {
            $brand = Brand::create($input);
        }

        if (isset($input['logo'])) {
            $prevLogo = $brand->image;
            if ($prevLogo) {
                $prevLogo->delete();
            }
            $image = new Image();
            $image->path = $request->file('logo')->store('public/images/logos/brands');
            $image->name = $request->file('logo')->hashName();
            $brand->image()->save($image);
        }
        $message = sprintf('Brand - %s was %s', $brand->name, $request->brand_id ? 'updated' : 'created');
        $request->session()->flash('brandStatus', $message);


        return redirect()->route('admin_brands_view');
    }

    /**
     * @param Brand $brand
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Brand $brand)
    {
        $brand->delete();

        return redirect()->route('admin_brands_view');
    }
}
