<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\MonthBestPrice;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\Site;
use App\Models\TrustpilotReview;
use App\Parsers\CSVParser\WebgainsParser\EightMobileParser;
use App\Parsers\CSVParser\WebgainsParser\RecycleParser;
use App\Parsers\JSONParser\CustomParsers\CarphoneWarehouseParser;
use App\Parsers\JSONParser\CustomParsers\MackbackParser;
use App\Parsers\JSONParser\CustomParsers\MusicMagpieParser;
use App\Parsers\JSONParser\CustomParsers\TescoMobileParser;
use App\Parsers\JSONParser\CustomParsers\TopDollarMobileParser;
use App\Parsers\JSONParser\CustomParsers\VodafoneParser;
use App\Parsers\Services\ModelService;
use App\Services\BestMonthPriceSetter;
use App\Services\HighChartService;
use App\Services\IMEI\IMEIChecker;
use App\Services\Merger\ProductAutoMerger;
use App\Services\TrustpilotReviewServices\ReviewsImporter;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * @param HighChartService $chartService
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(HighChartService $chartService)
    {
        $groups = Group::with('sites', 'sites.failedReports', 'sites.passedReports')->get();
        $sites = Site::with('failedReports', 'passedReports')
            ->where('group_id', null)->get();

        return view('admin.main', ['groups' => $groups, 'sitesWithoutGroup' => $sites, 'charts' => $chartService->generate()]);
    }

    public function parse(ModelService $modelService, ProductAutoMerger $merger)
    {
        $site = Site::find(12);
        $parser = new TopDollarMobileParser($site, $modelService);
    dd($parser->run());

        $products = $merger->getMergeList();

        return view('admin.test', ['products' => $products]);

    }


}
