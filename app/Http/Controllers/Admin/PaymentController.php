<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentFormRequest;
use App\Models\Payment;
use App\Models\PaymentTerm;
use App\Services\EntityServices\PaymentEntitiesService;

class PaymentController extends Controller
{
    /**
     * @var PaymentEntitiesService
     */
    protected $paymentService;

    /**
     * PaymentController constructor.
     * @param PaymentEntitiesService $paymentService
     */
    public function __construct(PaymentEntitiesService $paymentService)
    {
        $this->paymentService = $paymentService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $paymentMethods = Payment::all();
        $paymentTerms = PaymentTerm::all();

        return view('admin.payments.index', [
            'methods' => $paymentMethods,
            'terms' => $paymentTerms,
            'message' => session('paymentStatus')
        ]);
    }

    /**
     * @param $type
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($type)
    {
        return view('admin.payments.create', ['type' => $type]);
    }

    /**
     * @param $type
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function update($type, $id)
    {
        $model = $this->paymentService->getByIdAndType($id, $type);

        if (! $model) {
            return redirect()->route('admin_payments_view');
        }

        return view('admin.payments.update', [
            'model' => $model,
            'type' => $type,
        ]);
    }

    /**
     * @param PaymentFormRequest $request
     * @param $type
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storage(PaymentFormRequest $request, $type)
    {
        $input = $request->input();

        if ($request->id) {
            $model = $this->paymentService->getByIdAndType($request->id, $type);
            $model->update($input);
        } else {
            $this->paymentService->createEntityByType($type, $input);
        }
        $message = sprintf('%s was %s', ucfirst($type), $request->id ? 'updated' : 'created');
        $request->session()->flash('paymentStatus', $message);

        return redirect()->route('admin_payments_view');

    }

}