<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserFormRequest;
use App\Models\Role;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission');
        $this->middleware('not.superadmin')->except('storage', 'index', 'create');
        $this->middleware('not.itself')->except('storage', 'index', 'create');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $currentUser = Auth::getUser();
        $users = User::with('role')->orderBy('role_id')->where([
            ['id', '!=', $currentUser->id],
            ['role_id', '!=', 1]
        ])->get();

        return view('admin.users.index', [
            'users' => $users,
            'message' => session('userStatus'),
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $roles = Role::where('name', '!=', 'SuperAdmin')->get();


        return view('admin.users.create', ['roles' => $roles]);
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(User $user)
    {
        $roles = Role::where('name', '!=', 'SuperAdmin')->get();

        return view('admin.users.update', ['user' => $user, 'roles' => $roles]);
    }

    /**
     * @param UserFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storage(UserFormRequest $request)
    {
        $input = $request->input();

        if ($input['password']) {
            $input['password'] = bcrypt($input['password']);
        } else {
            unset($input['password']);
        }

        if ($request->user_id) {
            $user = User::find($request->user_id);
            $user->update($input);
        } else {
            $user = User::create($input);
        }

        $message = sprintf('User - %s was %s', $user->name, $request->user_id ? 'updated' : 'created');
        $request->session()->flash('userStatus', $message);

        return redirect()->route('admin_users_main');
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('admin_users_main');
    }
}
