<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SearchRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Services\ProductAttributesPreparator;
use App\Services\ProductEntityService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class SearchController extends Controller
{
    /**
     * @var ProductAttributesPreparator
     */
    protected $preparator;

    /**
     * SearchController constructor.
     * @param ProductAttributesPreparator $preparator
     */
    public function __construct(ProductAttributesPreparator $preparator)
    {
        $this->preparator = $preparator;
    }

    /**
     * Search form rendering
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.products.search');
    }

    /**
     * @param SearchRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(SearchRequest $request)
    {
        $param = $request->input('search');
        $products = Product::with('brand', 'singleProfile', 'image', 'imageThumbnail')
            ->where('name', 'like', "%{$param}%")
            ->paginate(48);
        $products->appends(['search' => $param])->links();

        return view('admin.products.search_result', [
            'products' => $products,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function autocomplete(Request $request)
    {
        $param = $request->input('search');

        $products = Product::with(
            'brand',
            'profiles',
            'imageThumbnail',
            'image',
            'profiles.bestWorkingPrice',
            'children',
            'children.profiles',
            'children.profiles.bestWorkingPrice'
        )
            ->where('name', 'like', "%{$param}%")
            ->limit(20)
            ->get();

        if (! count($products)) {
            $result[0]['message'] = Lang::get('products.search.failed');
            return response()->json(['data' => $result]);
        }

        return ProductResource::collection($this->preparator->prepareCollection($products));
    }
}
