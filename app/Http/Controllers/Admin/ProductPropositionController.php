<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductPropositionFormRequest;
use App\Models\Condition;
use App\Models\ProductProposition;
use App\Models\Proposition;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;

class ProductPropositionController extends Controller
{
    /**
     * @param Product $product
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Product $product)
    {
        $product->load('propositions');
        $conditions = Condition::where('lang', 'en')->get()->groupBy('alias');

        return view('admin.products.propositions.index', [
            'product' => $product,
            'conditions' => $conditions,
            'message' => session('productProposition')
        ]);
    }

    /**
     * @param Product $product
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Product $product)
    {
        $propositions = Proposition::all();
        $conditions = Condition::where('lang', 'en')->get();

        return view('admin.products.propositions.create', [
            'product' => $product,
            'conditions' => $conditions,
            'propositions' => $propositions
        ]);

    }

    /**
     * @param Product $product
     * @param ProductProposition $proposition
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Product $product, ProductProposition $proposition)
    {
        $mainPropositions = Proposition::all();
        $conditions = Condition::where('lang', 'en')->get();

        return view('admin.products.propositions.update', [
            'product' => $product,
            'conditions' => $conditions,
            'mainPropositions' => $mainPropositions,
            'proposition' => $proposition,
        ]);
    }

    /**
     * @param ProductPropositionFormRequest $request
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storage(ProductPropositionFormRequest $request, Product $product)
    {
        $proposition = ProductProposition::where([
            'product_id' => $product->id,
            'condition_key' => $request->condition_key,
            'proposition_id' => $request->proposition_id
        ])->get()->first();

        if (! $proposition) {
            $proposition = new ProductProposition();
        }

        if ($request->propositionId && $proposition->id != $request->propositionId) {
            ProductProposition::destroy($request->propositionId);
        }

        $proposition->product_id = $product->id;
        $proposition->condition_key = $request->condition_key;
        $proposition->price = $request->price;
        $proposition->proposition_id = $request->proposition_id;
        $proposition->save();

        $message = sprintf('Trade in proposition for %s was saved', $product->name);
        $request->session()->flash('productProposition', $message);

        return redirect()->route('admin_product_proposition_view', ['product' => $product->url_name]);
    }

    /**
     * @param Product $product
     * @param ProductProposition $proposition
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Product $product, ProductProposition $proposition)
    {
        $proposition->delete();

        return redirect()->route('admin_product_proposition_view', ['product' => $product->url_name]);
    }
}
