<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;

class PopularProductController extends Controller
{
    /**
     * Render popular products view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $products = Product::with('image', 'imageThumbnail', 'brand')
            ->where('is_popular', 1)
            ->get();

        return view('admin.popular.index', [
           'products' => $products,
        ]);
    }

    /**
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     */
    public function make(Product $product)
    {
        $product->update(['is_popular' => 1]);

        return redirect()->back();
    }

    /**
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove(Product $product)
    {
        $product->update(['is_popular' => 0]);

        return redirect()->back();
    }
}
