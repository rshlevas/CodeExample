<?php

namespace App\Http\Controllers\Admin;

use App\Models\Proposition;
use App\Models\Site;
use App\Http\Requests\PropositionFormRequest;
use App\Http\Controllers\Controller;

class PropositionController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $propositions = Proposition::with('site', 'productPropositions', 'productPropositions.product')->get();

        return view('admin.propositions.index', [
            'propositions' => $propositions,
            'message' => session('propositionStatus'),
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $sites = Site::where('is_active', 1)->get();

        return view('admin.propositions.create', ['sites' => $sites]);
    }

    /**
     * @param Proposition $proposition
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Proposition $proposition)
    {
        $sites = Site::where('is_active', 1)->get();

        return view('admin.propositions.update', ['proposition' => $proposition, 'sites' => $sites]);
    }

    /**
     * @param PropositionFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storage(PropositionFormRequest $request)
    {
        if ($request->propositionId) {
            $proposition = Proposition::find($request->propositionId);
            $proposition->update($request->input());
        } else {
            $proposition = Proposition::create($request->input());
        }
        $message = sprintf('Trade in proposition - %s was %s', $proposition->name, $request->propositionId ? 'updated' : 'created');
        $request->session()->flash('propositionStatus', $message);

        return redirect()->route('admin_propositions_main');
    }

    /**
     * @param Proposition $proposition
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Proposition $proposition)
    {
        $proposition->delete();

        return redirect()->route('admin_propositions_main');
    }
}
