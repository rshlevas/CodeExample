<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Answer;

class AnswerController extends Controller
{
    public function delete(Answer $answer)
    {
        $answer->delete();

        return redirect()->back();
    }
}
