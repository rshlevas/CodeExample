<?php

namespace App\Http\Controllers\Admin;

use App\Models\Condition;
use App\Models\Network;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\EntityServices\FilterEntityService;

class FilterController extends Controller
{
    public function index()
    {
        $conditions = Condition::where('lang', 'en')->orderBy('order')->get();
        $networks = Network::where('lang', 'en')->orderBy('order')->get();

        return view('admin.filters.index', [
            'conditions' => $conditions,
            'networks' => $networks,
            'message' => session('filterMessage'),
        ]);
    }

    public function update(FilterEntityService $service, $type, $filter)
    {
        $filter = $service->getByIdAndType($filter, $type);

        return view('admin.filters.update', ['filter' => $filter, 'type' => $type]);
    }

    public function store(Request $request, FilterEntityService $service)
    {
        $filter = $service->getByIdAndType($request->filterId, $request->type);
        $filter->update($request->input());

        $request->session()->flash('filterMessage', sprintf('%s %s was updated', ucfirst($request->type), $filter->name));

        return redirect()->route('admin_filters_main');
    }

    public function switchActiveState(Request $request, FilterEntityService $service, $type, $filter)
    {
        $filter = $service->getByIdAndType($filter, $type);
        $filter->is_active = $filter->is_active === 0 ? 1 : 0;
        $filter->save();
        $request->session()->flash('filterMessage', sprintf(
            '%s %s was %s',
            ucfirst($request->type),
            $filter->name,
            $filter->is_active === 0 ? 'disabled' : 'enabled'
         ));

        return redirect()->route('admin_filters_main');
    }

    public function storeOrder(Request $request, $type)
    {
        $classNames = ['condition' => Condition::class, 'network' => Network::class];
        $className = $classNames[$type];

        foreach (json_decode($request->filters, true) as $params) {
            $filter = $className::find($params['id']);
            $filter->order = $params['order'];
            $filter->save();
        }

        return response()->json(['data' => 'Success']);
    }
}
