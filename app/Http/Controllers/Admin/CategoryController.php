<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryFormRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $categories = Category::with('products')->orderBy('name')->get();

        return view('admin.categories.index', [
            'categories' => $categories,
            'message' => session('categoriesStatus'),
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * @param Category $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Category $category)
    {
        return view('admin.categories.update', ['category' => $category]);
    }

    /**
     * @param CategoryFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storage(CategoryFormRequest $request)
    {
        if ($request->category_id) {
            $category = Category::find($request->category_id);
            $category->update($request->input());
        } else {
            $category = Category::create($request->input());
        }

        $message = sprintf('Category - %s was %s', $category->name,$request->category_id ? 'updated' : 'created');
        $request->session()->flash('categoriesStatus', $message);

        return redirect()->route('admin_categories_view');
    }

    /**
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Category $category)
    {
        $category->delete();

        return redirect()->route('admin_categories_view');
    }
}
