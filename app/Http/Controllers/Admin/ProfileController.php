<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\ChangeProfileInfoRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Render the [rofile page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.profile.view', [
            'user' => Auth::getUser(),
            'message' => session('message'),
        ]);
    }

    /**
     * Storing of main profile info
     *
     * @param ChangeProfileInfoRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeInfo(ChangeProfileInfoRequest $request)
    {
        $user = User::find($request->userId);
        $user->update($request->input());
        $request->session()->flash('message', 'Profile info was changed');

        return redirect()->route('admin_profile');
    }

    /**
     * Changing the users password
     *
     * @param ChangePasswordRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storePassword(ChangePasswordRequest $request)
    {
        $user = User::find($request->userId);
        $user->password = Hash::make($request->password);
        $user->save();
        $request->session()->flash('message', 'Password was changed');

        return redirect()->route('admin_profile');
    }
}
