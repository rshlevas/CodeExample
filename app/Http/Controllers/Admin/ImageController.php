<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ImportRequest;
use App\Services\ImageServices\ImageImporter;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    /**
     * Render start import page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function import()
    {
        return view('admin.images.import', [
            'message' => session('importImage'),
        ]);
    }

    /**
     * Store images and connect with related entities
     *
     * @param ImportRequest $request
     * @param ImageImporter $importer
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ImportRequest $request, ImageImporter $importer)
    {
        $result = $importer->import($request->file('zip')->store('public/images/import'), $request->group);
        $message = sprintf('%d images for %s were imported', count($result), $request->group);
        $request->session()->flash('importImage', $message);

        return redirect()->route('admin_image_import');
    }
}
