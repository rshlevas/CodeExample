<?php

namespace App\Http\Controllers\Admin;

use App\Services\TrustpilotReviewServices\ReviewsImporter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ImportRequest;

class TrustpilotReviewController extends Controller
{
    /**
     * Render start import page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function import()
    {
        return view('admin.reviews.import', [
            'message' => session('importReview'),
        ]);
    }

    /**
     * Store images and connect with related entities
     *
     * @param ImportRequest $request
     * @param ReviewsImporter $importer
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ImportRequest $request, ReviewsImporter $importer)
    {
        $result = $importer->import($request->file('review')->store('public/reviews'));
        $message = sprintf('%d reviews were imported for %d sites', $result['reviews'], $result['sites']);
        $request->session()->flash('importReview', $message);

        return redirect()->route('admin_trustpilot_reviews_import');
    }
}
