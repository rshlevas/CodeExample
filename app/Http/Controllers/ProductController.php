<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Site;
use App\Services\FilterPriceService;
use App\Services\FilterSelector;
use App\Services\ProductAttributesPreparator;
use App\Services\ProductEntityService;
use App\Traits\DateMessageSelectable;
use App\Traits\SizeSelectable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use SizeSelectable, DateMessageSelectable;

    /**
     * @var ProductEntityService
     */
    protected $productManager;

    /**
     * @var ProductAttributesPreparator
     */
    protected $productPreparator;

    /**
     * @var FilterPriceService
     */
    protected $filterService;

    /**
     * ProductController constructor.
     * @param ProductEntityService $service
     * @param ProductAttributesPreparator $preparator
     * @param FilterPriceService $filterService
     */
    public function __construct(
        ProductEntityService $service,
        ProductAttributesPreparator $preparator,
        FilterPriceService $filterService
    ) {
        $this->productManager = $service;
        $this->productPreparator = $preparator;
        $this->filterService = $filterService;
    }

    /**
     * Render product view
     *
     * @param FilterSelector $selector
     * @param Product $product
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(FilterSelector $selector, $brand, $category, $product)
    {
        $product = $this->productManager->getProductByParam('url_name', $product)->first();
        if (! $product) {
            throw new ModelNotFoundException();
        }
        if($product->has('propositions')) {
            $product->load('propositions', 'propositions.proposition', 'propositions.proposition.site');
            $propositions = $product->propositions->groupBy('condition_key');
            $filteredPropositions = isset($propositions['default']) ? $propositions['default']->groupBy('proposition.site_id') : collect([]);
        }
        $preparedProduct = $this->productPreparator->prepareSingle($product);
        $prices = $this->filterService->filter($preparedProduct);
        $filters = $selector->run($preparedProduct);
        $unknownSites = Site::with('image', 'payments', 'paymentTerm')
            ->where('is_active', 2)->get();
        $relatedProducts = $this->productManager->getRelatedProducts(
            $product->name,
            [['brand_id', '=', $product->brand_id]],
            1,
            false
        );
        $bestPrice = $prices->first();

        return view('products.view', [
            'product' => $preparedProduct,
            'prices' => $prices,
            'unknownSites' => $unknownSites,
            'bestPrice' => $bestPrice,
            'updateMessage' => $this->selectDatePhrase($bestPrice->productProfile->product->updated_at),
            'filters' => $filters,
            'sizes' => $this->getSizes($product),
            'propositions' => isset($filteredPropositions) ? $filteredPropositions : collect([]),
            'relatedProducts' => $relatedProducts->slice(0, 5),
        ]);
    }

    /**
     * Returns best price of the product. Used for Ajax request of filter changing at product page
     *
     * @param Request $request
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function filterBestPrice(Request $request, $brand, $category, $product)
    {
        $product = $this->productManager->getProductByParam('url_name', $product)->first();
        $preparedProduct = $this->productPreparator->prepareSingle($product);
        $prices = $this->filterService->filter($preparedProduct, $request->query());
        $bestPrice = $prices->shift();

        if ($bestPrice) {
            $html = view('products.components.best_price', [
                'bestPrice' => $bestPrice,
                'updateMessage' => $this->selectDatePhrase($bestPrice->productProfile->product->updated_at),
            ])->render();
        } else {
            $html = '<div></div>';
        }

        return response()->json(['html' => $html]);
    }

    /**
     * Returns the product prices. Used for Ajax request of filter changing at product page
     *
     * @param Request $request
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function filterPrices(Request $request, $brand, $category, $product)
    {
        $product = $this->productManager->getProductByParam('url_name', $product)->first();
        $preparedProduct = $this->productPreparator->prepareSingle($product);
        $prices = $this->filterService->filter($preparedProduct, $request->query());
        if($product->has('propositions')) {
            $product->load('propositions', 'propositions.proposition', 'propositions.proposition.site');
            $propositions = $product->propositions->groupBy('condition_key');
            $filteredPropositions = isset($propositions[$request->condition])
                ? $propositions[$request->condition]->groupBy('proposition.site_id')
                : collect([]);
        }
        $bestPrice = $prices->first();
        $unknownSites = Site::with('image', 'payments', 'paymentTerm')
            ->where('is_active', 2)->get();

        $html = view('products.components.product_site_table', [
            'prices' => $prices,
            'bestPrice' => $bestPrice,
            'unknownSites' => $unknownSites,
            'propositions' => isset($filteredPropositions) ? $filteredPropositions : collect([]),
        ])->render();

        return response()->json(['html' => $html]);
    }

}
