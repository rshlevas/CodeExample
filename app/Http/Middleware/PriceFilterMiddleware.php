<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\ParameterBag;

class PriceFilterMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (empty($request->query()) || $this->checkParams($request->query())) {
            return $next($request);
        }

        return redirect()->route('home');
    }

    /**
     * @param array $params
     * @return bool
     */
    protected function checkParams(array $params)
    {
        $allowedParams = config('filter.middleware');

        foreach ($params as $filter => $value) {
            if (! isset($allowedParams[$filter]) ||  ! in_array($value, $allowedParams[$filter])) {
                return false;
            }
        }

        return true;
    }


}
