<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminCheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $allowedRoles = ['SuperAdmin', 'Admin'];

        if (! in_array(Auth::getUser()->role->name, $allowedRoles)) {
            return redirect('/home');
        }

        return $next($request);
    }
}
