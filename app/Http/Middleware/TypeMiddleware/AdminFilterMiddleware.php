<?php

namespace App\Http\Middleware\TypeMiddleware;

class AdminFilterMiddleware extends TypeMiddleware
{
    /**
     * @var array
     */
    protected $validTypes = ['condition', 'network'];
}
