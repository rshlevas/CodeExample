<?php

namespace App\Http\Middleware\TypeMiddleware;

class PaymentTypeMiddleware extends TypeMiddleware
{
    /**
     * @var array
     */
    protected $validTypes = ['method', 'term'];
}
