<?php

namespace App\Http\Middleware;

use Closure;

class NotSuperAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $request->user->is_super_admin ? redirect()->route('admin_main') : $next($request);
    }
}
