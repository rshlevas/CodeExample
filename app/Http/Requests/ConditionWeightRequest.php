<?php

namespace App\Http\Requests;

use App\Models\Condition;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ConditionWeightRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::getUser();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->generateRules();
    }

    /**
     * @return array
     */
    protected function generateRules()
    {
        $conditions = $this->getConditions();
        $rules = [];
        foreach ($conditions as $condition) {
            $rules["{$condition->alias}-min-weight"] = $this->getMinWeightRule($condition);
            $rules["{$condition->alias}-max-weight"] = $this->getMaxWeightRule($condition);
        }


        return $rules;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function getConditions()
    {
        return Condition::all();
    }

    /**
     * @return string
     */
    protected function getMinWeightRule($condition)
    {
        return 'required|integer|max:100|between:0,' . $this->{"{$condition->alias}-max-weight"};
    }

    /**
     * @return string
     */
    protected function getMaxWeightRule($condition)
    {
        return 'required|integer|max:100|between:' . $this->{"{$condition->alias}-min-weight"} . ',100';
    }
}
