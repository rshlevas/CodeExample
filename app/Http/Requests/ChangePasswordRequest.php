<?php

namespace App\Http\Requests;

use App\Rules\PasswordCheckRule;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::getUser();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::find($this->userId);

        return [
            'oldPassword' => ['required', new PasswordCheckRule($user)],
            'password' => 'required|string|min:6|confirmed',
        ];
    }
}
