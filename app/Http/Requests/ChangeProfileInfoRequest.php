<?php

namespace App\Http\Requests;

use App\Rules\PasswordCheckRule;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ChangeProfileInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::getUser();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::find($this->userId);
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'check_password' => ['required', new PasswordCheckRule($user)],
        ];

        if ($user->email === $this->email) {
            $rules['email'] = 'required|string|email|max:255';
        }

        return $rules;
    }
}
