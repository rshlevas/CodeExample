<?php

namespace App\Http\Requests;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProductFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::getUser();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $product = $this->route('product');
        $infoRules = [
            'name' => sprintf('required|unique:products,name,%s', $product->id),
            'url_name' => sprintf('required|unique:products,url_name,%s', $product->id),
        ];
        $imageRules = [
            'image' => 'required|mimes:jpeg,bmp,png|max:1024'
        ];

        return $this->image ? $imageRules : $infoRules;
    }
}
