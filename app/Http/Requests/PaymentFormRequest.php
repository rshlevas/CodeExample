<?php

namespace App\Http\Requests;

use App\Services\EntityServices\PaymentEntitiesService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PaymentFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::getUser();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->type == 'term' || $this->type == 'method') {
            return $this->getRules($this->type, $this->id);
        }

        return [

        ];

    }

    /**
     * @param $type
     * @param $id
     * @return array
     */
    protected function getRules($type, $id)
    {
        if ($id) {
            return $this->getRulesDependsOnId($type, $id);
        }

        return $this->getDefaultRuleByType($type);
    }

    /**
     * @param $type
     * @param $id
     * @return array
     */
    protected function getRulesDependsOnId($type, $id)
    {
        $paymentService = resolve(PaymentEntitiesService::class);
        $model = $paymentService->getByIdAndType($id, $type);

        if ($model->name !== $this->name) {
            return $this->getDefaultRuleByType($type);
        } else {
            return [
                'name' => 'required'
            ];
        }
    }


    /**
     * @param $type
     * @return array
     */
    protected function getDefaultRuleByType($type)
    {
        switch ($type) {
            case 'term':
                return [
                    'name' => 'required|unique:payment_term'
                ];
            case 'method':
                return [
                    'name' => 'required|unique:payments'
                ];
        }
    }
}
