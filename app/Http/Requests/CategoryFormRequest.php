<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CategoryFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::getUser();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|unique:categories',
            'url_name' => 'required|unique:categories',
        ];

        if ($this->category_id) {
            $rules['name'] = sprintf('required|unique:categories,name,%s', $this->category_id);
            $rules['url_name'] = sprintf('required|unique:categories,url_name,%s', $this->category_id);
        }

        return $rules;
    }
}
