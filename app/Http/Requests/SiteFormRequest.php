<?php

namespace App\Http\Requests;

use App\Models\Site;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class SiteFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::getUser();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|unique:sites',
            'url_name' => 'required|unique:sites',
            'path' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'trust_point' => 'nullable|numeric|between:0,10',
            'logo' => 'mimes:jpeg,bmp,png|max:1024',
            'trustpilot_link' => 'nullable|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
        ];

        if ($this->siteId) {
            $rules['name'] = sprintf('required|unique:sites,name,%s', $this->siteId);
            $rules['url_name'] = sprintf('required|unique:sites,url_name,%s', $this->siteId);
        }

        return $rules;
    }
}
