<?php

namespace App\Http\Requests;

use App\Models\Brand;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class BrandFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::getUser();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|unique:brands',
            'url_name' => 'required|unique:brands',
            'logo' => 'mimes:jpeg,bmp,png|max:1024',
        ];

        if ($this->brand_id) {
            $rules['name'] = sprintf('required|unique:brands,name,%s', $this->brand_id);
            $rules['url_name'] = sprintf('required|unique:brands,url_name,%s', $this->brand_id);
        }

        return $rules;
    }
}
