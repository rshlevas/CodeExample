<?php

namespace App\Providers;

use App\Parsers\CSVParser\RecycleParser as CSVParser;
use App\Parsers\ParserFactory;
use App\Parsers\Services\EntityChecker;
use App\Parsers\Services\EntityCreator;
use App\Parsers\Services\EntityDestroyer;
use App\Parsers\Services\ImageService;
use App\Parsers\Services\ModelService;
use App\Parsers\Services\ParseStatisticService;
use Illuminate\Support\ServiceProvider;

class ParserServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ParserFactory::class, function() {
            return new ParserFactory();
        });

        $this->app->bind(ModelService::class, function($app) {
            return new ModelService(
                $app->make(EntityCreator::class),
                $app->make(EntityChecker::class),
                $app->make(EntityDestroyer::class),
                $app->make(ImageService::class)
            );
        });

        $this->app->bind(EntityCreator::class, function() {
            return new EntityCreator();
        });

        $this->app->bind(EntityChecker::class, function() {
            return new EntityChecker();
        });

        $this->app->bind(EntityDestroyer::class, function() {
            return new EntityDestroyer();
        });

        $this->app->bind(ImageService::class, function() {
            return new ImageService();
        });

        $this->app->bind(ParseStatisticService::class, function() {
           return new ParseStatisticService();
        });
    }
}
