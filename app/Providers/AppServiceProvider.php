<?php

namespace App\Providers;

use App\Models\Image;
use App\Models\NegativeReport;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Observers\ImageObserver;
use App\Observers\NegativeReportObserver;
use App\Observers\ProductObserver;
use App\Observers\ProductPriceObserver;
use App\Services\EntityServices\PaymentEntitiesService;
use App\Services\EntityServices\ReportEntitiesService;
use App\Services\HighChartService;
use App\Services\ImageServices\Extractors\ExtractorFactory;
use App\Services\ImageServices\ProductImageService;
use App\Services\ProductAttributesPreparator;
use App\Services\IMEI\IMEIChecker;
use App\Services\ProductEntityService;
use App\Models\Post;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        NegativeReport::observe(NegativeReportObserver::class);
        ProductPrice::observe(ProductPriceObserver::class);
        Product::observe(ProductObserver::class);
        Image::observe(ImageObserver::class);
        $posts = Post::published()->with('attachment')->take(6)->orderBy('post_date', 'desc')->get()->slice(3, 3);
        View::share('posts', $posts);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PaymentEntitiesService::class, function ($app) {
            return new PaymentEntitiesService();
        });

        $this->app->bind(ReportEntitiesService::class, function ($app) {
            return new ReportEntitiesService();
        });

        $this->app->bind(HighChartService::class, function ($app) {
           return new HighChartService();
        });

        $this->app->bind(ProductImageService::class, function ($app) {
            return new ProductImageService();
        });

        $this->app->bind(ExtractorFactory::class, function ($app) {
           return new ExtractorFactory();
        });

        $this->app->bind(ProductAttributesPreparator::class, function ($app) {
           return new ProductAttributesPreparator();
        });

        $this->app->bind(IMEIChecker::class, function ($app) {
            return new IMEIChecker();
        });

        $this->app->bind(ProductEntityService::class, function ($app) {
            return new ProductEntityService();
        });

    }
}
