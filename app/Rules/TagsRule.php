<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class TagsRule implements Rule
{
    /**
     * Not allowed tags list
     *
     * @var array
     */
    protected $notAllowedTags = [
        'div', 'script', 'html', 'body', 'form', 'button', 'select', 'iframe'
    ];

    /**
     * Tags types
     *
     * @var array
     */
    protected $types = [
        'opening', 'closing'
    ];

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $result = [];

        foreach ($this->notAllowedTags as $tag) {
            $result[] = $this->checkTags(htmlspecialchars_decode($value), $tag);
        }

        return empty(array_filter($result));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute contains not allowed tags.';
    }

    /**
     * Check the value on tags presents
     *
     * @param $value
     * @param $tag
     * @return array
     */
    protected function checkTags($value, $tag)
    {
        $matches = [];

        foreach ($this->types as $type) {
            $matches[] = $this->checkTag($this->getPattern($tag, $type), $value, $matches);

        }

        return array_filter($matches);
    }

    /**
     * Check the value on tag present by pattern
     *
     * @param $pattern
     * @param $value
     * @param $matches
     * @return mixed
     */
    protected function checkTag($pattern, $value, $matches)
    {
        preg_match($pattern, $value, $matches);

        return $matches;
    }

    /**
     * Return the pattern depending on tag type
     *
     * @param $tag
     * @param $type
     * @return mixed
     */
    protected function getPattern($tag, $type)
    {
        $opening = "/<[\s]*{$tag}[\s\S.]*?>/";
        $closing = "/<\/[\s]*{$tag}[\s\S.]*?>/";

        return ${$type};
    }


}
