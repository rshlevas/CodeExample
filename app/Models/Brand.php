<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var array
     */
    protected $fillable = ['name', 'url_name', 'updated_at'];

    /**
     * Products of current brand
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function image()
    {
        return $this->morphOne(Image::class, 'imagable');
    }

    /**
     * @return bool|null
     */
    public function delete()
    {
        $this->image()->delete();

        return parent::delete();
    }

    /**
     * Name of the route key
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'url_name';
    }
}
