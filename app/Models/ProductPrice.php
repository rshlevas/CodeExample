<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPrice extends Model
{
    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'product_prices';

    /**
     * @var array
     */
    protected $fillable = [
        'price', 'currency', 'product_profile_id', 'created_at', 'condition', 'network', 'is_exist',
    ];

    /**
     * Product profiles related to current price
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function productProfile()
    {
        return $this->belongsTo(ProductProfile::class, 'product_profile_id', 'id');
    }
}
