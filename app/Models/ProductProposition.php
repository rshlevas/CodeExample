<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductProposition extends Model
{
    /**
     * @var string
     */
    protected $table = 'products_propositions';

    /**
     * @var array
     */
    protected $fillable = [
        'product_id', 'proposition_id', 'price', 'condition_key'
    ];

    /**
     * Product related to this proposition
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Proposition related to this entity
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function proposition()
    {
        return $this->belongsTo(Proposition::class);
    }
}
