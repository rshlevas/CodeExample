<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Description extends Model
{
    protected $fillable = [
        'content', 'language', 'describable_id', 'describable_type'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function describable()
    {
        return $this->morphTo();
    }

    public function getShortContentAttribute()
    {
        return strlen($this->content) > 350 ? sprintf('%s...', substr($this->content, 0, 347)) : $this->content;
    }

}
