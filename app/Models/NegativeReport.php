<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NegativeReport extends Model
{
    /**
     * @var string
     */
    protected $table = 'negative_report';

    /**
     * @var array
     */
    protected $fillable = [
        'site_id', 'reason'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    /**
     * Returns report type
     *
     * @return string
     */
    public function getTypeAttribute()
    {
        return 'failed';
    }
}
