<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MonthBestPrice extends Model
{
    /**
     * @var string
     */
    protected $table = 'month_best_price';

    /**
     * @var array
     */
    protected $fillable = ['price', 'product_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
