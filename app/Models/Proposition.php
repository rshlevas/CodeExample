<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proposition extends Model
{
    /**
     * @var string
     */
    protected $table = 'propositions';

    /**
     * @var array
     */
    protected $fillable = [
        'site_id', 'name', 'link'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productPropositions()
    {
        return $this->hasMany(ProductProposition::class);
    }

    /**
     * @return static
     */
    public function getProductsAttribute()
    {
        $products = collect([]);
        foreach ($this->productPropositions as $proposition) {
            $products->push($proposition->product);
        }

        return $products->unique();
    }
}
