<?php

namespace App\Models;

use Corcel\Model\Post as BasePost;

class Post extends BasePost
{
    /**
     * Make short content for the post
     *
     * @return mixed|string
     */
    public function getPostShortContentAttribute()
    {
        return strlen($this->post_content) > 250
            ? sprintf('%s...', substr($this->post_content, '0', '247'))
            : $this->post_content;
    }
}
