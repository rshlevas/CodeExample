<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrustpilotReview extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['author', 'title', 'site_id', 'content'];

    /**
     * @var string
     */
    protected $table = 'trustpilot_rewiews';

    /**
     * Target site of the review
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    /**
     * @return mixed|string
     */
    public function getShortContentAttribute()
    {
        return strlen($this->content) > 335
        ? sprintf('%s...', substr($this->content, 0, 332))
        : $this->content;
    }
}
