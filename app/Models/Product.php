<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'model',
        'brand_id',
        'check_name',
        'url_name',
        'is_popular',
        'updated_at',
        'size',
        'size_group',
        'size_group_display',
        'parent_id',
        'category_id',
    ];

    /**
     * The product best working price among all prices
     *
     * @var ProductPrice
     */
    protected $best_price;

    /**
     * Count of unique product profiles
     *
     * @var int
     */
    protected $profiles_count;

    /**
     * Product short name
     *
     * @var string
     */
    protected $short_name;

    /**
     * Profuct short model
     *
     * @var string
     */
    protected $short_model;

    /**
     * Brand name of current product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * Single product profile
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function singleProfile()
    {
        return $this->hasOne(ProductProfile::class);
    }
    /**
     * ProductProfiles of current product with last prices
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function profiles()
    {
        return $this->hasMany(ProductProfile::class);
    }

    /**
     * ProductProfiles of current product all prices
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function profilesWithAllPrices()
    {
        return $this->hasMany(ProductProfile::class)->with('prices');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function allImages()
    {
        return $this->morphMany(Image::class, 'imagable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function images()
    {
        return $this->morphMany(Image::class, 'imagable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function image()
    {
        return $this->morphOne(Image::class, 'imagable')->where('is_thumbnail', 0);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function imageThumbnail()
    {
        return $this->morphOne(Image::class, 'imagable')->where('is_thumbnail', 1);
    }

    /**
     * @return bool|null
     */
    public function delete()
    {
        foreach ($this->allImages() as $image) {
            $image->delete();
        }

        return parent::delete();
    }

    /**
     * Return the name of url model key
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'url_name';
    }

    /**
     * @return mixed|string
     */
    public function getShortNameAttribute()
    {
        return $this->short_name;
    }

    /**
     * Set product short name
     *
     * @param $name
     * @return $this
     */
    public function setShortName($name)
    {
        $this->short_name = $name;

        return $this;
    }

    /**
     * @return mixed|string
     */
    public function getShortModelAttribute()
    {
        return $this->short_model;
    }

    /**
     * Set product short model
     *
     * @param $model
     * @return $this
     */
    public function setShortModel($model)
    {
        $this->short_model = $model;

        return $this;
    }

    /**
     * Returns best price of current product among all profiles
     *
     * @return mixed
     */
    public function getBestPriceAttribute()
    {
        return $this->best_price;
    }

    /**
     * Set best price attribute
     *
     * @param ProductPrice $price
     * @return $this
     */
    public function setBestPrice(ProductPrice $price)
    {
        $this->best_price = $price;

        return $this;
    }

    /**
     * Return number of profiles
     *
     * @return int
     */
    public function getProfilesCountAttribute()
    {
        return $this->profiles_count;
    }

    /**
     * Set profiles count attribute
     *
     * @param int $count
     * @return $this
     */
    public function setProfilesCount(int $count)
    {
        $this->profiles_count = $count;

        return $this;
    }

    /**
     * Return descriptions of product
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function descriptions()
    {
        return $this->morphMany(Description::class, 'describable');
    }

    /**
     * Return the monthly best prices of the product
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function monthBestPrices()
    {
        return $this->hasMany(MonthBestPrice::class);
    }

    /**
     * Returns the month best price of product by year and month
     *
     * @param $year
     * @param $month
     * @return mixed
     */
    public function getMonthBestPrice($year, $month)
    {
        return $this->monthBestPrices()
            ->whereYear('updated_at', '=', $year)
            ->whereMonth('updated_at', '=', $month)
            ->get()
            ->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function sizeRelated()
    {
        return $this->belongsToMany(
            Product::class,
            'product_size_relation',
            'product_id',
            'related_id');
    }

    /**
     * Base product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Product::class, 'parent_id');
    }

    /**
     * Children products
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(Product::class, 'parent_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Trad in propositions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function propositions()
    {
        return $this->hasMany(ProductProposition::class);
    }
}
