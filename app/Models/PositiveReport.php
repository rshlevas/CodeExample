<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PositiveReport extends Model
{
    /**
     * @var string
     */
    protected $table = 'positive_report';

    /**
     * @var array
     */
    protected $fillable = [
        'site_id', 'brand', 'product', 'productProfile', 'nonChanged', 'price', 'failedLines'
    ];

    /**
     * Site owner of this report
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    /**
     * Returns report type
     *
     * @return string
     */
    public function getTypeAttribute()
    {
        return 'passed';
    }
}
