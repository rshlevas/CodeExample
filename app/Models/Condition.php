<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
    protected $fillable = [
        'name', 'alias', 'key', 'lang', 'min_weight', 'max_weight', 'order',  'is_active'
    ];
}
