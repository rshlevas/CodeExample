<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    protected $fillable = [
        'name', 'path', 'imagable_id', 'imagable_type', 'is_thumbnail'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function imagable()
    {
        return $this->morphTo();
    }

    /**
     * Delete image from db and storage dir
     *
     * @return bool|null
     */
    public function delete()
    {
        $path = str_replace('storage', 'public', $this->path);
        Storage::delete($path);

        return parent::delete();
    }

    /**
     * Returns image source
     *
     * @return mixed
     */
    public function getSourceAttribute()
    {
        return env('APP_ENV') !== 'local' ? $this->CDN_path : asset($this->path);
    }
}
