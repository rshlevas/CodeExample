<?php

namespace App\Parsers;

use App\Models\Site;
use App\Parsers\Exceptions\ParserException;
use App\Parsers\Interfaces\ParserInterface;
use App\Parsers\Services\ModelService;

abstract class Parser implements ParserInterface
{
    /**
     * @var bool
     */
    protected $imagable = false;

    /**
     * @var bool
     */
    protected $sourceCheckable = false;

    /**
     * @var bool
     */
    protected $is_valid = true;

    /**
     * @var ModelService
     */
    protected $modelService;

    /**
     * @var Site
     */
    protected $site;

    /**
     * @var array
     */
    protected $customFilters = [];

    /**
     * Array with number of parse results
     *
     * @var array
     */
    protected $parseResults = [
        'brand' => 0,
        'product' => 0,
        'productProfile' => 0,
        'price' => 0,
        'nonChanged' => 0,
        'failedLines' => 0,
    ];

    protected $dataMap = [
        'brand' => [
            'name' => null,
            'url_name' => null,
        ],
        'product' => [
            'name' => null,
            'model' => null,
            'check_name' => null,
            'url_name' => null,
        ],
        'product_profile' => [
            'site_id' => null,
        ],
        'link' => [
            'link' => null,
        ],
        'price' => [
            0 => [
                'price' => null,
                'currency' => null,
                'created_at' => null,
                'condition' => 1,
                'network' => 1,
                'is_exist' => 1,
            ],
        ],
        'images' => [
            0 => [
                'url' => null,
                'product_class' => null,
            ],
            1 => [
                'url' => null,
                'product_class' => null,
                'thumb' => true,
            ]
        ],
    ];


    /**
     * BaseParser constructor.
     * @param ModelService $modelService
     * @param Site $site
     */
    public function __construct(Site $site, ModelService $modelService)
    {
        $this->modelService = $modelService;
        $this->site = $site;
        $this->dataMap['product_profile'] = $this->site->id;
        $this->addCustomFilters();
    }

    /**
     * @return array
     */
    public function run()
    {
        $source = $this->getSource();
        $count = 0;

        foreach ($source as $line) {
           if (env('APP_ENV') === 'local' && $count > 3000) {
                break;
            }
            $this->parseLine($line);
            $count++;
            dump($count);
        }

        return $this->parseResults;
    }

    /**
     * @return array
     * @throws ParserException
     */
    abstract protected function getSource();

    /**
     * Function, that sends prepared data to ModelService class
     *
     * @param array $line
     * @return bool
     */
    protected function parseLine(array $line)
    {
        $data = $this->prepareData($line);
        $data = $this->setProperData($data);
        $data = $this->removeLowPrices($data);
        $this->checkData($data);
        dump($data['product']['name']. " " . $this->site->name);
        if ( ! $this->is_valid || ! $this->checkDefaultConditionExist($data['price']) || ! $this->checkSource($data['link']['link'])) {
            return $this->parseResults['failedLines']++;
        }
        $answer = $this->modelService->run($data, $this->imagable);
        dump($answer);
        return $this->handle($answer);
    }

    /**
     * Checks if at prices info present at least one price with default filter values
     *
     * @param $prices
     * @return bool
     */
    protected function checkDefaultConditionExist($prices)
    {
        foreach ($prices as $price) {
            if ($price['condition'] === 1 && $price['network'] === 1) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $data
     * @return mixed
     */
    protected function removeLowPrices($data)
    {
        foreach ($data['price'] as $key => $price) {
            if($price['price'] < 0.50) {
                unset($data['price'][$key]);
            }
        }

        return $data;
    }

    /**
     * Check if there is no empty data params
     *
     * @return bool
     */
    protected function checkData($data)
    {
        $this->is_valid = true;
        if ( ! $this->imagable && isset($data['images'])) {
            array_pop($data);
        }

        return $this->checkParam($data);
    }

    /**
     * Check if present any empty params
     *
     * @param array $params
     * @return bool
     */
    protected function checkParam(array &$params)
    {
        foreach ($params as $param) {
            if (! $param || empty($param)) {
                $this->is_valid = false;
            }
            if (is_array($param)) {
                $this->checkParam($param);
            }
        }
    }

    /**
     * Checks if the product profile link is valid
     *
     * @param string $url
     * @return bool
     */
    protected function checkSource(string $url)
    {
        if (! $this->sourceCheckable) {
            return true;
        }
        $headers = @get_headers($url, 1);

        return stripos($headers[0],"200 OK") ? true : false;
    }

    /**
     * Function, that handel the ModelService answer.
     *
     * @param $answer
     * @return bool
     */
    protected function handle($answer)
    {
        $this->parseResults[$answer]++;

        return true;
    }

    /**
     * Return parser class name
     *
     * @return string
     */
    public function getName()
    {
        return get_class($this);
    }

    /**
     * Prepare necessary data. Prepare product name, brand name, model and checking name
     *
     * @param array $data
     * @return array
     */
    protected function setProperData(array $data)
    {
        $name = $data['product']['name'];
        $data['product']['name'] = trim(ucfirst($this->removeDuplicates($this->filter($name))));
        $nameParts = explode(' ', $data['product']['name']);
        $data['product']['check_name'] = $this->getCheckName($nameParts);
        $data['product']['url_name'] = $this->getUrlName($nameParts);
        $data['brand']['name'] = ucfirst(array_shift($nameParts));
        $data['brand']['url_name'] = strtolower($this->getUrlName(explode(' ', $data['brand']['name'])));
        $data['product']['model'] = implode(' ', $nameParts);
        $data = $this->setSize($data);

        return $data;
    }

    /**
     * Set product size, if it exists, into data array
     *
     * @param $data
     * @return mixed
     */
    protected function setSize($data)
    {
        $size = $this->getSizeIfPresent($data['product']['check_name']);

        return $size ? $this->setSizeParams($data, $size) : $data;
    }

    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    protected function getSizes()
    {
        return config('filter.sizes.parsing');
    }

    /**
     * Checks if size info present at product name, returns size if it present
     *
     * @param $checkName
     * @return bool|mixed
     */
    protected function getSizeIfPresent($checkName)
    {
        foreach ($this->getSizes() as $size) {
            if (strpos($checkName, $size) !== false) {
                return $size;
            }
        }

        return false;
    }

    /**
     * Set size params into data array
     *
     * @param $data
     * @param $size
     * @return mixed
     */
    protected function setSizeParams($data, $size)
    {
        $data['product']['size'] = strtoupper($size);
        $data['product']['size_group'] = str_replace("{$size} ",'', $data['product']['check_name']);
        $data['product']['size_group_display'] = trim(str_replace(strtoupper($size),'', $data['product']['name']));

        return $data;
    }

    abstract protected function prepareData(array $line);

    /**
     * Create checking name of product from its name
     *
     * @param array $nameParts
     * @return string
     */
    protected function getCheckName(array $nameParts)
    {
        natcasesort($nameParts);

        return strtolower(implode(' ', $nameParts));
    }

    /**
     * Create url name of product from its name
     *
     * @param array $nameParts
     * @return string
     */
    protected function getUrlName(array $nameParts)
    {
        return implode('_', $nameParts);
    }

    /**
     * Remove duplicates from name string
     *
     * @param $string
     * @return string
     */
    protected function removeDuplicates($string)
    {
        return implode(' ', array_filter(array_unique(explode(' ', $string))));
    }

    /**
     * Replace parts of the string according to filters
     *
     * @param $string
     * @return mixed
     */
    protected function filter($string)
    {
        foreach ($this->filters as $filter) {
            $string = $filter['method']($filter['needle'], $filter['replace'], $string);
        }

        return $string;
    }

    /**
     * Adds filter to the filters list
     *
     * @return array|bool
     */
    protected function addCustomFilters()
    {
        return empty($this->customFilters) ? false : $this->filters = array_merge($this->customFilters, $this->filters);
    }

    /**
     * Number of filters that will be used to replace unnecessary data
     *
     * @var array
     */
    protected $filters = [
        0 => [
            'method' => 'str_replace',
            'needle' => ['.', ','],
            'replace' => ' ',
        ],
        1 => [
            'method' => 'preg_replace',
            'needle' => ['/[wW][iI]-?[fF][iI]/'],
            'replace' => 'WIFI',
        ],
        2 => [
            'method' => 'preg_replace',
            'needle' => '/\s?\+\s?/',
            'replace' => ' ',
        ],
        3 => [
            'method' => 'str_ireplace',
            'needle' => ['ericsson ', '(', ')', 'with '],
            'replace' => '',
        ],
        4 => [
            'method' => 'str_ireplace',
            'needle' => 'mini',
            'replace' => 'Mini',
        ],
        5 => [
            'method' => 'str_replace',
            'needle' => ['gb ', 'Gb ', 'gB '],
            'replace' => 'GB ',
        ],
        6 => [
            'method' => 'preg_replace',
            'needle' => ['/\s?[gG][bB]/'],
            'replace' => 'GB',
        ],
        7 => [
            'method' => 'str_ireplace',
            'needle' => 'LGGB230',
            'replace' => 'LG GB230',
        ],
        8 => [
            'method' => 'str_ireplace',
            'needle' => 'iphone',
            'replace' => 'iPhone',
        ],
        9 => [
            'method' => 'str_ireplace',
            'needle' => ['4s ', '5s ', '6s ', '5c ', '3gs ', '3g ', '2g '],
            'replace' => ['4S ', '5S ', '5S ', '5C ', '3GS ', '3G ', '2G '],
        ],
    ];

    /**
     * Destructor
     */
    public function __destruct()
    {
        return;
    }
}