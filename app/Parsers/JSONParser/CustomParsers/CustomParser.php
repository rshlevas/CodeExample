<?php

namespace App\Parsers\JSONParser\CustomParsers;

use App\Models\Site;
use App\Parsers\Exceptions\ParserException;
use App\Parsers\JSONParser\JSONParser;
use App\Parsers\Services\ModelService;

abstract class CustomParser extends JSONParser
{
    /**
     * @var \Illuminate\Config\Repository|mixed
     */
    protected $priceFilters;

    /**
     * @var
     */
    protected $priceDate;

    /**
     * GeckoMobileParser constructor.
     * @param Site $site
     * @param ModelService $modelService
     */
    public function __construct(Site $site, ModelService $modelService)
    {
        parent::__construct($site, $modelService);
        $this->priceFilters = $this->getPriceFilters();
    }

    /**
     * @return mixed
     */
    protected function getSource()
    {
        return $this->getJSON($this->site->api_link);
    }

    /**
     * Prepare prices in proper view
     *
     * @param array $prices
     * @return array
     */
    protected function preparePrices(array $prices)
    {
        $preparedPrices = [];
        foreach ($prices as $condition => $networkPrices) {
            if (isset($networkPrices['networks'])) {
                $preparedPrices = $this->preparePricesByNetwork($condition, $networkPrices, $preparedPrices);
            } elseif (isset($networkPrices['price'])) {
                $networkPrices['name'] = 'Unlocked';
                $networkPrices['exists'] = 1;
                $preparedPrices[] = $this->prepareSinglePrice($networkPrices, $condition);
            }
        }

        return $preparedPrices;
    }

    /**
     * Get all prices of this condition
     *
     * @param $condition
     * @param $prices
     * @param $preparedPrices
     * @return array
     */
    protected function preparePricesByNetwork($condition, $prices, $preparedPrices)
    {
        foreach ($prices['networks'] as $price)
        {
            $preparedPrices[] = $this->prepareSinglePrice($price, $condition);
        }

        return $preparedPrices;
    }

    /**
     * Prepare price array for further action
     *
     * @param array $price
     * @param string $condition
     * @return array
     */
    protected function prepareSinglePrice(array $price, string $condition)
    {
        $pricePrepared = [
            'price' => $price['price'],
            'currency' => 'GBP',
            'created_at' => $this->priceDate,
            'condition' => $this->getPriceFilterValue($condition, 'condition'),
            'network' => $this->getPriceFilterValue($price['name'], 'network'),
        ];
        if (isset($price['exists']) && (int) $price['exists'] === 1) {
            $pricePrepared['is_exist'] = 1;
        }

        return $pricePrepared;
    }

    /**
     * Try to get value of filter from config file. Throws exception if filter doesn't exist
     *
     * @param $name
     * @param $type
     * @return mixed
     * @throws ParserException
     */
    protected function getPriceFilterValue($name, $type)
    {
        try {
            return $this->priceFilters[$type][$name];
        } catch (\Exception $e) {
            throw new ParserException(
                sprintf('There is no filter with name - %s at %s filters', $name, $type)
            );
        }
    }

    /**
     * Takes price filters prepared proper for this parser from filter.php config file
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    protected function getPriceFilters()
    {
        return config('filter.parsing.' . self::class);
    }

    /**
     * Set price date
     *
     * @param $date
     * @return $this
     */
    protected function setPriceDate($date)
    {
        $this->priceDate = $date;

        return $this;
    }

    /**
     * Change Iphone brand on Apple
     *
     * @param $name
     * @return string
     */
    protected function prepareBrandName($name)
    {
        return strpos(strtolower($name), 'iphone') === 0 ? "Apple" : $name;
    }
}