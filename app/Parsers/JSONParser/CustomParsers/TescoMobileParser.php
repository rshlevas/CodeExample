<?php

namespace App\Parsers\JSONParser\CustomParsers;

class TescoMobileParser extends CustomParser
{
    protected $customFilters = [
        0 => [
            'method' => 'str_replace',
            'needle' => 'GOOGLE',
            'replace' => 'Google',
        ],
        1 => [
            'method' => 'str_replace',
            'needle' => 'HUAWEI',
            'replace' => 'Huawei',
        ],
    ];

    protected function prepareData(array $line)
    {
        $this->setPriceDate($line['updated_at']);

        return [
            'brand' => [
                'name' => $this->prepareBrandName($line['brand']),
            ],
            'product' => [
                'name' => $line['product_name'],
            ],
            'product_profile' => [
                'site_id' => $this->site->id,
            ],
            'link' => [
                'link' => $line['url'],
            ],
            'price' => $this->preparePrices($line['prices']),
        ];
    }
}