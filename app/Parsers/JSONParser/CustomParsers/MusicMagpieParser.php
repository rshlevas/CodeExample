<?php

namespace App\Parsers\JSONParser\CustomParsers;

class MusicMagpieParser extends CustomParser
{
    protected $customFilters = [
        0 => [
            'method' => 'str_replace',
            'needle' => '08gb',
            'replace' => '8gb',
        ],
        1 => [
            'method' => 'str_replace',
            'needle' => '\'',
            'replace' => '',
        ],
        2 => [
            'method' => 'str_replace',
            'needle' => 'HUAWEI',
            'replace' => 'Huawei',
        ],
    ];

    protected function prepareData(array $line)
    {
        $this->setPriceDate($line['updated_at']);

        return [
            'brand' => [
                'name' => $this->prepareBrandName($line['brand']),
            ],
            'product' => [
                'name' => $line['product_name'],
            ],
            'product_profile' => [
                'site_id' => $this->site->id,
            ],
            'link' => [
                'link' => $line['url'],
            ],
            'price' => $this->preparePrices($line['prices']),
        ];
    }

}