<?php

namespace App\Parsers\JSONParser;

use App\Parsers\Parser;

abstract class JSONParser extends Parser
{
    protected function getJSON($link)
    {
        $file = file($link);

        return json_decode($file[0], true);
    }
}