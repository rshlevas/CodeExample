<?php

namespace App\Parsers\Services;

use App\Models\Image;
use App\Models\Product;
use App\Services\ImageServices\Resizable;
use App\Traits\ResourceChecker;
use Illuminate\Support\Facades\Storage;

class ImageService
{
    use Resizable, ResourceChecker;

    /**
     * @param array $params
     * @return bool
     */
    public function create(array $params)
    {
        if ( $this->isImageExist($params['product_id'], isset($params['thumb'])) || ! $this->isSourceExist($params['url'])) {
            return false;
        }

        if ($params = $this->parseImage($params)) {
            return $this->createImage($params);
        }

        return false;
    }

    /**
     * @param $params
     * @return null
     */
    protected function parseImage($params)
    {
        $dirName = implode('_', explode(' ', $params['product_name']));
        $name = $params['brand'] . '/' . $dirName . '/' . str_random(10) . '.jpg';
        $contents = $this->getImageContents($params['url']);
        if ($this->storageImage($name, $contents)) {
            $params['name'] = basename($name);
            $params['path'] = 'storage/images/products/' . $name;
            $this->resizeImages($params);
            return $params;
        }

        return null;
    }

    /**
     * Resize of uploaded images
     *
     * @param $params
     * @return mixed
     */
    protected function resizeImages($params)
    {
        return $this->resize(public_path($params['path']), isset($params['thumb']));
    }

    /**
     * @param string $url
     * @return mixed
     */
    protected function getImageContents(string $url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    /**
     * @param $productId
     * @param bool $thumb
     * @return mixed
     */
    protected function isImageExist($productId, bool $thumb)
    {
        $product = Product::where('id', $productId)
            ->get()
            ->first();

        return $thumb ? $product->imageThumbnail : $product->image;
    }

    /**
     * @param $name
     * @param $contents
     * @return mixed
     */
    protected function storageImage($name, $contents)
    {
        return Storage::disk('productImages')->put($name, $contents);
    }

    /**
     * @param $params
     * @return mixed
     */
    protected function createImage($params)
    {
        $data = [
            'name' => $params['name'],
            'path' => $params['path'],
            'imagable_id' => $params['product_id'],
            'imagable_type' => $params['product_class'],
            'is_thumbnail' => (int) isset($params['thumb']),
        ];

        return Image::create($data);
    }
}