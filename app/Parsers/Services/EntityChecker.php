<?php

namespace App\Parsers\Services;

class EntityChecker extends EntityService
{
    /**
     * Set type of db operation
     */
    protected function setType()
    {
        $this->type = 'where';
    }

    protected function run(array $where)
    {
        $qb = parent::run($where);

        return $qb->get()->first();
    }
}