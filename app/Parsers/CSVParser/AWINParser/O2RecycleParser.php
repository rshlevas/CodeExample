<?php

namespace App\Parsers\CSVParser\AWINParser;

use App\Models\Product;
use Carbon\Carbon;

class O2RecycleParser extends AWINParser
{
    /**
     * @var bool
     */
    protected $imagable = true;

    /**
     * @var string
     */
    protected $zipName = 'o2recycle.zip';

    /**
     * @var array
     */
    protected $customFilters = [
        0 => [
            'method' => 'str_replace',
            'needle' => 'HUAWEI',
            'replace' => 'Huawei',
        ],
        1 => [
            'method' => 'str_replace',
            'needle' => 'GOOGLE',
            'replace' => 'Google',
        ],
    ];

    /**
     * Function that prepared data structure for ModelService
     *
     * @param array $line
     * @return array
     */
    protected function prepareData(array $line)
    {
        return [
            'brand' => [
                'name' => $line['brand_name'],
            ],
            'product' => [
                'name' => $line['description'],
            ],
            'product_profile' => [
                'site_id' => $this->site->id,
            ],
            'link' => [
                'link' => $line['merchant_deep_link'],
            ],
            'price' => [
                0 => [
                    'price' => $line['search_price'],
                    'currency' => $line['currency'],
                    'created_at' => Carbon::today(),
                    'condition' => 1,
                    'network' => 1,
                    'is_exist' => 1,
                ],
            ],
            'images' => [
                0 => [
                    'url' => $line['aw_image_url'],
                    'product_class' => Product::class,
                ],
                1 => [
                    'url' => $line['merchant_image_url'],
                    'product_class' => Product::class,
                    'thumb' => true,
                ],
            ],
        ];
    }
}