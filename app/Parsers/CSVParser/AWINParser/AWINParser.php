<?php

namespace App\Parsers\CSVParser\AWINParser;

use App\Parsers\CSVParser\CSVParser;
use App\Parsers\Exceptions\ParserException;
use Chumper\Zipper\Facades\Zipper;
use Illuminate\Support\Facades\Storage;

abstract class AWINParser extends CSVParser
{
    /**
     * @var string
     */
    protected $zipName = 'zipName.zip';

    /**
     * @var
     */
    protected $csvName;

    /**
     * @var
     */
    protected $zipFilePath;

    /**
     * @var
     */
    protected $csvFilePath;

    /**
     * @return array
     * @throws ParserException
     */
    protected function getSource()
    {
        $data = $this->getFileContents($this->site->api_link);

        if ( ! $this->storageZipFile($this->zipName, $data)) {
            throw new ParserException("Can't connect to the {$this->site->name} api link");
        }

        $this->setFilePath($this->zipName);
        $this->setCSVName();
        Zipper::make($this->zipFilePath)->extractTo($this->csvFilePath);
        $this->csvFilePath = "{$this->csvFilePath}/{$this->csvName}";

        return $this->getCSV($this->csvFilePath);

    }

    /**
     *  set csv file name
     */
    protected function setCSVName()
    {
        $listNames = Zipper::make($this->zipFilePath)->listFiles();
        $this->csvName =$listNames[0];
    }

    /**
     * Set files storage path
     *
     * @param $name
     * @return $this
     */
    protected function setFilePath($name)
    {
        $this->zipFilePath = storage_path("app/public/datafeeds/zip/{$name}");
        $this->csvFilePath = storage_path("app/public/datafeeds/zip/");

        return $this;
    }

    /**
     * @param string $url
     * @return mixed
     */
    protected function getFileContents(string $url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    /**
     * @param $name
     * @param $contents
     * @return mixed
     */
    protected function storageZipFile($name, $contents)
    {
        return Storage::disk('datafeeds')->put($name, $contents);
    }

    /**
     *
     */
    public function __destruct()
    {
        /*Storage::disk('datafeeds')->delete($this->zipName);
        Storage::disk('datafeeds')->delete($this->csvName);*/
    }
}