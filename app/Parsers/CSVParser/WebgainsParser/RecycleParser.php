<?php

namespace App\Parsers\CSVParser\WebgainsParser;

use App\Models\Product;

class RecycleParser extends WebgainsParser
{
    protected $imagable = true;

    /**
     * Function that prepared data structure for ModelService
     *
     * @param array $line
     * @return array
     */
    protected function prepareData(array $line)
    {
        return [
            'brand' => [
                'name' => $line['brand'],
            ],
            'product' => [
                'name' => $line['product_name'],
                'model' => $line['model_number'],
            ],
            'product_profile' => [
                'site_id' => $this->site->id,
            ],
            'link' => [
                'link' => $this->getProductLink($line['deeplink']),
            ],
            'price' => [
                0 => [
                    'price' => $line['price'],
                    'currency' => $line['currency'],
                    'created_at' => $line['last_updated'],
                    'condition' => 1,
                    'network' => 1,
                    'is_exist' => 1,
                ]
            ],
            'images' => [
                0 => [
                    'url' => $line['image_url'],
                    'product_class' => Product::class,
                ],
                1 => [
                    'url' => $line['image_thumbnail_url'],
                    'product_class' => Product::class,
                    'thumb' => true,
                ]
            ],
        ];
    }

    /**
     * @param string $link
     * @return mixed
     */
    protected function getProductLink(string $link)
    {
        $pattern = '/(?:https[\:\/a-zA-z0-9-_\.]+)/';
        $matches = [];
        preg_match($pattern, $link, $matches);

        return $matches[0];
    }
}