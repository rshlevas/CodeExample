<?php

namespace App\Parsers\CSVParser\WebgainsParser;

use App\Parsers\CSVParser\CSVParser;
use App\Parsers\Exceptions\ParserException;

abstract class WebgainsParser extends CSVParser
{
    /**
     * @return array
     * @throws ParserException
     */
    protected function getSource()
    {
        return $this->getCSV($this->site->api_link);
    }
}