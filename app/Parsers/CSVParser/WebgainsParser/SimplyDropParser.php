<?php

namespace App\Parsers\CSVParser\WebgainsParser;

use App\Models\Product;

class SimplyDropParser extends WebgainsParser
{
    /**
     * Turn on imagable mode
     *
     * @var bool
     */
    protected $imagable = true;

    /**
     * @var array
     */
    protected $customFilters = [
        0 => [
            'method' => 'str_ireplace',
            'needle' => 'Huawei2',
            'replace' => 'Huawei',
        ],
        1 => [
            'method' => 'str_ireplace',
            'needle' => 'Alcatel2',
            'replace' => 'Alcatel',
        ],
        2 => [
            'method' => 'str_ireplace',
            'needle' => 'ZTE2',
            'replace' => 'ZTE',
        ],
    ];

    /**
     * Function that prepared data structure for ModelService
     *
     * @param array $line
     * @return array
     */
    protected function prepareData(array $line)
    {
        return [
            'brand' => [
                'name' => $line['brand'],
            ],
            'product' => [
                'name' => $this->prepareProductName($line['product_name']),
                'model' => $line['model_number'],
            ],
            'product_profile' => [
                'site_id' => $this->site->id,
            ],
            'link' => [
                'link' => $this->getProductLink($line['deeplink']),
            ],
            'price' => [
                0 => [
                    'price' => $line['normal_price'],
                    'currency' => $line['currency'],
                    'created_at' => $line['last_updated'],
                    'condition' => 1,
                    'network' => 1,
                    'is_exist' => 1,
                ],
                1 => [
                    'price' => $line['non_working_price'],
                    'currency' => $line['currency'],
                    'created_at' => $line['last_updated'],
                    'condition' => 2,
                    'network' => 1,
                    'is_exist' => 1,
                ],
            ],
            'images' => [
                0 => [
                    'url' => $line['image_url'],
                    'product_class' => Product::class,
                ],
            ],
        ];
    }

    /**
     * @param string $link
     * @return mixed
     */
    protected function getProductLink(string $link)
    {
        $pattern = '/(?:http[\:\/a-zA-z0-9-_\.]+)/';
        $matches = [];
        preg_match_all($pattern, $link, $matches);

        return $matches[0][1];
    }

    /**
     * @param $name
     * @return mixed
     */
    protected function prepareProductName($name)
    {
        return $this->checkNameVariants($name) ? str_replace('Apple', 'Apple Iphone', $name) : $name;
    }

    /**
     * @param $name
     * @return bool
     */
    protected function checkNameVariants($name)
    {
        if (strpos($name, 'Apple') !== false) {
            if (strpos($name, 'Apple Watch') === false && strpos($name, 'Apple Iphone') === false) {
               return true;
            }
        }

        return false;
    }


}