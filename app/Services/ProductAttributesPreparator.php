<?php

namespace App\Services;

use Illuminate\Support\Collection;
use App\Models\Product;

/**
 * Class ProductAttributesPreparator
 *
 * This class could be used for prepare Product model attributes.
 * Before sending for view rendering Product entity or collection
 * should be prepared by this service.
 * This service have two public methods, so it could prepare as single
 * Product entity as Collection of them
 *
 * @package App\Services
 */
class ProductAttributesPreparator
{
    /**
     * Method, that used for preparation of attributes of products in collection
     *
     * @param Collection $products
     * @return Collection
     */
    public function prepareCollection(Collection $products)
    {
        $preparedProduct = collect([]);

        foreach ($products as $product) {
            try {
                $preparedProduct->push($this->prepareSingle($product));
            } catch (\Throwable $e) {
                //Do nothing
            }
        }

        return $preparedProduct;
    }

    /**
     * Method, that used for preparation of attributes of single product entity
     *
     * @param Product $product
     * @return Product
     */
    public function prepareSingle(Product $product)
    {
        $product->setProfilesCount($this->getProfilesCountAttribute($product));
        $product->setBestPrice($this->getBestPriceAttribute($product));
        $product->setShortName($this->getShorter($product, 'name'));
        $product->setShortModel($this->getShorter($product, 'model'));

        return $product;
    }

    /**
     * Prepare best price attribute of Product entity
     *
     * @param Product $product
     * @return mixed
     */
    protected function getBestPriceAttribute(Product $product)
    {
        return count($product->children) ? $this->getBestPriceWithChildren($product) : $this->getBestPrice($product);
    }

    /**
     * Collect and sort best price of the product entity. Prices are taken from
     * all product profiles and select the best between them
     *
     * @param Product $product
     * @return mixed
     */
    protected function getBestPrice(Product $product)
    {
        $prices = collect([]);
        foreach ($product->profiles as $profile) {
            $prices->push($profile->bestWorkingPrice);
        }

        $sorted = $this->sortByPriceDesc($this->removeEmptyPrice($prices));

        return $sorted->first();
    }

    /**
     * Sort collection of prices by price desc
     *
     * @param Collection $prices
     * @return Collection
     */
    protected function sortByPriceDesc(Collection $prices)
    {
        return $prices->sortByDesc(function($price) {
            return $price->price;
        });
    }

    /**
     * Remove prices if they are null
     *
     * @param Collection $prices
     * @return Collection
     */
    protected function removeEmptyPrice(Collection $prices)
    {
        return $prices->filter();
    }

    /**
     * This method is analogue of the getBestPrice method. But it should be used
     * if product entity has children products. So this method select the best price
     * of the product entity among all its profiles and children profiles
     *
     * @param Product $product
     * @return mixed
     */
    protected function getBestPriceWithChildren(Product $product)
    {
        $prices = collect([]);
        $prices->push($this->getBestPrice($product));
        foreach ($product->children as $child) {
            $prices->push($this->getBestPrice($child));
        }

        $sorted = $this->sortByPriceDesc($prices);

        return $sorted->first();
    }

    /**
     * Method that used for preparation of profile_count attribute of product entity
     *
     * @param Product $product
     * @return int
     */
    public function getProfilesCountAttribute(Product $product)
    {
        return count($product->children) ? $this->calculateProfiles($product) : count($product->profiles);
    }

    /**
     * This method used for preparation of profile_count attribute if product entity has children products
     * Method calculates the number of unique product profiles and its children profiles
     *
     * @param Product $product
     * @return int
     */
    protected function calculateProfiles(Product $product)
    {
        $sites = [];
        foreach ($product->children as $child) {
            foreach ($child->profiles as $profile) {
                $sites[] = $profile->site_id;
            }
        }
        foreach ($product->profiles as $profile) {
            $sites[] = $profile->site_id;
        }

        return count(array_unique($sites));
    }

    /**
     * Returns the shorter attribute, if it longer than 28 symbols
     *
     * @param Product $product
     * @param $type
     * @return mixed|string
     */
    protected function getShorter(Product $product, $type)
    {
        return strlen($product->{$type}) > 28
            ? sprintf('%s...', substr($product->{$type}, 0, 25))
            : $product->{$type};
    }
}