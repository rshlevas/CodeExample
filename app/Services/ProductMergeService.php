<?php

namespace App\Services;

use App\Models\Product;
use Illuminate\Support\Collection;

/**
 * Class ProductMergeService
 * This service could be used to manage the product synonyms (as base product, as children products)
 *
 * @package App\Services
 */
class ProductMergeService
{
    /**
     * Returns the children of given product. If product is not the base product, it will return
     * the children of given product parent
     *
     * @param Product $product
     * @return mixed
     */
    public function getChildren(Product $product)
    {
        return $product->parent_id ? $product->parent->children : $product->children;
    }

    /**
     * Returns the parent of given product. If product is base product itself method will return
     * the given product itself
     *
     * @param Product $product
     * @return Product|mixed
     */
    public function getParent(Product $product)
    {
        return $product->parent_id ? $product->parent : $product;
    }

    /**
     * Method returns products, that have the name similar with the given product.
     * Method exclude the given product from collection before return it
     *
     * @param Product $product
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getSimilar(Product $product)
    {
        $searchName = substr($product->name, 0, round(0.5 * strlen($product->name)));

        return Product::with('image', 'imageThumbnail')
            ->where([
                ['name', 'like', "{$searchName}%"],
                ['name', '!=', $product->name]
            ])->doesntHave('children')
            ->doesntHave('parent')
            ->get();
    }

    /**
     * Method, that put synonyms (children) for given products by synonyms ids
     *
     * @param Product $product
     * @param array $synonymsIds
     */
    public function setSynonyms(Product $product, array $synonymsIds)
    {
        if (count($product->children)) {
            $this->removeAllSynonyms($product->children);
        }
        $this->updateParentId($product, $synonymsIds);

        return;
    }

    /**
     * Make all given synonyms independent from their parents
     *
     * @param Collection $synonyms
     */
    protected function removeAllSynonyms(Collection $synonyms)
    {
        foreach ($synonyms as $synonym)
        {
            $synonym->parent_id = null;
            $synonym->save();
        }
    }

    /**
     * Update parent of synonyms. Method should receive ids of synonyms
     * Method will get synonym products by ids itself
     *
     * @param $product
     * @param $synonymsIds
     */
    protected function updateParentId($product, $synonymsIds)
    {
        $synonyms = $this->getSynonymsEntities($synonymsIds);
        foreach ($synonyms as $synonym)
        {
            $this->addSynonym($synonym, $product);
        }
    }

    /**
     * Method that return the synonyms entities by given ids
     *
     * @param array $ids
     * @return mixed
     */
    protected function getSynonymsEntities(array $ids)
    {
        return Product::whereIn('id', $ids)->get();
    }

    /**
     * Connected the given synonym product with parent product
     *
     * @param Product $synonym
     * @param Product $product
     */
    public function addSynonym(Product $synonym, Product $product)
    {
        $synonym->parent_id = $product->id;
        $synonym->save();
    }
}