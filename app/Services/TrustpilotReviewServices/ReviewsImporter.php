<?php

namespace App\Services\TrustpilotReviewServices;

use App\Models\Site;
use App\Models\TrustpilotReview;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class ReviewsImporter
{
    /**
     * @var string
     */
    protected $file;

    /**
     * Keys that should be at json_decode review item
     *
     * @var array
     */
    protected $keys = ['review_text', 'review_title', 'reviewer_name', 'url'];

    /**
     * Result of import
     *
     * @var array
     */
    protected $result = ['sites' => 0, 'reviews' => 0];

    /**
     * Start import function
     *
     * @param $path
     * @return array
     */
    public function import($path)
    {
        $this->configure($path);
        $source = $this->getSource($this->getFilePath($path));
        foreach ($this->getSites() as $site) {
            isset($source[$site->trustpilot_link]) ?
                $this->importLastReviews($source[$site->trustpilot_link]->slice(0, 5), $site) : '';
        }

        return $this->result;
    }

    /**
     * Configure the service properties
     *
     * @param $path
     * @return $this
     */
    protected function configure($path)
    {
        $this->setFile($path);

        return $this;
    }

    /**
     * Import given reviews into DB
     *
     * @param Collection $reviews
     * @param Site $site
     */
    protected function importLastReviews(Collection $reviews, Site $site)
    {
        if (count($site->reviews)) {
            $this->deletePreviousReviews($site);
        }
        foreach ($reviews as $review) {
            $this->importReview($review, $site);
        }

        $this->result['sites']++;
    }

    /**
     * Import single review
     *
     * @param $review
     * @param $site
     * @return bool|int
     */
    protected function importReview($review, $site)
    {
        if (! $this->properDataStructure($review)) {
            return false;
        }

        return $this->saveReview($this->prepareData($review, $site));

    }

    /**
     * Saves review data into DB
     *
     * @param $data
     * @return int
     */
    protected function saveReview($data)
    {
        TrustpilotReview::create($data);

        return $this->result['reviews']++;
    }

    /**
     * Check if given review data has valid structure
     *
     * @param $review
     * @return bool
     */
    protected function properDataStructure($review)
    {
        $params = array_keys($review);
        sort($params);

        return $this->keys === $params;
    }

    /**
     * Prepare given review data for next insertion into DB
     *
     * @param $review
     * @param $site
     * @return array
     */
    protected function prepareData($review, $site)
    {
        return [
            'author' => $review['reviewer_name'],
            'title' => $review['review_title'],
            'content' => $review['review_text'],
            'site_id' => $site->id,
        ];
    }

    /**
     * Get all sites entities
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function getSites()
    {
        return Site::with('reviews')->get();
    }

    /**
     * Delete site previous reviews
     *
     * @param Site $site
     */
    protected function deletePreviousReviews(Site $site)
    {
        foreach ($site->reviews as $review) {
            $review->delete();
        }
    }

    /**
     * Get source from upload file.
     *
     * @param $path
     * @return static
     */
    protected function getSource($path)
    {
        $file = file_get_contents($path);
        $json = json_decode($file, true);

        return collect($json)->groupBy('url');
    }


    /**
     * Set file path, that should be used for deleting file during destruction of the class
     *
     * @param $file
     * @return $this
     */
    protected function setFile($file)
    {
        $path = str_replace('storage', 'public', $file);
        $this->file = $path;

        return $this;
    }

    /**
     * Generate file path, that will be used as source
     *
     * @param $path
     * @return $this
     */
    protected function getFilePath($path)
    {
        $fileName = basename($path);

        return  storage_path(sprintf('app/public/reviews/%s', $fileName));
    }

    /**
     * Class destructor
     */
    public function __destruct()
    {
        Storage::delete($this->file);
    }
}