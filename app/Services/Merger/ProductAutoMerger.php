<?php

namespace App\Services\Merger;

use App\Services\ProductEntityService;
use phpDocumentor\Reflection\Types\Null_;

class ProductAutoMerger
{
    /**
     * @var ProductEntityService
     */
    protected $productManager;

    public function __construct(ProductEntityService $service)
    {
        $this->productManager = $service;
    }

    public function getMergeList()
    {
        $list = collect([]);

        foreach ($this->baseProducts as $product) {
           $list = $list->merge($this->getMergeListForSingle($product));
        }

        return $list;

    }

    protected function getMergeListForSingle($params)
    {
        $baseProduct = $this->getBaseProduct($params['name']);

        if ($baseProduct->size) {
            $sizeRelatedProducts = $this->productManager->getProductsByParamsWithBaseRelations([
                ['size_group', '=', $baseProduct->size_group],
                ['id', '!=', $baseProduct->id]
            ]);
        }

        $products = $baseProduct->size ? $sizeRelatedProducts->push($baseProduct) : collect([0 => $baseProduct]);

        $where = [];
        foreach ($params['except'] as $param) {
            $where[] = ['name', 'NOT LIKE', "%$param%"];
        }

        $prepared = [];
        foreach ($products as $product) {
            $prepared[$product->name] = $this->searchByWords($product, $where);
        }

        return $prepared;
    }

    protected function searchByWords($product, $where)
    {
        $nameParts = explode(' ', $product->size_group_display ? $product->size_group_display : $product->name);
        array_shift($nameParts);
        $products = collect([]);

        foreach ($nameParts as $namePart) {
            $params = $where;
            $params[] = ['brand_id', '=', $product->brand_id];
            $params[] = ['id', '!=', $product->id];
            $params[] = ['name', 'LIKE', "%$namePart%"];
            $params[] = ['size', '=', $product->size];

            if($product->size_group) {
                $params[] = ['size_group', '!=', $product->size_group];
            }
            $products = $products->merge($this->productManager->getProductsByParamsWithBaseRelations($params));
        }



        $groupProducts = $products->groupBy('id')->filter(function ($collection) use ($nameParts) {
            return count($collection) === count($nameParts);
        });

        $synonyms = collect([]);

        foreach ($groupProducts as $collection) {
            $synonyms = $synonyms->merge($collection);
        }

        return $synonyms->unique('id');
    }

    protected function getBaseProduct($name)
    {
        return $this->productManager->getProductsByParamsWithBaseRelations([0 => ['name', '=', $name]])->first();
    }

    protected $baseProducts = [
        ['name' => 'Samsung Galaxy S3 Neo', 'except' => ['mini', 'tab', 'note']],
        ['name' => 'Samsung Galaxy S3 Mini', 'except' => ['neo', 'tab', 'note'], 'with' => 'S III'],
        ['name' => 'Samsung Galaxy S3 16GB', 'except' => ['neo', 'mini', 'tab', 'note']],
        ['name' => 'Samsung Galaxy S3', 'except' => ['neo', 'mini', 'tab', 'note', 'ace'], 'with' => 'S III'],
        ['name' => 'Samsung Galaxy S5 Neo', 'except' => ['duos', 'plus', 'zoom', 'active', 'mini', 'tab', 'note']],
        ['name' => 'Samsung Galaxy S5 Active', 'except' => ['neo', 'duos', 'plus', 'zoom', 'mini', 'tab', 'note']],
        ['name' => 'Samsung Galaxy S5 Plus', 'except' => ['neo', 'duos', 'zoom', 'active', 'mini', 'tab', 'note']],
        ['name' => 'Samsung Galaxy S5', 'except' => ['neo', 'duos', 'plus', 'zoom', 'active', 'mini', 'tab', 'note', 'ace']],
        ['name' => 'Samsung Galaxy S9 Plus', 'except' => ['mini', 'tab', 'note']],
        ['name' => 'Samsung Galaxy S9', 'except' => ['edge', 'plus', 'mini', 'tab', 'note']],
        ['name' => 'Samsung Galaxy S9 Plus 64GB', 'except' => ['edge', 'mini', 'tab', 'note']],
        ['name' => 'Samsung Galaxy S9 64GB', 'except' => ['edge', 'plus', 'mini', 'tab', 'note']],
        ['name' => 'Samsung Galaxy S8 64GB', 'except' => ['edge', 'plus', 'mini', 'tab', 'note']],
        ['name' => 'Samsung Galaxy S8 Plus 64GB', 'except' => ['mini', 'tab', 'note']],
        ['name' => 'Samsung Galaxy S8', 'except' => ['edge', 'plus', 'mini', 'tab', 'note']],
        ['name' => 'Samsung Galaxy S8 Plus', 'except' => ['mini', 'tab', 'note']],
        ['name' => 'Samsung Galaxy S7 32GB', 'except' => ['edge', 'plus', 'mini', 'tab', 'note']],
        ['name' => 'Samsung Galaxy S7 Edge 32GB', 'except' => ['plus', 'mini', 'tab', 'note']],
        ['name' => 'Samsung Galaxy S6 32GB', 'except' => ['edge', 'plus', 'mini', 'tab', 'note']],
        ['name' => 'Samsung Galaxy S6 Edge 32GB', 'except' => ['plus', 'mini', 'tab', 'note']],
        ['name' => 'Samsung Galaxy S6 Edge Plus 32GB', 'except' => ['mini', 'tab', 'note']],
    ];
}