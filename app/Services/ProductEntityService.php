<?php

namespace App\Services;

use App\Models\Product;

/**
 * Class ProductEntityService
 * This class is service that could be used to get product entities from DB
 * This method get product with eager loading of their relations
 *
 * @package App\Services
 */
class ProductEntityService
{
    /**
     * Name of product entity class
     *
     * @var string
     */
    protected $className = Product::class;

    /**
     * Take products from DB by IDs
     *
     * @param array $ids
     * @param null $number
     * @return mixed
     */
    public function getProductsByIds(array $ids, $number = null)
    {
        return $this->getBuilder($number)
            ->whereIn('id', $ids)
            ->doesntHave('parent')
            ->get();
    }

    /**
     * Take products from DB by given column and its value
     *
     * @param $column
     * @param $value
     * @param null $number
     * @return mixed
     */
    public function getProductByParam($column, $value, $number = null)
    {
        return $this->getBuilder($number)
            ->where($column, $value)
            ->doesntHave('parent')
            ->get();
    }

    /**
     * Get products from DB where their names like given param
     *
     * @param $key
     * @param null $number
     * @return mixed
     */
    public function getProductsLike($key, $number = null)
    {
        return $this->getBuilder($number)
            ->where('name', 'like', "%{$key}%")
            ->doesntHave('parent')
            ->get();
    }

    /**
     * Get products from DB by params
     *
     * @param $params
     * @param null $number
     * @return mixed
     */
    public function getProductsByParams($params, $number = null)
    {
        return $this->getBuilder($number)
            ->where($params)
            ->doesntHave('parent')
            ->get();
    }

    /**
     * Get related products by given product name. Params array should contains additional where parameters in a form
     * of array:
     * $params = [
     *      ['brand_id', '=', 'Samsung']
     * ]
     * $decreaseIndex could be used to filter the results by relevance. By default it 0
     *
     * @param string $name
     * @param array $params
     * @param int $decreaseIndex
     * @param bool $relations
     * @return \Illuminate\Support\Collection|static
     */
    public function getRelatedProducts(string $name, array $params, int $decreaseIndex = 0, $relations = true)
    {
        $nameParts = explode(' ', $name);
        array_shift($nameParts);
        $products = collect([]);

        foreach ($nameParts as $namePart) {
            $where = [
                ['name', 'LIKE', "%{$namePart}%"],
                ['name', '!=', $name]
            ];
            $products = $relations ? $products->merge($this->getProductsByParams(array_merge($where, $params)))
                : $products->merge($this->getProductsByParamsWithBaseRelations(array_merge($where, $params)));
        }
        $groupProducts = $products->groupBy('id')->filter(function ($collection) use ($nameParts, $decreaseIndex) {
            return count($collection) >= count($nameParts) - $decreaseIndex;
        });
        if (! count($groupProducts)) {
            return collect([]);
        }

        $synonyms = collect([]);

        foreach ($groupProducts as $collection) {
            $synonyms = $synonyms->merge($collection);
        }

        return $synonyms->unique();
    }

    /**
     * Get products from DB by params
     *
     * @param $params
     * @return mixed
     */
    public function getProductsByParamsWithBaseRelations($params)
    {
        return $this->getBuilderWithoutRelations()
            ->where($params)
            ->doesntHave('parent')
            ->get();
    }

    /**
     * Select which builder return. If number variable is given, it will return
     * the builder, configure to take the number of entities. Without number, builder will
     * prepare to return all values
     *
     * @param null $number
     * @return mixed
     */
    protected function getBuilder($number = null)
    {
        return $number ? $this->getBuilderWithTake($number) : $this->getBaseBuilder();
    }

    /**
     * Create base builder, with loading of the all product relations
     *
     * @return mixed
     */
    protected function getBaseBuilder()
    {
        return $this->className::with([
            'profiles',
            'image',
            'imageThumbnail',
            'profiles.bestWorkingPrice',
            'brand',
            'category',
            'children',
            'children.profiles',
            'children.profiles.bestWorkingPrice'
        ]);
    }

    protected function getBuilderWithoutRelations()
    {
        return $this->className::with([
            'brand',
            'category',
        ]);
    }

    /**
     * @param int $number
     * @return mixed
     */
    protected function getBuilderWithTake(int $number)
    {
        return $this->getBaseBuilder()->take($number);
    }
}