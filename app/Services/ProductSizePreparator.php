<?php

namespace App\Services;

use Illuminate\Support\Collection;

class ProductSizePreparator
{
    /**
     * @param Collection $productsToPrepare
     * @return Collection
     */
    public function prepare(Collection $productsToPrepare)
    {
        $products = $this->groupBySize($productsToPrepare);
        if (isset($products[''])) {
            $productsWithoutSizes = $products['']->groupBy('name');
            unset($products['']);
        }

        $productsPrepared = collect([]);
        isset($productsWithoutSizes) ? $productsPrepared = $productsPrepared->merge($productsWithoutSizes) : '';

        return $productsPrepared->merge($products);
    }

    /**
     * @param Collection $products
     * @return Collection
     */
    protected function groupBySize(Collection $products)
    {
        return $products->sortBy('size', SORT_NATURAL)
            ->groupBy('size_group_display');
    }

}