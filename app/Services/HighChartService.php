<?php

namespace App\Services;

use App\Models\PositiveReport;
use App\Models\Site;

class HighChartService
{
    /**
     * Data get from DB
     *
     * @var array
     */
    protected $data = [];

    /**
     * Series, used for chart data preparation
     *
     * @var array
     */
    protected $series = [
        [
            'name' => 'passed',
            'data' => [],
            'color' => '#228B22'
        ],
        [
            'name' => 'failed',
            'data' => [],
            'color' => '#dc143c'
        ]
    ];

    /**
     * Final charts data used for charts rendering
     *
     * @var array
     */
    protected $charts = [
        'chart' => ['type' => 'line'],
        'title' => ['text' => 'Positive/Negative reports Dynamics'],
        'xAxis' => [
            'categories' => null,
        ],
        'yAxis' => [
            'title' => [
                'text' => 'Number of reports'
            ]
        ],
        'series' => null
    ];

    /**
     * Generate charts data, that used for chart rendering
     *
     * @return array
     */
    public function generate()
    {
        return $this->getChartsData($this->getAllSites());
    }

    /**
     * Take all Sites entities from DB
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function getAllSites()
    {
        return Site::with('passedReports', 'failedReports')->get();
    }

    /**
     * Prepare charts data and return its
     *
     * @param $sites
     * @return array
     */
    protected function getChartsData($sites)
    {
        $data = $this->getData($sites);
        $dates = array_keys($data);
        $series = $this->getSeries($data);

        return $this->getCharts($series, $dates);
    }

    /**
     * Get main data according to DB request result
     *
     * @param $sites
     * @return array
     */
    protected function getData($sites)
    {
        foreach ($sites as $site) {
            $this->getDataFromSite($site);
        }

        return $this->data;
    }

    /**
     * Get necessary data from site entity
     *
     * @param $site
     */
    protected function getDataFromSite($site)
    {
        foreach ($site->all_reports as $report) {
            $this->getDataFromReport($report);
        }
    }

    /**
     * Get necessary data from parse report entity
     *
     * @param $report
     * @return int
     */
    protected function getDataFromReport($report)
    {
        if ($report instanceof PositiveReport) {
            return $this->setData($report->created_at->format('d-m-Y'), 'passed');
        }

        return $this->setData($report->created_at->format('d-m-Y'), 'failed');
    }

    /**
     * Set proper data into variable
     *
     * @param $date
     * @param $type
     * @return int
     */
    protected function setData($date, $type)
    {
        if (isset($this->data[$date][$type])) {
            return $this->data[$date][$type]++;
        }

        return $this->data[$date][$type] = 1;
    }

    /**
     * Prepare charts from base variable and return proper charts
     *
     * @param array $series
     * @param $dates
     * @return array
     */
    protected function getCharts(array $series, $dates)
    {
        $charts = $this->charts;
        $charts['xAxis']['categories'] = $dates;
        $charts['series'] = $series;

        return $charts;
    }

    /**
     * Prepare series from base variable
     *
     * @param array $data
     * @return array
     */
    protected function getSeries(array $data)
    {
        $series = $this->series;

        foreach ($data as $item) {
            $series[0]['data'][] = isset($item['passed']) ? $item['passed'] : 0;
            $series[1]['data'][] = isset($item['failed']) ? $item['failed'] : 0;
        }

        return $series;
    }
}