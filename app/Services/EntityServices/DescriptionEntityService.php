<?php

namespace App\Services\EntityServices;

use App\Models\Description;
use App\Models\Product;
use App\Models\Site;

class DescriptionEntityService extends BaseEntityService
{
    /**
     * @var array
     */
    protected $models = [
        'site' => Site::class,
        'product' => Product::class,
        'description' => Description::class,
    ];

    protected $relations = [
        'site' => 'descriptions',
        'product' => 'descriptions'
    ];

    protected $searchColumns = [
        'site' => 'url_name',
        'product' => 'url_name',
    ];

    /**
     * @param $type
     * @param $name
     * @return mixed
     */
    public function getModelByTypeAndName($type, $name)
    {
        return $this->getByParamsAndType($this->getParamsByType($type, $name), $type);
    }

    /**
     * @param $type
     * @param $name
     * @return array
     */
    protected function getParamsByType($type, $name)
    {
        return [$this->searchColumns[$type] => $name];
    }

}