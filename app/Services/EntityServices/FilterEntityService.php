<?php

namespace App\Services\EntityServices;

use App\Models\Network;
use App\Models\Condition;

class FilterEntityService extends BaseEntityService
{
    /**
     * @var array
     */
    protected $models = [
        'network' => Network::class,
        'condition' => Condition::class,
    ];
}