<?php

namespace App\Services;

use App\Models\Product;
use Illuminate\Support\Collection;

/**
 * Class ProductSizeRelationSetter
 * This class could be used to combine given product with other products
 * in group combine by their size
 *
 * @package App\Services
 */
class ProductSizeRelationSetter
{
    /**
     * Start the set of the size related products
     *
     * @param Product $product
     * @return bool
     */
    public function set(Product $product)
    {
        $related = $this->getRelated($product);

        return count($related) ? $this->setRelated($product, $related) : false;
    }

    /**
     * Looks for related products by size group at DB
     *
     * @param $product
     * @return mixed
     */
    protected function getRelated($product)
    {
        return Product::where([
            ['size_group', $product->size_group],
            ['id', '!=', $product->id]
        ])->get();
    }

    /**
     * Set the related products at DB
     *
     * @param Product $product
     * @param Collection $related
     * @return bool
     */
    protected function setRelated(Product $product, Collection $related)
    {
        foreach ($related as $relatedProduct)
        {
            $product->sizeRelated()->save($relatedProduct);
            $relatedProduct->sizeRelated()->save($product);
        }

        return true;
    }
}