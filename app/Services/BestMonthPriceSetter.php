<?php

namespace App\Services;

use App\Models\MonthBestPrice;
use App\Models\ProductPrice;

class BestMonthPriceSetter
{
    protected $className = MonthBestPrice::class;

    /**
     * @param ProductPrice $price
     * @return bool|mixed
     */
    public function set(ProductPrice $price)
    {
        if (! $this->checkPriceParams($price)) {
            return false;
        }
        if (! $this->checkIfExist($price)) {
            return $this->setEntity($price, true);
        }

        if (! $this->checkIfPriceValueLarger($price)) {
            return $this->setEntity($price);
        }

        return false;
    }

    protected function checkPriceParams(ProductPrice $price)
    {
        return $price->condition === 1 && $price->network === 1;
    }

    protected function  checkIfPriceValueLarger(ProductPrice $price)
    {
        $bestPrice = $this->getEntity($price);

        return $bestPrice->price >= $price->price;
    }

    protected function setEntity(ProductPrice $price, $new = false)
    {
        return $new ? $this->create($price) : $this->update($price);
    }

    protected function create($price)
    {
        return $this->className::create([
            'price' => $price->price,
            'product_id' => $price->productProfile->product->id
        ]);
    }

    protected function update($price)
    {
        $bestPrice = $this->getEntity($price);
        $bestPrice->update([
            'price' => $price->price,
        ]);

        return $bestPrice;
    }

    protected function checkIfExist(ProductPrice $price)
    {
        return $this->getEntity($price);
    }

    protected function getEntity(ProductPrice $price)
    {
        return $monthBestPrice = $price
            ->productProfile
            ->product
            ->getMonthBestPrice($price->updated_at->year, $price->updated_at->month);
    }
}