<?php

namespace App\Services\ImageServices;

use App\Services\ImageServices\Extractors\ExtractorFactory;
use Illuminate\Support\Facades\Storage;

class ImageImporter
{
    /**
     * @var
     */
    protected $zipFile;

    /**
     * @var
     */
    protected $zipFilePath;

    /**
     * @var ExtractorFactory
     */
    protected $factory;

    /**
     * ImageImporter constructor.
     * @param ExtractorFactory $factory
     */
    public function __construct(ExtractorFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * Starts the image import. It use the path of the uploaded zip file
     * and the alias of the entity as params
     *
     * @param $path
     * @param $entity
     * @return mixed
     */
    public function import($path, $entity)
    {
        $this->configure($path);
        $extractor = $this->getExtractor($entity);

        return $extractor->extract($this->zipFilePath);
    }

    /**
     * Get the necessary extractor by entity alias
     *
     * @param $entity
     * @return mixed
     */
    protected function getExtractor($entity)
    {
        return $this->factory->get($entity);
    }

    /**
     * Set the zipfile and its path, that will be used for extraction and deleting of zip files
     *
     * @param $path
     */
    protected function configure($path)
    {
        $this->setZipFile($path)->setZipFilePath($path);
    }

    /**
     * Set the zip file path, that will be used by extractor
     *
     * @param $path
     * @return $this
     */
    protected function setZipFilePath($path)
    {
        $fileName = basename($path);
        $this->zipFilePath = storage_path(sprintf('app/public/images/import/%s', $fileName));

        return $this;
    }

    /**
     * Set sip file path, that should be used for deleting the zip file during destruction of the class
     *
     * @param $zipFile
     * @return $this
     */
    protected function setZipFile($zipFile)
    {
        $path = str_replace('storage', 'public', $zipFile);
        $this->zipFile = $path;

        return $this;
    }

    /**
     * Class destructor
     */
    public function __destruct()
    {
        Storage::delete($this->zipFile);
    }
}