<?php

namespace App\Services\ImageServices\Extractors;

use App\Services\ImageServices\Resizable;
use App\Models\Image;
use Chumper\Zipper\Facades\Zipper;
use Chumper\Zipper\Zipper as BaseZipper;

abstract class BaseExtractor implements ExtractorInterface
{
    use Resizable;

    /**
     * @var array
     */
    protected $model = [];

    /**
     * Directory path of the zip file
     *
     * @var string*
     */
    protected $dirPath;

    /**
     * Directory paht of the image files
     *
     * @var
     */
    protected $imageDir;

    /**
     * Image model class name
     *
     * @var string
     */
    protected $imageClass = Image::class;

    /**
     * Array with default params for image resizing
     *
     * @var array
     */
    protected $resizeParams = [
        'width' => 100,
        'height' => 100
    ];

    /**
     * BaseExtractor constructor.
     */
    public abstract function __construct();

    /**
     * Function that start extraction of the images from zip files and
     * connection them with related entities
     *
     * @param $filePath
     * @return array
     */
    public function extract($filePath)
    {
        $allImages = $this->getNames($filePath);
        $filteredImages = $this->filterNames($allImages);
        $this->extractByNames($filePath, $filteredImages);

        return $this->saveImages($filteredImages);

    }

    /**
     * Function that saves images at DB and at HDD
     *
     * @param $names
     * @return array
     */
    protected function saveImages($names)
    {
        $models = [];

        foreach ($names as $name) {
            $entity = $this->getEntity($this->getCheckParam($name));
            $this->saveImage($entity, $name);
            $models[] = $entity;
        }

        return $models;
    }

    /**
     * Saves single image
     *
     * @param $entity
     * @param $name
     * @return bool
     */
    protected function saveImage($entity, $name)
    {
        if ($entity->image && $this->previousImagePreparation($entity, $name)) {
            return true;
        }
        $image = $this->getImageEntity($name);
        $this->resize($image->path, false, $this->resizeParams['width'], $this->resizeParams['height']);

        return $entity->image()->save($image);
    }


    /**
     * Function that looks for existing images of given entity and delete them if their names and
     * dislocation are different from novel
     *
     * @param $entity
     * @param $name
     * @return bool
     */
    protected function previousImagePreparation($entity, $name)
    {
        if ($entity->image->name !== $name) {
            $entity->image->delete();
            return false;
        } else {
            $this->resize($entity->image->path, false, $this->resizeParams['width'], $this->resizeParams['height']);
            return true;
        }
    }

    /**
     * Creates new image entity and push necessary parameters to it
     *
     * @param $name
     * @param bool $thumb
     * @return mixed
     */
    protected function getImageEntity($name, $thumb = false)
    {
        $image = new $this->imageClass();
        $image->path = sprintf('%s%s', $this->imageDir, $name);
        $image->name = sprintf('%s', $name);
        $image->is_thumbnail = (int)$thumb;

        return $image;
    }

    /**
     * Extract files from zip file by the given names
     *
     * @param $filePath
     * @param array $names
     */
    protected function extractByNames($filePath, array $names)
    {
        Zipper::make($filePath)->extractTo($this->dirPath, $names, BaseZipper::WHITELIST | BaseZipper::EXACT_MATCH);
    }

    /**
     * Filter the file names, and left only those that could be connected with entities form DB
     *
     * @param $namesList
     * @return array
     */
    protected function filterNames($namesList)
    {
        $filteredNames = [];
        foreach ($namesList as $name) {
            $this->getEntity($this->getCheckParam($name)) ? $filteredNames[] = $name : '';
        }

        return $filteredNames;
    }

    /**
     * Get checking param from image name, that will be used for related entity search
     *
     * @param $param
     * @return mixed
     */
    protected function getCheckParam($param)
    {
        return str_replace(['.png', '.jpg'], '', $param);
    }

    /**
     * Search for entity by the given param
     *
     * @param $param
     * @return mixed
     */
    protected function getEntity($param)
    {
        return $this->model['className']::where($this->model['column'], $param)->get()->first();
    }

    /**
     * Returns names of files, situated at zip file
     *
     * @param $filePath
     * @return mixed
     */
    protected function getNames($filePath)
    {
        return Zipper::make($filePath)->listFiles();
    }
}