<?php

namespace App\Services\ImageServices\Extractors;

interface ExtractorInterface
{
    public function __construct();

    public function extract($filePath);
}