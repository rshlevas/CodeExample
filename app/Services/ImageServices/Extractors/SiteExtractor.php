<?php

namespace App\Services\ImageServices\Extractors;

use App\Models\Image;
use App\Models\Site;
use Chumper\Zipper\Facades\Zipper;
use Chumper\Zipper\Zipper as BaseZipper;

class SiteExtractor extends BaseExtractor
{
    /**
     * @var array
     */
    protected $model = [
        'className' => Site::class,
        'column' => 'url_name'
    ];

    /**
     * SiteExtractor constructor.
     */
    public function __construct()
    {
        $this->dirPath = storage_path('app/public/images/logos/sites/');
        $this->imageDir = 'storage/images/logos/sites/';
    }
}