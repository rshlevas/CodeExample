<?php

namespace App\Services\ImageServices\Extractors;

use App\Models\Product;

class ProductExtractor extends BaseExtractor
{
    /**
     * @var array
     */
    protected $resizeParams = [
        'general' => [
            'width' => 280,
            'height' => 400,
        ],
        'thumb' => [
            'width' => 70,
            'height' => 100,
        ],
    ];

    /**
     * @var array
     */
    protected $model = [
        'className' => Product::class,
        'column' => 'url_name'
    ];

    /**
     * ProductExtractor constructor.
     */
    public function __construct()
    {
        $this->dirPath = storage_path('app/public/images/products/import/');
        $this->imageDir = 'storage/images/products/import/';
    }

    /**
     * Saves image into DB and at HDD
     *
     * @param $entity
     * @param $name
     * @return bool
     */
    protected function saveImage($entity, $name)
    {
        if ($entity->image && $this->previousImagePreparation($entity, $name)) {
            return true;
        }

        $image = $this->getImageEntity($name);
        $this->copyImage($image->path, $this->getThumbName($name));
        $imageThumbnail = $this->getImageEntity($this->getThumbName($name), true);
        $this->resizeImage($image->path, false);
        $this->resizeImage($imageThumbnail->path, true);
        $entity->imageThumbnail()->save($imageThumbnail);

        return $entity->image()->save($image);
    }

    /**
     * Make a copy of image, that will be used for further thumbnail
     *
     * @param $path
     * @param $name
     * @return bool
     */
    protected function copyImage($path, $name)
    {
        return copy(public_path($path), public_path(sprintf('%s%s', $this->imageDir, $name)));
    }

    /**
     * Generate name of the thumbnail image
     *
     * @param $name
     * @return mixed
     */
    protected function getThumbName($name)
    {
        return str_replace('.', '_thumb.', $name);
    }

    /**
     * Delete previous images of the entity if their name are differed from novel ones
     *
     * @param $entity
     * @param $name
     * @return bool
     */
    protected function previousImagePreparation($entity, $name)
    {
        if ($entity->image->name !== $name) {
            $entity->image->delete();
            $entity->imageThumbnail ? $entity->imageThumbnail->delete() : '';
            return false;
        } else {
            $this->resizeImage($entity->image->path, false);
            $this->resizeImage($entity->imageThumbnail->path, true);
            return true;
        }
    }

    /**
     * Function that used for resizing of the images
     *
     * @param $path
     * @param $thumb
     * @return mixed
     */
    protected function resizeImage($path, $thumb)
    {
        $width = $thumb ? $this->resizeParams['thumb']['width'] : $this->resizeParams['general']['width'];
        $height = $thumb ? $this->resizeParams['thumb']['height'] : $this->resizeParams['general']['height'];

        return $this->resize($path, $thumb, $width, $height);
    }

}