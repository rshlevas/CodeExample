<?php

namespace App\Services\ImageServices\Extractors;

class ExtractorFactory
{
    /**
     * Extractor classes
     *
     * @var array
     */
    protected $extractors = [
        'sites' => SiteExtractor::class,
        'brands' => BrandExtractor::class,
        'products' => ProductExtractor::class,
    ];

    /**
     * Try to create extractor by given alias
     *
     * @param $alias
     * @return mixed
     * @throws \Exception
     */
    public function get($alias)
    {
        try {
            return new $this->extractors[$alias];
        } catch (\Exception $e) {
            throw new \Exception(sprintf('Cannot get extractor for %s', $alias));
        }
    }
}