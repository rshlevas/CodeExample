<?php

namespace App\Services\ImageServices\Extractors;

use App\Models\Brand;

class BrandExtractor extends BaseExtractor
{
    /**
     * @var array
     */
    protected $model = [
        'className' => Brand::class,
        'column' => 'url_name'
    ];

    /**
     * BrandExtractor constructor.
     */
    public function __construct()
    {
        $this->dirPath = storage_path('app/public/images/logos/brands/');
        $this->imageDir = 'storage/images/logos/brands/';
    }
}