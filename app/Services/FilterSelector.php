<?php

namespace App\Services;

use App\Models\Condition;
use App\Models\Network;
use App\Models\Product;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;

class FilterSelector
{
    /**
     * @var
     */
    protected $filtersInfo;

    /**
     *
     */
    protected $filterEntities = ['condition' => Condition::class, 'network' => Network::class];

    /**
     * FilterSelector constructor.
     */
    public function __construct()
    {
        $this->configure();
    }

    /**
     * @param Product $product
     * @return array
     */
    public function run(Product $product)
    {
        $product->load(['profiles', 'profiles.prices', 'children', 'children.profiles', 'children.profiles.prices']);
        $prices = count($product->children) ? $this->getPricesWithChildren($product) : $this->getPrices($product);

        return $this->orderByAsc($this->getFilters($prices));

    }

    /**
     * @param array $filters
     * @return array]
     */
    protected function orderByAsc(array $filters)
    {
        $sorted = [];

        foreach ($filters as $key => $filter) {
            uasort($filter, function ($a, $b) {
                if ($a['order'] === $b['order']) {
                    return 0;
                }
                return ($a['order'] < $b['order']) ? -1 : 1;
            });
            $sorted[$key] = $filter;
        }

        return $sorted;
    }

    /**
     * @param $product
     * @return Collection|static
     */
    protected function getPricesWithChildren($product)
    {
        $prices = $this->getPrices($product);
        foreach ($product->children as $child) {
            $prices = $prices->merge($this->getPrices($child));
        }

        return $prices;
    }

    /**
     * @param $product
     * @return Collection
     */
    protected function getPrices($product)
    {
        $prices = collect([]);
        $profiles = $product->profiles;
        foreach ($profiles as $profile) {
            foreach ($profile->prices as $price) {
                $prices->push($price);
            }
        }

        return $prices;
    }

    /**
     * @param Collection $prices
     * @return array
     */
    protected function getFilters(Collection $prices)
    {
        $filters = [];
        foreach ($this->filtersInfo as $param) {
            $filters[$param['column']] = $this->getFilter($prices, $param['column']);
        }

        return $this->filter($filters);
    }

    /**
     * @param $filters
     * @return array
     */
    protected function filter($filters)
    {
        $result = [];
        foreach ($filters as $name => $filter) {
            foreach ($filter as $item) {
                if (isset($this->filtersInfo[$name]['filters'][$item])) {
                    $result[$name][] = $this->filtersInfo[$name]['filters'][$item];
                }
            }
        }

        return $result;
    }

    /**
     * @param $prices
     * @param $filter
     * @return mixed
     */
    protected function getFilter($prices, $filter)
    {
        return $prices->map(function($price, $key) use ($filter) {
            return $price->{$filter};
        })->unique()->sort()->toArray();
    }

    /**
     * @return void
     */
    protected function configure()
    {
        $this->filtersInfo = config('filter.services.' . get_class($this));
        $this->setFiltersParams();
    }

    /**
     * @return void
     */
    protected function setFiltersParams()
    {
        foreach ($this->filterEntities as $alias => $className)
        {
            $this->setFilterParams($alias, $className);
        }
    }

    /**
     * @param $alias
     * @param $className
     * @return array|bool
     */
    protected function setFilterParams($alias, $className)
    {
        $filters = [];
        foreach ($this->getFilterFromDB($className) as $filter) {
            $filters[$filter['key']] = $filter;
        }

        return empty($filters) ? false : $this->filtersInfo[$alias]['filters'] = $filters;
    }

    /**
     * @return mixed
     */
    protected function getFilterFromDB($className)
    {
        return $className::select(['name', 'key', 'alias', 'order'])
            ->where([
                'lang' => App::getLocale(),
                'is_active' => 1,
            ])->orderBy('order')
            ->get()
            ->toArray();
    }


}