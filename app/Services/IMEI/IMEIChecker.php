<?php

namespace App\Services\IMEI;

class IMEIChecker
{
    /**
     * Site api lonk
     *
     * @var string
     */
    protected $link;

    /**
     * Api key
     *
     * @var string
     */
    protected $key;

    /**
     * IMEIChecker constructor.
     */
    public function __construct()
    {
        $this->configure();
    }

    /**
     * Returns phone information in form of array, or array with error message if the
     * imei was incorrect or the were problems with connection
     *
     * @param $param
     * @return array|mixed|object
     */
    public function getPhone($param)
    {
        return $this->execute($param);

    }

    /**
     * Execute request and returns decoded response form imei api
     *
     * @param $param
     * @return array|mixed|object
     */
    protected function execute($param)
    {
        $response = $this->makeRequest($param);

        return json_decode($response, true);

    }

    /**
     * Prepared request body in form of query
     *
     * @param $param
     * @return string
     */
    protected function prepareRequestBody($param)
    {
        return http_build_query(['imei' => $param, 'key' => $this->key]);
    }

    /**
     * Makes request, closes connection and returns response
     *
     * @param $param
     * @return resource
     */
    protected function makeRequest($param)
    {
        $request = $this->prepareRequest($param);
        $response = curl_exec ($request);
        curl_close($request);

        return $response;
    }

    /**
     * Prepare request for executing
     *
     * @param $param
     * @return resource
     */
    protected function prepareRequest($param)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$this->link);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->prepareRequestBody($param));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        return $ch;
    }

    /**
     * Configure the service for proper work
     *
     * @return void
     */
    protected function configure()
    {
        $params = $this->getParams();
        $this->setKey($params['key'])->setLink($params['link']);
    }

    /**
     * Returns necessary params. Takes if form configuration file
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    protected function getParams()
    {
        return config('imei');
    }

    /**
     * Set key property
     *
     * @param $key
     * @return $this
     */
    protected function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Set link property
     *
     * @param $link
     * @return $this
     */
    protected function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

}