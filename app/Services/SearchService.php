<?php

namespace App\Services;

use App\Models\Brand;
use App\Services\IMEI\IMEIChecker;

class SearchService
{
    /**
     * @var ProductEntityService
     */
    protected $productManager;

    /**
     * @var IMEIChecker
     */
    protected $IMEIChecker;

    /**
     * SearchService constructor.
     * @param ProductEntityService $service
     * @param IMEIChecker $checker
     */
    public function __construct(ProductEntityService $service, IMEIChecker $checker)
    {
        $this->productManager = $service;
        $this->IMEIChecker = $checker;
    }

    /**
     * @param $param
     * @return \Illuminate\Support\Collection|mixed
     */
    public function search($param)
    {
        return $this->checkIfImei($param) ? $this->searchByImei($param) : $this->searchByName($param);
    }

    /**
     * @param $param
     * @return mixed
     */
    protected function searchByName($param)
    {
        $products = $this->productManager->getProductsLike($param);

        return count($products) === 1 ? $products->first() : $products;
    }

    /**
     * @param $param
     * @return \Illuminate\Support\Collection|mixed
     */
    protected function searchByImei($param)
    {
        $productInfo = $this->IMEIChecker->getPhone($param);
        $productName = $this->buildProductName($productInfo);
        if (! $productName) {
            return collect([]);
        }
        $productByName = $this->getProductByName($productName);

        return $productByName ? $productByName : $this->searchSimilar($productName);
    }

    /**
     * @param $productName
     * @return \Illuminate\Support\Collection|mixed
     */
    protected function searchSimilar($productName)
    {
        $products = $this->searchByName($productName);

        return count($products) ? $products : $this->searchByWords($productName);
    }

    /**
     * @param $name
     * @return \Illuminate\Support\Collection
     */
    protected function searchByWords($name)
    {
        $nameParts = explode(' ', $name);
        $brand = array_shift($nameParts);
        $brandId = Brand::where('name', $brand)->get()->first()->id;
        if (! $brandId) {
            return collect([]);
        }
        $params = [
            ['brand_id', '=', $brandId]
        ];

        return $this->productManager->getRelatedProducts($name, $params, 1);
    }

    /**
     * @param array $parts
     * @return bool|string
     */
    protected function buildProductName(array $parts)
    {
        if (isset($parts['brand']) && isset($parts['model'])) {
            return sprintf('%s %s', $parts['brand'], $parts['model']);
        }

        return false;

    }

    /**
     * @param $name
     * @return mixed
     */
    protected function getProductByName($name)
    {
        return $this->productManager->getProductByParam('name', $name)->first();
    }

    /**
     * @param $param
     * @return bool
     */
    protected function checkIfImei($param)
    {
        return is_numeric($param) && strlen($param) === 15;
    }
}