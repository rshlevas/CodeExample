<?php

namespace App\Services\QuizServices;

use App\Models\Condition;
use App\Models\Question;

class QuizService
{
    /**
     * Returns condition according to the answered questions
     *
     * @return mixed
     */
    public function getResult()
    {
        return $this->getCondition($this->getPercentage());
    }

    /**
     * Returns maximum of points, that user could get
     *
     * @return int
     */
    protected function getMaxWeight()
    {
        $maxWeight = 0;
        foreach ($this->getQuestions() as $question)
        {
            $maxWeight = $maxWeight + $question->maxWeightAnswer->weight;
        }

        return $maxWeight;
    }

    /**
     * Get all questions from DB
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function getQuestions()
    {
        return Question::with('maxWeightAnswer')->get();
    }

    /**
     * Get condition according to the weight
     *
     * @param $value
     * @return mixed
     */
    protected function getCondition($value)
    {
        return Condition::where([
            ['max_weight', '>=', $value],
            ['min_weight', '<=', $value],
            ['lang', 'en'],
        ])->get()
        ->first();
    }

    /**
     * Returns users point, that he got during quiz form the session
     *
     * @return \Illuminate\Session\SessionManager|\Illuminate\Session\Store|mixed
     */
    protected function getUserPoints()
    {
        return session('quiz-points');
    }

    /**
     * Calculate the percentage of users point in comparison with theoretical max result
     *
     * @return float|int
     */
    protected function getPercentage()
    {
        return $this->getUserPoints() * 100 / $this->getMaxWeight();
    }
}