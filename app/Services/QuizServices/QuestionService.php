<?php

namespace App\Services\QuizServices;

use App\Models\Answer;
use App\Models\Question;
use App\Services\QuizServices\Exceptions\QuestionServiceException;

class QuestionService
{
    protected $answersNumber;

    /**
     * Given data map, that used for data structure check
     *
     * @var array
     */
    protected  $dataMap = ['content'];

    /**
     * Creates new question and related answers
     *
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        $this->setAnswerNumber($data, 'create');
        $this->checkData($data);

        return $this->createQuestion($data);
    }

    /**
     * Creates translation of the question
     *
     * @param array $data
     * @return mixed
     */
    public function createTranslation(array $data)
    {
        $this->answersNumber = $data['answer_number'];
        $this->checkData($data);

        return $this->createQuestion($data);
    }

    /**
     * Updates question and its related answers
     *
     * @param array $data
     * @return mixed
     */
    public function update(array $data)
    {
        $this->setAnswerNumber($data, 'update');
        $this->checkData($data);

        return $this->updateQuestion($data);

    }

    /**
     * Updates the question translation
     *
     * @param array $data
     * @return mixed
     */
    public function updateTranslation(array $data)
    {
        $this->answersNumber = $data['answer_number'];
        $this->checkData($data);

        return $this->updateQuestion($data);
    }

    /**
     * Question creation logic
     *
     * @param $data
     * @return mixed
     */
    protected function createQuestion($data)
    {
        $question = Question::create($data);

        return $this->setAnswers($data, $question, 'create');
    }

    /**
     * Question updating logic
     *
     * @param $data
     * @return mixed
     */
    protected function updateQuestion($data)
    {
        $question = $this->getQuestion($data['questionId']);
        $question->update($data);

        return $this->setAnswers($data, $question, 'update');
    }

    /**
     * Finds question by ID
     *
     * @param int $id
     * @return mixed
     */
    protected function getQuestion(int $id)
    {
        return Question::find($id);
    }

    /**
     * Set related answer entities fot the given question
     *
     * @param $data
     * @param $question
     * @param $type
     * @return mixed
     */
    protected function setAnswers($data, $question, $type)
    {
        for($i = 1; $i <= $this->answersNumber; $i++) {
            $answer = $this->getAnswer($data, $i, $type);
            $question->answers()->save($answer);
        }

        return $question;
    }

    /**
     * Return answer with set data
     *
     * @param $data
     * @param $index
     * @param $type
     * @return Answer
     */
    protected function getAnswer($data, $index, $type)
    {
        $answer = $this->getAnswerByType($data, $index, $type);

        return $this->setAnswerInfo($answer, $data, $index);
    }

    /**
     * Set info to the answer entity
     *
     * @param Answer $answer
     * @param $data
     * @param $index
     * @return Answer
     */
    protected function setAnswerInfo(Answer $answer, $data, $index)
    {
        $answer->content = $data["answer-{$index}"];
        $answer->weight = $data["answer-weight-{$index}"];
        $answer->lang = isset($data['lang']) ? $data['lang'] : 'en';

        return $answer;
    }

    /**
     * Returns Answer entity by action type
     *
     * @param $data
     * @param $index
     * @param $type
     * @return Answer|bool
     */
    protected function getAnswerByType($data, $index, $type)
    {
        switch($type) {
            case 'update':
                return isset($data["answer-id-{$index}"]) ? Answer::find($data["answer-id-{$index}"]) : new Answer();
                break;
            case 'create':
                return new Answer();
                break;
        }

        return false;
    }

    /**
     * Check the structure of given data
     *
     * @param $data
     * @return $this
     */
    protected function checkData($data)
    {
        return $this->checkQuestionData($data)->checkAnswersData($data);
    }

    /**
     * Check the question data
     *
     * @param $data
     * @return $this
     */
    protected function checkQuestionData($data)
    {
        foreach ($this->dataMap as $field) {
            $this->checkField($field, $data);
        }

        return $this;
    }

    /**
     * Check the answer data
     *
     * @param $data
     * @return $this
     */
    protected function checkAnswersData($data)
    {
        for ($i = 1; $i <= $this->answersNumber; $i++) {
            $this->checkField("answer-$i", $data);
            $this->checkField("answer-weight-$i", $data);
        }

        return $this;
    }

    /**
     * Set answer number property
     *
     * @param $data
     * @param $type
     * @return float|int
     */
    protected function setAnswerNumber($data, $type)
    {
        return $this->answersNumber = $this->calculateAnswersNumber($data, $type);
    }

    /**
     * Calculates answer number by given data
     *
     * @param $data
     * @return float|int
     */
    protected function calculateAnswersNumber($data, $type)
    {
        switch ($type) {
            case 'update':
                return (count($data) - 4 - $data['answer_number']) / 2;
                break;
            case 'create':
                return (count($data) - 2) / 2;
                break;
        }

        return false;
    }

    /**
     * Check if necessary filed is set. Throws exception if not
     *
     * @param $field
     * @param $data
     * @return bool
     * @throws QuestionServiceException
     */
    protected function checkField($field, $data)
    {
        if ( ! isset($data[$field])) {
            throw new QuestionServiceException("Incorrect data structure. No {$field} info set");
        }

        return true;
    }
}