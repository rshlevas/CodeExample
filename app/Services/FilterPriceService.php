<?php

namespace App\Services;

use App\Models\Product;
use App\Models\ProductPrice;
use Illuminate\Support\Collection;

class FilterPriceService
{
    protected $relationFilters;

    public function __construct()
    {
        $this->relationFilters = config('filter.services.' . get_class($this));
    }

    /**
     * Filter product profile prices according to given filters
     *
     * @param array $filters
     * @param Product $product
     * @return Collection
     */
    public function filter(Product $product, array $filters = [])
    {
        $where = $this->getParams($filters);

        return count($product->children) ? $this->getPricesWithChildren($product, $where) : $this->getPrices($product, $where);
    }

    /**
     * @param $product
     * @param $where
     * @return Collection
     */
    protected function getPricesWithChildren($product, $where)
    {
        $result = $this->getPrices($product, $where);
        foreach ($product->children as $child) {
            $result = $result->merge($this->getPrices($child, $where));
        }
        $result = $result->groupBy(function ($item, $key) {
            return $item->productProfile->site_id;
        });

        $prices = collect([]);

        foreach ($result as $item) {
            $prices->push($item->where('price', $item->max('price'))->first());
        }

        return $this->orderByPriceDesc($this->orderByTrustpilotDesc($prices));
    }

    /**
     * Get productPrice entity from DB by prepared condition
     *
     * @param $where
     * @return mixed
     */
    protected function getPrice($where, $ids)
    {
        return ProductPrice::with(
                'productProfile',
                'productProfile.site',
                'productProfile.site.image',
                'productProfile.site.paymentTerm',
                'productProfile.site.payments',
                'productProfile.link'
            )->where($where)
            ->whereIn('product_profile_id', $ids)
            ->get();
    }

    /**
     * Prepare prices collection of each profile. Price is marked by array key according to
     * product profile
     *
     * @param $product
     * @param $where
     * @return Collection
     */
    protected function getPrices($product, $where)
    {
        $ids = $product->profiles->pluck('id')->toArray();
        $prices = $this->getPrice($where, $ids);

        return $this->orderByPriceDesc($this->orderByTrustpilotDesc($prices));
    }

    /**
     * Order prices collection by price desc
     *
     * @param Collection $prices
     * @return Collection
     */
    protected function orderByPriceDesc(Collection $prices)
    {
        return $prices->sortByDesc(function($price) {
            return $price->price;
        });
    }

    /**
     * Order prices collection by site trustpilot asc
     *
     * @param Collection $prices
     * @return Collection
     */
    protected function orderByTrustpilotDesc(Collection $prices)
    {
        return $prices->sortByDesc(function($price) {
            return $price->productProfile->site->trust_point;
        });
    }

    /**
     * Prepare where params for request according to the filters
     *
     * @param $filters
     * @return array
     */
    protected function getParams($filters)
    {
        $params = [];

        $filterNames = array_keys($this->relationFilters);
        foreach ($filterNames as $name) {
            $params[] = $this->getParam($filters, $name);
        }

        return $params;
    }

    /**
     * Decides to take default param or not
     *
     * @param $filters
     * @param $name
     * @return array
     */
    protected function getParam($filters, $name)
    {
        return isset($filters[$name]) ? $this->getCondition($name, $filters[$name]) : $this->getCondition($name);
    }

    /**
     * Get where condition according to the configure map
     *
     * @param $name
     * @param null $filterValue
     * @return array
     */
    protected function getCondition($name, $filterValue = null)
    {
        $value = $filterValue ? $this->relationFilters[$name][$filterValue] : $this->relationFilters[$name]['default'];

        return [$this->relationFilters[$name]['column'], $value];
    }
}