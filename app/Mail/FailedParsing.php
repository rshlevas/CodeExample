<?php

namespace App\Mail;

use App\Models\NegativeReport;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FailedParsing extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var NegativeReport
     */
    public $report;

    /**
     * Create a new message instance.
     * FailedParsing constructor.
     *
     * @param NegativeReport $report
     */
    public function __construct(NegativeReport $report)
    {
        $this->report = $report;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('admin.emails.failed_report', [
            'report' => $this->report
        ]);
    }
}
