<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*____________________________________________________________________________________________________________________
|
|                                                  ADMIN SECTION
|_____________________________________________________________________________________________________________________
|
| Here declared route of admin section. To all of them "admin" prefix will be added.
| Subsections:
|   - General - main page routes
|   - Profile - profile displaying and editing
|   - User - user managing section
|   - Site - recyclers entities managing
|   - Brand - manufacturer entities managing
|   - Category - category entities managing
|   - Payment - payment methods and term managing
|   - Parsing - parsing reports managing
|   - Product - products entities managing
|   - Description - products and recyclers description managing
|   - Quiz - quiz questions, answers and scenario managing
|   - Images - images import
|   - Trustpilot - trustpilot reviews import
|   - Filters - filters section managing
|   - Propositions - trade in propositions
|
*/
Route::prefix('admin')->middleware('auth')->group(function () {

    /*
    |                                          Admin General Section
    |_________________________________________________________________________________________________________________
    |
    |  Here route for admin main page
    |
    */
    Route::get('/', 'Admin\HomeController@index')->name('admin_main');

    /*
    |                                          Admin Profile Section
    |_________________________________________________________________________________________________________________
    |
    |  Here routes for profiles managing are declared
    |
    */
    Route::get('/profile', 'Admin\ProfileController@index')->name('admin_profile');
    Route::post('/profile/store/email', 'Admin\ProfileController@storeInfo')->name('admin_profile_storage_info');
    Route::post('/profile/store/password', 'Admin\ProfileController@storePassword')
        ->name('admin_profile_storage_password');

    /*
    |                                          Admin User Section
    |_________________________________________________________________________________________________________________
    |
    |  Here routes for user managing are declared
    |
    */
    Route::get('/users', 'Admin\UserController@index')->name('admin_users_main');
    Route::get('/users/create', 'Admin\UserController@create')->name('admin_users_create');
    Route::get('/users/delete/{user}', 'Admin\UserController@destroy')
        ->where('user', '[0-9]+')
        ->name('admin_users_delete');
    Route::get('/users/update/{user}', 'Admin\UserController@update')
        ->where('user', '[0-9]+')
        ->name('admin_users_update');
    Route::post('/users/save', 'Admin\UserController@storage')->name('admin_users_storage');

    /*
    |                                          Admin Site Section
    |_________________________________________________________________________________________________________________
    |
    |  Here routes for recyclers managing are declared
    |
    */
    Route::get('/sites', 'Admin\SiteController@index')->name('admin_sites_main');
    Route::get('/sites/view/{site}', 'Admin\SiteController@view')
        ->where('site', '[0-9a-zA-Z_-]+')
        ->name('admin_sites_view');
    Route::get('/sites/create', 'Admin\SiteController@create')->name('admin_sites_create');
    Route::get('/sites/delete/{site}', 'Admin\SiteController@destroy')
        ->where('site', '[0-9a-zA-Z_-]+')
        ->name('admin_sites_delete');
    Route::get('/sites/update/{site}', 'Admin\SiteController@update')
        ->where('site', '[0-9a-zA-Z_-]+')
        ->name('admin_sites_update');
    Route::post('/sites/save', 'Admin\SiteController@storage')->name('admin_sites_storage');

    /*
    |                                          Admin Brand Section
    |_________________________________________________________________________________________________________________
    |
    |  Here routes for manufacturers managing are declared
    |
    */
    Route::get('/brands', 'Admin\BrandController@index')->name('admin_brands_view');
    Route::get('/brands/create', 'Admin\BrandController@create')->name('admin_brands_create');
    Route::get('/brands/delete/{brand}', 'Admin\BrandController@destroy')
        ->where('brand', '[0-9a-zA-Z_-]+')
        ->name('admin_brands_delete');
    Route::get('/brands/update/{brand}', 'Admin\BrandController@update')
        ->where('brand', '[0-9a-zA-Z_-]+')
        ->name('admin_brands_update');
    Route::post('/brands/save', 'Admin\BrandController@storage')->name('admin_brands_storage');

    /*
    |                                          Admin Category Section
    |_________________________________________________________________________________________________________________
    |
    |  Here routes for categories managing are declared
    |
    */
    Route::get('/categories', 'Admin\CategoryController@index')->name('admin_categories_view');
    Route::get('/categories/create', 'Admin\CategoryController@create')->name('admin_categories_create');
    Route::get('/categories/delete/{category}', 'Admin\CategoryController@destroy')
        ->where('category', '[0-9a-zA-Z_-]+')
        ->name('admin_categories_delete');
    Route::get('/categories/update/{category}', 'Admin\CategoryController@update')
        ->where('brand', '[0-9a-zA-Z_-]+')
        ->name('admin_categories_update');
    Route::post('/categories/save', 'Admin\CategoryController@storage')->name('admin_categories_storage');

    /*
    |                                          Admin Payment Section
    |_________________________________________________________________________________________________________________
    |
    |  Here routes for payment terms and methods managing are declared
    |
    */
    Route::get('/payments', 'Admin\PaymentController@index')->name('admin_payments_view');
    Route::get('/payments/{type}/create', 'Admin\PaymentController@create')
        ->name('admin_payments_create')
        ->where('type', '[a-z]+')
        ->middleware('payment.type');
    Route::get('/payments/{type}/update/{id}', 'Admin\PaymentController@update')
        ->where(['type' => '[a-z]+','id' => '[0-9]+'])
        ->name('admin_payments_update')
        ->middleware('payment.type');
    Route::post('/payments/{type}/save', 'Admin\PaymentController@storage')
        ->name('admin_payments_storage')
        ->where('type', '[a-z]+')
        ->middleware('payment.type');

    /*
    |                                          Admin Parse Section
    |_________________________________________________________________________________________________________________
    |
    |  Here routes for parsing reports managing are declared
    |
    */
    Route::get('/reports/{site}/{type}/', 'Admin\ReportController@view')
        ->where(['type' => '[a-z]+', 'site' => '[0-9a-zA-Z_-]+'])
        ->name('admin_reports_view')
        ->middleware('report.type');
    Route::get('/reports/{site}/{type}/delete/', 'Admin\ReportController@destroyByType')
        ->where(['type' => '[a-z]+', 'site' => '[0-9a-zA-Z_-]+'])
        ->name('admin_reports_delete')
        ->middleware('report.type');
    Route::get('/reports/delete/', 'Admin\ReportController@destroyAll')->name('admin_reports_delete_all');
    Route::get('/report/{report}/delete/{type}', 'Admin\ReportController@destroy')
        ->where(['type' => '[a-z]+', 'site' => '[0-9a-zA-Z_-]+'])
        ->name('admin_reports_delete_single')
        ->middleware('report.type.delete');

    /*
    |
    |                                          Admin Product Section
    |_________________________________________________________________________________________________________________
    |
    |  Here routes for all actions with products are declared
    |  Subsections: search, general, most popular, products with no images, merging of products and month best price.
    |
    */
    //Search subsection
    Route::get('/search', 'Admin\SearchController@index')->name('admin_search');
    Route::any('/search/result', 'Admin\SearchController@search')->name('admin_search_result');
    Route::post('/search/autocomplete', 'Admin\SearchController@autocomplete')->name('admin_search_autocomplete');

    //General subsection
    Route::get('/product/{product}', 'Admin\ProductController@view')
        ->name('admin_product_view')
        ->where('product', '[0-9a-zA-Z_-]+');
    Route::get('/product/{product}/update', 'Admin\ProductController@update')
        ->name('admin_product_update')
        ->where('product', '[0-9a-zA-Z_-]+');
    Route::post('/product/{product}/store', 'Admin\ProductController@store')
        ->name('admin_product_store')
        ->where('product', '[0-9a-zA-Z_-]+');

    //Most popular section and products with no image
    Route::get('/no-image', 'Admin\ProductController@viewNoImage')->name('admin_product_no_image');
    Route::get('/most-popular', 'Admin\PopularProductController@index')->name('admin_popular_main');
    Route::get('/most-popular/{product}', 'Admin\PopularProductController@make')
        ->name('admin_make_popular')
        ->where('product', '[0-9a-zA-Z_-]+');
    Route::get('/most-popular/{product}/remove', 'Admin\PopularProductController@remove')
        ->name('admin_remove_popular')
        ->where('product', '[0-9a-zA-Z_-]+');

    //Merging of products
    Route::get('/product/{product}/merge', 'Admin\ProductMergeController@index')
        ->name('admin_product_merge')
        ->where('product', '[0-9a-zA-Z_-]+');
    Route::post('/product/{product}/merge/store', 'Admin\ProductMergeController@store')
        ->name('admin_product_merge_store')
        ->where('product', '[0-9a-zA-Z_-]+');
    Route::post('/product/{product}/synonym/add', 'Admin\ProductMergeController@add')
        ->name('admin_product_synonym_add')
        ->where('product', '[0-9a-zA-Z_-]+');
    //Month best price deleting
    Route::any('/month-best-price/delete/{price}', 'Admin\MonthPriceController@destroy')
        ->name('admin_month_price_delete')
        ->where('price', '[0-9]+');

    /*
    |                                          Admin Description Section
    |_________________________________________________________________________________________________________________
    |
    |  Here routes for recyclers and product description managing are declared
    |
    */
    Route::get('/descriptions/view/{type}/{name}', 'Admin\DescriptionController@view')
        ->name('admin_descriptions_view')
        ->where(['type' => '[a-z]+', 'name' => '[0-9a-zA-Z_-]+'])
        ->middleware('description.type');
    Route::post('/descriptions/store/{type}', 'Admin\DescriptionController@store')
        ->name('admin_descriptions_store')
        ->where('type', '[a-z]+')
        ->middleware('description.type');
    Route::get('/descriptions/delete/{description}', 'Admin\DescriptionController@destroy')
        ->name('admin_descriptions_delete')
        ->where('description', '[0-9]+');
    Route::get('/descriptions/create/{type}/{model}', 'Admin\DescriptionController@create')
        ->name('admin_descriptions_create')
        ->where(['type' => '[a-z]+', 'model' => '[0-9]+'])
        ->middleware('description.type');

    /*
    |                                          Admin Quiz Section
    |_________________________________________________________________________________________________________________
    |
    |  Here routes for quiz managing are declared
    |  Subsections: general, questions, translations, answers
    |
    */
    //General
    Route::get('/quiz', 'Admin\QuizController@index')->name('admin_quiz_main');
    Route::post('/quiz/condition/store', 'Admin\ConditionController@store')->name('admin_condition_weight_store');
    Route::post('/quiz/question/order/store', 'Admin\QuizController@storeQuestionOrder')
        ->name('admin_question_order_store');

    //Quiz questions subsection
    Route::get('/question/create', 'Admin\QuestionController@create')->name('admin_question_create');
    Route::get('/question/update/{question}', 'Admin\QuestionController@update')
        ->where('question', '[0-9]+')
        ->name('admin_question_update');
    Route::get('/question/delete/{question}', 'Admin\QuestionController@delete')
        ->where('question', '[0-9]+')
        ->name('admin_question_delete');
    Route::post('/question/store', 'Admin\QuestionController@store')->name('admin_question_store');

    //Quiz questions translations subsection
    Route::get('/quiz/question/translation/view/{question}', 'Admin\QuestionTranslationController@index')
        ->where('question', '[0-9]+')
        ->name('admin_question_translation_view');
    Route::get('/quiz/question/translation/add/{question}', 'Admin\QuestionTranslationController@create')
        ->where('question', '[0-9]+')
        ->name('admin_question_translation_create');
    Route::get('/quiz/question/translation/update/{question}', 'Admin\QuestionTranslationController@update')
        ->where('question', '[0-9]+')
        ->name('admin_question_translation_update');
    Route::post('/quiz/question/translation/store', 'Admin\QuestionTranslationController@store')
        ->name('admin_question_translation_store');

    //Quiz answer subsection
    Route::any('/answer/delete/{answer}', 'Admin\AnswerController@delete')
        ->name('admin_answer_delete')
        ->where('answer', '[0-9]+');

    /*
    |                                          Admin Image Section
    |_________________________________________________________________________________________________________________
    |
    |  Here routes for image import are declared
    |
    */
    Route::get('/images/import', 'Admin\ImageController@import')
        ->name('admin_image_import');
    Route::post('/images/import/store', 'Admin\ImageController@store')
        ->name('admin_image_import_store');

    /*
    |                                          Admin Trustpilot Review Section
    |_________________________________________________________________________________________________________________
    |
    |  Here routes for trustpilot review import are declared
    |
    */
    Route::get('/trustpilot/reviews/import', 'Admin\TrustpilotReviewController@import')
        ->name('admin_trustpilot_reviews_import');
    Route::post('/trustpilot/reviews/import/store', 'Admin\TrustpilotReviewController@store')
        ->name('admin_trustpilot_reviews_import_store');

    /*
    |                                          Admin Filter Section
    |_________________________________________________________________________________________________________________
    |
    |  Here routes for filter section are declared
    |
    */
    Route::get('/filters', 'Admin\FilterController@index')
        ->name('admin_filters_main');
    Route::get('/filters/update/{type}/{filter}', 'Admin\FilterController@update')
        ->where(['type' => '[a-z]+', 'filter' => '[0-9]+'])
        ->name('admin_filters_update')
        ->middleware('filter.type');
    Route::post('/filters/store', 'Admin\FilterController@store')
        ->name('admin_filters_store');
    Route::get('/filters/switch/active/{type}/{filter}', 'Admin\FilterController@switchActiveState')
        ->name('admin_filters_active_switch')
        ->where(['type' => '[a-z]+', 'filter' => '[0-9]+'])
        ->middleware('filter.type');
    Route::post('/filter/store/order/{type}', 'Admin\FilterController@storeOrder')
        ->name('admin_filters_order_store')
        ->where('type', '[a-z]+')
        ->middleware('filter.type');;

    Route::get('/parse', 'Admin\HomeController@parse')->name('parse');

    /*
    |                                          Admin Proposition Section
    |_________________________________________________________________________________________________________________
    |
    |  Here routes for trade in propositions managing are declared
    |
    */
    Route::get('/proposition', 'Admin\PropositionController@index')->name('admin_propositions_main');
    Route::get('/proposition/create', 'Admin\PropositionController@create')->name('admin_propositions_create');
    Route::get('/proposition/delete/{proposition}', 'Admin\PropositionController@destroy')
        ->where('proposition', '[0-9]+')
        ->name('admin_propositions_delete');
    Route::get('/proposition/update/{proposition}', 'Admin\PropositionController@update')
        ->where('proposition', '[0-9]+')
        ->name('admin_propositions_update');
    Route::post('/proposition/save', 'Admin\PropositionController@storage')->name('admin_propositions_storage');
    Route::get('/{product}/propositions', 'Admin\ProductPropositionController@index')
        ->where('product', '[0-9a-zA-Z_-]+')
        ->name('admin_product_proposition_view');
    Route::get('/{product}/propositions/create', 'Admin\ProductPropositionController@create')
        ->where('product', '[0-9a-zA-Z_-]+')
        ->name('admin_product_proposition_create');
    Route::get('/{product}/propositions/update/{proposition}', 'Admin\ProductPropositionController@update')
        ->where(['product' => '[0-9a-zA-Z_-]+', 'proposition' => '[0-9]+'])
        ->name('admin_product_proposition_update');
    Route::get('/{product}/propositions/delete/{proposition}', 'Admin\ProductPropositionController@destroy')
        ->where(['product' => '[0-9a-zA-Z_-]+', 'proposition' => '[0-9]+'])
        ->name('admin_product_proposition_delete');
    Route::post('/{product}/propositions/save', 'Admin\ProductPropositionController@storage')
        ->where('product', '[0-9a-zA-Z_-]+')
        ->name('admin_product_proposition_storage');

});

/*____________________________________________________________________________________________________________________
|
|                                                  USER SECTION
|_____________________________________________________________________________________________________________________
|
| Here declared route of user section.
| Subsections:
|   - Auth - standard auth routes of laravel
|   - Main - Site general pages
|   - Recycler - Recyclers pages
|   - Brand - manufacturers pages
|   - Product - products pages
|   - Sitemaps - sitemaps routes
|   - Quiz - quiz to get the condition of users product
|
*/
/*
|--------------------------------------------------------------------------
|                                Auth routes
|--------------------------------------------------------------------------
|
| Standard laravel auth routes
|
*/
Auth::routes();
/*
|--------------------------------------------------------------------------
|                                Site main pages
|--------------------------------------------------------------------------
|
| Here declared routes of site main pages
|
*/
Route::get('/', 'HomeController@index')->name('home');
Route::get('/most-popular', 'HomeController@viewPopular')->name('most_popular');
Route::get('/most-popular/{keyPhone}', 'HomeController@viewKeyProducts')
    ->name('key_product_view')
    ->where('keyPhone', '[a-z]+')
    ->middleware('key.product');
Route::get('/best-prices', 'HomeController@viewBestPrices')->name('best_prices_view');
Route::get('/about', 'HomeController@viewAbout')->name('about_view');

//Language switching
Route::get('/lang/{lang}', 'LanguageController@switchLang')->name('lang_switch')->where('lang', '[a-z]+');

/*
|--------------------------------------------------------------------------
|                                Product routes
|--------------------------------------------------------------------------
|
| Here are declared routes for product actions.
| Subsections: search, general, sell links
|
*/
//Search subsection
Route::any('/search', 'SearchController@search')->name('search');
Route::post('/search/autocomplete', 'SearchController@autocomplete')->name('search_autocomplete');

//Product subsection
Route::get('{brand}/{category}/{product}', 'ProductController@view')
    ->where([
        'product' => '[0-9a-zA-Z_-]+',
        'brand' => '[0-9a-zA-Z_-]+',
        'category' => '[0-9a-zA-Z_-]+',
    ])->name('product_view');
Route::get('{brand}/{category}/{product}/filters/best-price', 'ProductController@filterBestPrice')
    ->where([
        'product' => '[0-9a-zA-Z_-]+',
        'brand' => '[0-9a-zA-Z_-]+',
        'category' => '[0-9a-zA-Z_-]+',
    ])->name('product_best_price_filtering')
    ->middleware('price.filter');
Route::get('{brand}/{category}/{product}/filters/prices', 'ProductController@filterPrices')
    ->where([
        'product' => '[0-9a-zA-Z_-]+',
        'brand' => '[0-9a-zA-Z_-]+',
        'category' => '[0-9a-zA-Z_-]+',
    ])->name('product_prices_filtering')
    ->middleware('price.filter');

//Sell link section
Route::get('/outlink/{link}', 'SellLinkController@redirect')->where('link', '[0-9]+')->name('sell_link');

/*
|--------------------------------------------------------------------------
|                                Recyclers routes
|--------------------------------------------------------------------------
|
| Here are declared routes for recyclers actions.
|
*/
Route::get('/recyclers', 'RecyclerController@index')->name('recyclers_list');
Route::get('/recyclers/{site}', 'RecyclerController@view')->where('site', '[0-9a-zA-Z_-]+')->name('recyclers_view');

/*
|--------------------------------------------------------------------------
|                                Sitemaps routes
|--------------------------------------------------------------------------
|
| Here are declared routes for site sitemaps
|
*/
Route::get('/sitemaps.xml', 'SitemapsController@index')->name('sitemaps_main');
Route::get('/sitemaps/general.xml', 'SitemapsController@general')->name('sitemaps_general');
Route::get('/sitemaps/products.xml', 'SitemapsController@products')->name('sitemaps_products');
Route::get('/sitemaps/brands.xml', 'SitemapsController@brands')->name('sitemaps_brands');
Route::get('/sitemaps/recyclers.xml', 'SitemapsController@sites')->name('sitemaps_recyclers');

/*
|--------------------------------------------------------------------------
|                                Quiz routes
|--------------------------------------------------------------------------
|
| Here are declared routes for quiz actions.
|
*/
Route::get('/quiz', 'QuizController@index')->name('quiz_main');
Route::get('/quiz/scenario/{answer}/{step}', 'QuizController@nextQuestion')
    ->where(['answer' => '[0-9]+', 'step' => '[0-9]+'])
    ->name('quiz_next_question');

/*
|--------------------------------------------------------------------------
|                                Brand routes
|--------------------------------------------------------------------------
|
| Here are declared routes for manufacturers actions.
|
*/
Route::get('/make', 'BrandController@index')->name('brand_main');
Route::get('make/{brand}/', 'BrandController@view')->where('brand', '[0-9a-zA-Z_-]+')->name('brand_view');

