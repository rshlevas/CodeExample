@extends('layouts.app')

@section('title', trans('recyclers.title.view', ['site' => $site->name]))

@section('canonical-link', route('recyclers_view', ['site' => $site->url_name]))

@section('breadcrumbs')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{ route('home') }}">@lang('header.home')</a></li>
                    <li><a href="{{ route('recyclers_list') }}">@lang('header.partners')</a></li>
                    <li class="active text-capitalize"><strong>{{ $site->name }}</strong></li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            @include('recyclers.recycler_info', ['site' => $site])
            <br>
            @if(count($site->reviews))
                @include('recyclers.recyclers_reviews', ['reviews' => $site->reviews, 'link' => $site->trustpilot_link])
            @endif
            @include('recyclers.recycler_description', ['site' => $site])
        </div>
    </div>
@endsection