<div class="col-md-10 col-md-offset-1">
    <h2>@lang('recyclers.about', ['site' => $site->name])</h2><br>
    <div class="col-md-12">
        @if ($site->description(App::getLocale()) && isset($shorter))
            {!! $site->description(App::getLocale())->short_content !!}
        @elseif ($site->description(App::getLocale()) && ! isset($shorter))
            {!! $site->description(App::getLocale())->content !!}
        @else
            Description of {{ $site->name }} will be later
        @endif
            <br>
    </div>
</div>