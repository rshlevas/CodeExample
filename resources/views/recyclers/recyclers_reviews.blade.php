<link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet">

<section id="what-customers-say" class="page-section container">
    <div class="container-constrained">
        <div class="grid">
            <div class="col-md-12 text-center">
                <h2>Reviews</h2>
            </div>
            @foreach($reviews->chunk(3) as $reviewsGroup)

                @if(count($reviewsGroup) === 3)
                    <div class="col-md-12">
                @elseif(count($reviewsGroup) === 2)
                    <div class="col-md-8 col-md-offset-2">
                @else
                    <div class="col-md-4 col-md-offset-4">
                @endif
                @foreach($reviewsGroup as $review)
                        @if(count($reviewsGroup) === 3)
                            <div class="col-md-4">
                        @elseif(count($reviewsGroup) === 2)
                            <div class="col-md-6">
                        @else
                            <div class="col-md-12">
                        @endif
                        <div class="cell container-constrained-xs">
                            <blockquote>
                                <p>{{ $review->short_content }}</p>
                                <footer>
                                    {{ $review->author }}
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                @endforeach
                </div>
            @endforeach
            <div class="col-md-12 col--bleed">
                <div class="text-center">
                    <p>
                        Find more on <br>
                            <a href="{{ $link }}" target="_blank"><img
                                                    src="{{asset('/images/trustpilot.svg') }}"
                                                    alt="trustpilot"></a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>