@extends('layouts.app')

@section('title', trans('recyclers.title.index'))

@section('canonical-link', route('recyclers_list'))

@section('breadcrumbs')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{ route('home') }}">@lang('header.home')</a></li>
                    <li class="active text-capitalize"><strong>@lang('header.partners')</strong></li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            @foreach($sites as $site)
                @include('recyclers.recycler_info', ['site' => $site])
                <br>
                @include('recyclers.recycler_description', ['site' => $site, 'shorter' => true])
                <br>
            @endforeach
        </div>
    </div>
@endsection