@php $comparePhrase = $product->profiles_count > 1 ?
            trans('products.compare.prices.several', ['number' => $product->profiles_count]) :
            trans('products.compare.prices.single');
@endphp

    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-primary row" style="padding-top:10px; padding-bottom:10px;">
            <div class="col-md-3 col-sm-3 col-xs-3">
                <a class="link-unstyled"
                   href="{{ route('brand_view', ['brand' => $product->brand->url_name]) }}">
                    {{ strtoupper($product->brand->name) }}
                </a>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-3">
                @if ($product->image)
                    <img style="height: 120px;" class="img-responsive center-block"
                         src="{{ asset($product->image->source) }}">
                @elseif ($product->imageThumbnail)
                    <img style="height: 120px;" class="img-responsive center-block"
                         src="{{ asset($product->imageThumbnail->source) }}">
                @else
                    <img style="height: 120px;" class="img-responsive center-block"
                         src="{{ asset('images/no-image.jpeg') }}">
                @endif
            </div>
            <div class="col-md-2 col-sm-2 col-xs-3">
                <h3 class="text-center">
                    <i class="icon-{{ strtolower($product->best_price->currency) }}"></i>
                    {{ $product->best_price->price }}
                </h3>
            </div>
            <a class="link-unstyled" href="{{ route('product_view', [
                    'brand' => $product->brand->url_name,
                    'category' => $product->category->url_name,
                    'product' => $product->url_name
                ]) }}"
                title="{{ $product->name }}"><div class="col-md-5 col-sm-5 col-xs-3 text-center">
                <h4><span class="align-middle">{{$product->short_model}}</span></h4>
                <span class="small">
                    <strong>
                        <u>{{ $comparePhrase }}</u>
                    </strong>
                </span>
            </div>
            </a>

        </div>
    </div>