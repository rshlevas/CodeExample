@extends('layouts.app')

@section('title', trans('header.prices'))

@section('canonical-link', route('best_prices_view'))

@section('breadcrumbs')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{ route('home') }}">@lang('header.home')</a></li>
                    <li class="active text-capitalize"><strong>@lang('homepage.best.prices')</strong></li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-10 col-md-offset-1 text-center">
                    <h3>@lang('homepage.best.prices.phrase')</h3><br>
                </div>
                @foreach($products as $product)
                    @include('site.components.products_column', ['product' => $product])
                @endforeach
            </div>
        </div>
    </div>
@endsection