@extends('layouts.app')

@section('title', trans('homepage.works'))

@section('canonical-link', route('about_view'))

@section('breadcrumbs')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{ route('home') }}">@lang('header.home')</a></li>
                    <li class="active text-capitalize"><strong>@lang('header.works')</strong></li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-10 col-md-offset-1 text-center">
                    @include('main.how-it-works');
                </div>
            </div>
        </div>
    </div>
@endsection