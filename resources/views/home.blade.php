@extends('layouts.app')

@section('title', trans('homepage.title'))

@section('canonical-link', route('home'))

@section('meta-desc', trans('homepage.meta.desc'))

@section('content')

<style type="text/css">
.bg-white {background: #fff;



}
</style>
    <div class="container" >
       
        <div class="row bg-white">
            <div id="most_popular" class="col-sm-4 col-xs-12">
                <h3 class="text-center">@lang('homepage.most.popular')</h3><br>
                    @include('main.most-popular', [
                        'products' => $mostPopular,
                        'route' => route('most_popular'),
                    ])
            </div>
            <div id="iphone" class="col-sm-4 col-xs-12">
                <h3 class="text-center">iPhone</h3><br>
                    @include('main.most-popular', [
                        'products' => $iphones,
                        'route' => route('key_product_view', ['keyPhone' => 'iphone']),
                    ])
            </div>
            <div id="samsung" class="col-sm-4 col-xs-12">
                <h3 class="text-center">Samsung Galaxy</h3><br>
                    @include('main.most-popular', [
                        'products' => $samsung,
                        'route' => route('key_product_view', ['keyPhone' => 'galaxy']),
                    ])
            </div>
        </div>
        <br>
            @include('main.best-prices', ['bestPrices' => $bestPrices])
        <br>
            @include('main.sites', ['sites' => $sites])
        <br>
            @include('main.testimonials');
        <br>
            @include('main.brands', ['brands' => $brands])
        <br>
        <div class="row">
            @include('main.how-it-works');
        </div>
        <br>
    </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.js"></script>
<script>
    $("img").lazyload({
        effect : "fadeIn"
    });
</script>

@endsection






