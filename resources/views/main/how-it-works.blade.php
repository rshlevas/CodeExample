<div class="container">
<div id="about" class="col-sm-10 col-sm-offset-1">
    <h3 class="text-center">@lang('homepage.works')</h3><br>
    <p>There are over 40 different mobile phone recyclers in the UK and Ireland nowadays, with all of them competing for your hard-earned money. The problem is that there has never been a way to compare prices from these different providers in an unbiased fashion, several websites claiming to be mobile phone recycling comparison services are in fact just run by the recycler! And of course their prices are always at the top of the price comparison table. <strong>Thankfully, SellMyPhone.org.uk is completely independent and unbiased</strong> - we only show you prices from leading recycling companies, not fly-by-night operations who might not pay you!</p>

<p>Sell My Phone is different, we compare prices side-by-side, making sure to show you all of the relevant details to make an informed and quick decision. Don't be fooled by fake or internal reviews on other websites, Sell My Phone only uses Trustpilot for ratings so you can be sure they are independent and accurate. What's more, Sell My Phone also compares payout times - if you need cash fash, in most cases, you will receive your cash the next day, by either PayPal, Bank Transfer (BACS) or a cheque in the post if you are old-school!</p>

<p>To start comparing prices, simply type your phone or device name into the search bar at the top of this page. SellMyPhone.org.uk compares prices from up to 35 different providers - more than any other website guaranteed. </p>
</div>
</div>