<div id="best-prices" class="row">
    <div class="col-sm-12">
        <h3 class="text-center">@lang('homepage.best.prices')</h3>
        <h5 class="text-center">Updated every hour based on working condition</h5>
        <br>
        @foreach($bestPrices as $chunk)
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 panel panel-default">
            
                    @foreach($chunk as $product)
                        <div class="col-md-12">
                            <a class="link-unstyled" title="{{ $product->name }}"
                                href="{{ route('product_view', [
                                    'brand' => $product->brand->url_name,
                                    'category' => $product->category->url_name,
                                    'product' => $product->url_name
                                ]) }}">
                                <div class="col-md-3">
                                    @if ($product->imageThumbnail)
                                        <img style="max-height: 120px;" class="img-responsive center-block"
                                             src="{{ asset($product->imageThumbnail->source) }}">
                                    @elseif ($product->image)
                                        <img style="max-height: 120px;" class="img-responsive center-block"
                                             src="{{ asset($product->image->source) }}">
                                    @else
                                        <img style="height: 80px;" class="img-responsive center-block"
                                             src="{{ asset('images/no-image.jpeg') }}">
                                    @endif
                                </div>
                                <div class="col-md-3">
                                    <h4 class="text-center">
                                        &pound;{{ $product->best_price->price }}
                                    </h4>
                                </div>
                                <div class="col-md-6">
                                    <span class="align-middle small"> {{ $product->brand->name }}
                                   <strong>{{ $product->short_model }}</strong></span>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
          
        @endforeach
    </div>
</div>