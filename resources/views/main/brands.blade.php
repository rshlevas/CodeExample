<div id="brands" class="row">
    <div class="col-sm-10 col-sm-offset-1">
        <div class="col-sm-10 col-sm-offset-1">
            <h3 class="text-center ">@lang('homepage.brands')</h3><br>
        </div>

        @foreach($brands as $brand)
            <a href="{{ route('brand_view', ['brand' => $brand->url_name]) }}" title="{{ $brand->name }}">
                <div class="col-lg-2 col-sm-3 col-xs-4">
                    <div class="panel panel-primary" style="height: 130px;">
                        <div class="panel-heading">{{ $brand->name }}</div>
                        @if ($brand->image)
                            <img style="height: 80px;" class="img-responsive center-block"
                                 src="{{ $brand->image->source }}">
                        @else
                            <img style="height: 80px;" class="img-responsive center-block"
                                 src="{{ asset('images/no-image.jpeg') }}">
                        @endif
                    </div>
                </div>
            </a>
        @endforeach
        @if ( count($brands) == 18)
            <div class="col-lg-1 col-lg-offset-11 col-sm-3 col-sm-offset-8 col-xs-4 col-xs-offset-6">
                <a href="{{ route('brand_main') }}" class="btn btn-primary">@lang('buttons.view.more')  <i class="icon-double-angle-right"></i></a>
            </div>
        @endif
    </div>
</div>