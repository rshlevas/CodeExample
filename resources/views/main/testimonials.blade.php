<link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet">



<section id="what-customers-say" class="page-section container">
    <div class="container-constrained">
        <div class="grid">
            <div class="col-md-12 text-center">
                <h2>What other users are saying</h2>
            </div>
            <div class="col-12 col-sm-4">
                <div class="cell container-constrained-xs">
                    <blockquote cite="https://twitter.com/StaceyCarson/statuses/7910090756866560">
                        <p>Thank you for finally offering a comparison service that works - my iPhone 6 had a smashed screen yet it was so easy to compare REAL prices.</p>
                        <footer>
                            Stacey C
                        </footer>
                    </blockquote>
               </div>
            </div>
            
            <div class="col-12 col-sm-4">
                <div class="cell container-constrained-xs">
                     <blockquote cite="https://twitter.com/letsdothis2/statuses/7956513994643584">
                        <p>Found the price quoted was accurate and got the money the next day from Mobile Cash Mate!</p>
                        <footer>
                            Kirstin A
                        </footer>
                    </blockquote>
                  </div>
            </div>
            
            <div class="col-12 col-sm-4">
                <div class="cell container-constrained-xs">
                      <blockquote cite="https://twitter.com/Sloane1998/statuses/7959534806241848">
                        <p>I love this site! So simple but such a brilliant idea. Very pleased I don't need to keep checking all the different websites manually.</p>
                        <footer>
                            Asmit B
                        </footer>
                    </blockquote>
                 </div>
            </div>
            <div class="col-12 col--bleed">
                <div class="text-center">
                     <p>
                        Reviews from Twitter, find more on <br>
                        <a href="https://uk.trustpilot.com/review/sellmyphone.org.uk" target="_blank"><img src="/images/trustpilot.svg" alt="trustpilot"></a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>