@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li class="active">Profile</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        @if ($message)
                            @include('admin.components.success_message', ['message' => $message])
                        @endif
                        <h4 class="text-center">Edit main info</h4>
                        <form class="form-horizontal" method="POST" action="{{ route('admin_profile_storage_info') }}"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="userId" value="{{ $user->id }}">
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Name</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{ $user->name }}" required>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">Email</label>
                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ $user->email }}" required>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('check_password') ? ' has-error' : '' }}">
                                <label for="check_password" class="col-md-4 control-label">Password</label>
                                <div class="col-md-6">
                                    <input id="check_password" type="password" class="form-control" name="check_password"
                                           placeholder="Enter your password to confirm changes" required>
                                    @if ($errors->has('check_password'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('check_password') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update Info
                                    </button>
                                </div>
                            </div>
                        </form>
                        <br>
                        <h4 class="text-center">Change password</h4>
                        <form class="form-horizontal" method="POST" action="{{ route('admin_profile_storage_password') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="userId" value="{{ $user->id }}">
                            <div class="form-group{{ $errors->has('oldPassword') ? ' has-error' : '' }}">
                                <label for="oldPassword" class="col-md-4 control-label">Old Password</label>

                                <div class="col-md-6">
                                    <input id="oldPassword" type="password" class="form-control"
                                           name="oldPassword" placeholder="Enter your old password" required>

                                    @if ($errors->has('oldPassword'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('oldPassword') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">New Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control"
                                           name="password" placeholder="Enter your new password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="col-md-4 control-label">Confirm New Password</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation" placeholder="Confirm new password" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update Password
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection