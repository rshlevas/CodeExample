@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li class="active">Import reviews</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        @if ($message)
                            @include('admin.components.success_message', ['message' => $message])
                        @endif
                        <h4 class="text-center">Upload json file with reviews</h4>
                        <p class="text-center">Each review at json file should have next params: 'review_text', 'review_title', 'reviewer_name', 'url'.  </p>
                        <form class="form-horizontal" method="POST" action="{{ route('admin_trustpilot_reviews_import_store') }}"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('review') ? ' has-error' : '' }}">
                                <label for="review" class="col-md-4 control-label">Json file</label>

                                <div class="col-md-6">
                                    <input id="review" type="file" class="form-control" name="review" required>
                                    @if ($errors->has('review'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('review') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Import
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection