@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li class="active">Sites</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        @if ($message)
                            @include('admin.components.success_message', ['message' => $message])
                        @endif
                        <h3 class="text-center">Site section managing</h3>
                        <a href="{{ route('admin_sites_create') }}" class="btn btn-primary">+ Add new site</a>
                        <a href="{{ route('admin_image_import') }}" class="btn btn-success">+ Import images</a>
                        <a href="{{ route('admin_trustpilot_reviews_import') }}" class="btn btn-warning">+ Import reviews</a>
                        <table class="table table-hover text-center">
                            <thead class="text-uppercase">
                            <tr>
                                <th class="col-md-3 info text-center">Name</th>
                                <th class="col-md-2 info text-center">Api Link</th>
                                <th class="col-md-2 info text-center">Logo</th>
                                <th class="col-md-2 info text-center">Active</th>
                                <th class="col-md-2 info text-center" colspan="2">Manage <i
                                            class="icon-fixed-width icon-cogs"></i></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($groups as $group)
                                <tr>
                                    <td colspan="6"><i class="icon-group"></i> <strong>{{ $group->name }} group</strong>
                                    </td>
                                </tr>
                                @foreach($group->sites as $site)
                                    <tr>
                                        <td><a href="{{ $site->path }}">{{ $site->name }}</a></td>
                                        <td>
                                            @if ($site->api_link)
                                                <i class="icon-li icon-ok" style="color:green"></i>
                                            @else
                                                <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($site->image)
                                                <img style="max-height: 100px;" src="{{ asset('storage/images/logos/sites') }}/{{$site->image->name}}"
                                                     class="img-responsive img-thumbnail">
                                            @else
                                                <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($site->is_active === 1)
                                                <i class="icon-li icon-ok" style="color:green"></i>
                                            @elseif ($site->is_active === 2)
                                                <i class="icon-li icon-lock" style="color:blue"></i>
                                            @else
                                                <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
                                            @endif
                                        </td>

                                        <td>
                                            <a class="btn btn-default"
                                               href="{{ route('admin_sites_view', ['site' => $site->url_name]) }}">
                                                <i class="icon-fixed-width icon-eye-open"></i>
                                            </a>
                                            <a class="btn btn-success"
                                               href="{{ route('admin_sites_update', ['site' => $site->url_name]) }}">
                                                <i class="icon-fixed-width icon-pencil"></i>
                                            </a>
                                            <a class="btn btn-danger"
                                               href="#" data-toggle="modal" data-target="#confirm-delete"
                                               data-href="{{ route('admin_sites_delete', ['site' => $site->url_name]) }}">
                                                <i class="icon-trash icon-large"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                            @if (count($sitesWithoutGroup))
                                <tr>
                                    <td colspan="6"><i class="icon-group"></i> <strong>Sites without group</strong>
                                    </td>
                                </tr>
                                @foreach($sitesWithoutGroup as $site)
                                    <tr>
                                        <td><a href="{{ $site->path }}">{{ $site->name }}</a></td>
                                        <td>
                                            @if ($site->api_link)
                                                <i class="icon-li icon-ok" style="color:green"></i>
                                            @else
                                                <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($site->image)
                                                <img src="{{ asset('storage/images/logos/sites') }}/{{$site->image->name}}"
                                                     class="img-responsive img-thumbnail">
                                            @else
                                                <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($site->is_active === 1)
                                                <i class="icon-li icon-ok" style="color:green"></i>
                                            @elseif ($site->is_active === 2)
                                                <i class="icon-li icon-lock" style="color:blue"></i>
                                            @else
                                                <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
                                            @endif
                                        </td>

                                        <td>
                                            <a class="btn btn-default"
                                               href="{{ route('admin_sites_view', ['site' => $site->url_name]) }}">
                                                <i class="icon-fixed-width icon-eye-open"></i>
                                            </a>
                                            <a class="btn btn-success"
                                               href="{{ route('admin_sites_update', ['site' => $site->url_name]) }}">
                                                <i class="icon-fixed-width icon-pencil"></i>
                                            </a>
                                            <a class="btn btn-danger"
                                               href="#" data-toggle="modal" data-target="#confirm-delete"
                                               data-href="{{ route('admin_sites_delete', ['site' => $site->url_name]) }}">
                                               <i class="icon-trash icon-large"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                        </table>
                            <div class="modal fade" id="confirm-delete"
                                 tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                @include('admin.components.modal_window', ['target' => 'this site'])
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection