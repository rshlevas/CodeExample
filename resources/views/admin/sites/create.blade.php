@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li><a href="{{ route('admin_sites_main') }}">Sites</a></li>
                            <li class="active">Add Site</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <h4 class="text-center">Fill the site info (* - means, that fields is required)</h4>
                        <form class="form-horizontal" method="POST" action="{{ route('admin_sites_storage') }}"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Name*</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name"
                                           placeholder="Enter site name" required>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="url_name" class="col-md-4 control-label">Link display name*</label>
                                <div class="col-md-6">
                                    <input id="url_name" type="text" class="form-control" name="url_name"
                                           placeholder="Enter site link display name" required>
                                    @if ($errors->has('url_name'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('url_name') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="path" class="col-md-4 control-label">Url*</label>
                                <div class="col-md-6">
                                    <input id="path" type="text" class="form-control" name="path"
                                           placeholder="Enter site main page url" required>
                                    @if ($errors->has('path'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('path') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="api_link" class="col-md-4 control-label">Api Link</label>
                                <div class="col-md-6">
                                        <textarea id="api_link" rows="2" class="form-control" name="api_link"
                                                  placeholder="Enter the link to datafeed source"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="group" class="col-md-4 control-label">Group <i class="icon-group"></i></label>
                                <div class="col-md-6">
                                    <select name="group_id" class="form-control selectpicker"  data-style="btn-primary" id="group">
                                        <option value="">Nothing selected</option>
                                        @foreach($groups as $group)
                                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="payments" class="col-md-4 control-label">Payments <i class="icon-money"></i></label>
                                <div class="col-md-6">
                                    <select name="payments[]" class="form-control selectpicker"  data-style="btn-primary" multiple id="payments">
                                        @foreach($payments as $payment)
                                            <option value="{{ $payment->id }}">{{ $payment->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="payment_term" class="col-md-4 control-label">Payment term <i class="icon-calendar"></i></label>
                                <div class="col-md-6">
                                    <select name="payment_term_id" class="form-control selectpicker"  data-style="btn-primary" id="payment_term">
                                        <option value="">Nothing selected</option>
                                        @foreach($terms as $term)
                                            <option value="{{ $term->id }}">{{ $term->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="is_returnable" class="col-md-4 control-label">Free Return?</label>
                                <div class="col-md-6">
                                    <select name="is_returnable" class="form-control selectpicker"  data-style="btn-primary" id="is_returnable">
                                        <option data-icon="icon-ban-circle" value="0">No</option>
                                        <option data-icon="icon-ok" value="1">Yes</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="is_free_post" class="col-md-4 control-label">Free Post?</label>
                                <div class="col-md-6">
                                    <select name="is_free_post" class="form-control selectpicker"  data-style="btn-primary" id="is_free_post">
                                        <option data-icon="icon-ban-circle" value="0">No</option>
                                        <option data-icon="icon-ok" value="1">Yes</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="trust_point" class="col-md-4 control-label">Trustpilot rating</label>
                                <div class="col-md-6">
                                    <input id="trust_point" type="text" class="form-control" name="trust_point"
                                           placeholder="Enter number between 0 and 10">
                                    @if ($errors->has('trust_point'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('trust_point') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="trustpilot_link" class="col-md-4 control-label">Trustpilot Url</label>
                                <div class="col-md-6">
                                    <input id="trustpilot_link" type="text" class="form-control" name="trustpilot_link"
                                           placeholder="Enter trustpilot url">
                                    @if ($errors->has('trustpilot_link'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('trustpilot_link') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="is_active" class="col-md-4 control-label">Active</label>
                                <div class="col-md-6">
                                    <select name="is_active" class="form-control selectpicker"  data-style="btn-primary" id="is_active">
                                        <option data-icon="icon-ban-circle" value="0">No</option>
                                        <option data-icon="icon-ok" value="1">Yes</option>
                                        <option data-icon="icon-lock" value="2">No Price</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
                                <label for="logo" class="col-md-4 control-label">Logo</label>

                                <div class="col-md-6">
                                    <div class="col-md-6">
                                        <img id="logo-image" src="{{ asset('images/no-image.jpeg') }}" class="img img-thumbnail">
                                    </div>
                                    <label class="btn btn-default btn-primary">
                                        Browse <input id="logo" type="file" class="form-control file" name="logo" style="display: none;">
                                    </label>
                                    @if ($errors->has('logo'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('logo') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection