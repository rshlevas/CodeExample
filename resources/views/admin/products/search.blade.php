@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li class="active text-capitalize">Product Search</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <h4 class="text-center">Enter payment product name</h4>
                        <form class="form-horizontal" method="POST" action="{{ route('admin_search_result')}}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="search" class="col-md-2 control-label">Search</label>
                                <div class="col-md-8">
                                    <input id="search" type="text" class="form-control" name="search"
                                           placeholder="Enter product name" required>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Search
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $("#search").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "{{ route('admin_search_autocomplete') }}",
                        type: "POST",
                        dataType: "json",
                        data: {search: request.term},
                        success: function (data) {
                            response($.map(data, function (item) {
                                return item;
                            }));
                        }
                    });
                },
                focus: function (event, ui) {
                    //$( "#search" ).val( ui.item.name );
                    return false;
                },
                select: function (event, ui) {
                    window.location.href = ui.item.url;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                var inner_html;
                if (item.message) {
                    inner_html = '<div class="col-md-8 col-sm-offset-2 small">' +
                        item.message + '</div>';
                } else {
                    inner_html = '<a class="link-unstyled" href="' + item.url + '" >' +
                        '<div class="col-md-3 col-sm-3 col-xs-3"><img style="height: 60px;"' +
                        ' class="img-responsive center-block" src="' + item.image +
                        '" ></div><div class="col-md-2 col-sm-2 col-xs-2"><h4 class="text-center small"><b>' +
                        '<i class="icon-' + item.currency + '"></i> ' + item.price +
                        '</b></h4></div><div class="col-md-6 col-sm-6 col-xs-6 small text-center">' +
                        item.name + '</div></a>';
                }
                return $("<li class=\"ui-menu-item\"></li>")
                    .data("item.autocomplete", item)
                    .append(inner_html)
                    .appendTo(ul);
            };
        });
    </script>
@endsection