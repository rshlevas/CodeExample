@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li class="active">Products without images</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <h3 class="text-center">Products without images ({{ $products->total() }}) <a href="{{ route('admin_image_import') }}" class="btn btn-success">+ Import images</a></h3>

                        @foreach ($products as $product)
                            <div class="col-md-4">
                                <a class="link-unstyled" href="{{ route('admin_product_view', ['product' => $product->url_name]) }}">{{ $product->name }}</a>
                            </div>
                        @endforeach
                        <div class="col-md-10 col-md-offset-1 text-center">
                            <h4>{{ $products->links() }}</h4><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection