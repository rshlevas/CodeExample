@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li><a href="{{ route('admin_popular_main') }}">Most Popular</a></li>
                            <li class="active">Product: <strong>{{ $product->name }}</strong></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        @if ($message)
                            @include('admin.components.success_message', ['message' => $message])
                        @endif
                        <h4 class="text-center">{{ $product->name }}
                            <a class="btn btn-success"
                               href="{{ route('admin_product_update', ['product' => $product->url_name]) }}">
                                <i class="icon icon-pencil"></i> Edit info
                            </a>
                        </h4><br>
                        <div class="col-md-4">
                            @if ($product->image)
                                <form method="POST"
                                      action="{{ route('admin_product_store', ['product' => $product->url_name]) }}"
                                      enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                        <img id="logo-image" src="{{ asset($product->image->path) }}"
                                             class="img-responsive center-block"><br>
                                        <label class="btn btn-default btn-primary">
                                            Update Image <input id="logo" type="file" class="form-control file"
                                                                name="image"
                                                                style="display: none;" onchange="SwitchButton()"
                                                                required>
                                        </label>
                                        <button type="submit" id="save-btn" class="btn btn-primary"
                                                style="display: none;">
                                            Save
                                        </button>
                                        @if ($errors->has('image'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </form>
                            @else
                                <form method="POST"
                                      action="{{ route('admin_product_store', ['product' => $product->url_name]) }}"
                                      enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                        <img id="logo-image" src="{{ asset('images/no-image.jpeg') }}"
                                             class="img img-thumbnail">
                                        <label class="btn btn-default btn-primary">
                                            Select Image <input id="logo" type="file" class="form-control file"
                                                                name="image"
                                                                style="display: none;" onchange="SwitchButton()"
                                                                required>
                                        </label>
                                        <button type="submit" id="save-btn" class="btn btn-primary"
                                                style="display: none;">
                                            Save
                                        </button>
                                        @if ($errors->has('image'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </form>
                            @endif
                        </div>
                        <div class="col-md-8">
                            <p>Brand: <strong>{{ $product->brand->name }}</strong></p>
                            <p>Category: <strong>{{ $product->category->name }}</strong></p>
                            @if ($product->parent)
                            <p>Current product has <strong>Base product :</strong>
                                <a href="{{ route('admin_product_view', ['product' => $product->parent->url_name]) }}">
                                    {{ $product->parent->name }}
                                </a>
                            </p>
                            @endif
                            @if (count($product->children))
                                <p>Current product is a base product. It has {{ count($product->children) }} children <br>
                                    <strong>Related products :</strong>
                                    @foreach($product->children as $child)
                                        <a href="{{ route('admin_product_view', ['product' => $child->url_name]) }}">
                                            {{ $child->name }}
                                        </a><br>
                                    @endforeach
                                </p>
                            @endif
                            <p>
                                <a class="btn btn-success"
                                   href="{{ route('admin_product_merge', ['product' => $product->url_name]) }}">
                                    Manage Synonyms
                                </a>
                            </p>
                            <p>Is popular?: @if ($product->is_popular == 1)
                                    Yes<br><br>
                                    <a class="btn btn-danger"
                                       href="{{ route('admin_remove_popular', ['product' => $product->url_name]) }}">
                                        <i class="icon-fixed-width icon-ban-circle" style="color:red"></i> Remove from
                                        popular?
                                    </a>
                                @else
                                    No<br>
                                    <a class="btn btn-success"
                                       href="{{ route('admin_make_popular', ['product' => $product->url_name]) }}">
                                        <i class="icon-li icon-ok"></i> Make popular?
                                    </a>
                                @endif
                            </p>
                            <p>Current product has {{ count($product->descriptions) }} description(s)<br>
                                <a class="btn btn-success"
                                   href="{{ route('admin_descriptions_view', ['type' => 'product', 'name' => $product->url_name]) }}">
                                    <i class="icon-pencil"></i> Add/Edit Description
                                </a>
                            </p>
                            <p>Current product has {{ count($product->propositions) }} proposition(s)<br>
                                <a class="btn btn-success"
                                   href="{{ route('admin_product_proposition_view', ['product' => $product->url_name]) }}">
                                    <i class="icon-pencil"></i> Add/Edit Propositions
                                </a>
                            </p>
                        </div>
                        <br>
                        @if(count($product->monthBestPrices))
                            <div class="col-md-offset-2 col-md-8">
                                @include('admin.products.components.month_best_price', ['product' => $product])
                            </div>
                            <br>
                        @endif
                        <div class="col-md-offset-2 col-md-8">
                            @include('admin.products.components.prices_table', ['product' => $product, 'filters' => $filters])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function SwitchButton() {
            $("#save-btn").slideToggle("fast");
            return false;
        }
    </script>
@endsection