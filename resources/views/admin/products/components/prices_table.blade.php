<h4 class="text-center"><strong>Sites with available prices:</strong></h4>
<table class="table table-responsive table-hover">
    <thead class="text-uppercase">
    <tr class="info">
        <th>N</th>
        <th>Price <i class="icon-money"></i></th>
        <th>Exist?</th>
        <th>Condition</th>
        <th>Network</th>
    </tr>
    </thead>
    <tbody>
    @foreach($product->profiles as $profile)
        <tr class="text-center">
            <td colspan="4"><i class="icon-group"></i>
                <strong>{{ $profile->site->name }}</strong></td>
        </tr>
        @php $count = 1; @endphp
        @foreach($profile->prices as $price)
            <tr>
                <td><strong>{{ $count }}</strong></td>
                <td><i class="icon-{{ strtolower($price->currency) }}"></i>
                    {{ $price->price }}
                </td>
                <td>
                    @if ($price->is_exist == 1)
                        <i class="icon-li icon-ok" style="color:green"></i>
                    @else
                        <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
                    @endif
                </td>
                <td>{{ $filters['condition'][$price->condition] }}</td>
                <td>{{ $filters['network'][$price->network] }}</td>
            </tr>
            @php $count++; @endphp
        @endforeach
    @endforeach
    </tbody>
</table>