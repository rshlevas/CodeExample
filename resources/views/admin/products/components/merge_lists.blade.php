<div class="col-md-12" style="display: flex;">
<div style="display: flex; flex: 0 50%; width: 100%">
        <ul id='synonyms'
            data-token="{{ csrf_token() }}"
            data-href="{{ route('admin_product_merge_store', ['product' => $baseProduct->url_name]) }}"
            class="list-group connectedSortable synonyms rounded"
            style="border: 1px dotted lightblue; width: 95%;">
            <li class="list-group-item synonyms clearfix list-group-item-success">
                <div class="col-md-8 col-md-offset-2">
                    <h4>Synonyms</h4>
                    <span class="align-middle">Drag potential synonym here</span>
                </div>
            </li>
            @foreach($relatedProducts as $product)
                <li data-id="{{ $product->id }}" class="synonyms list-group-item clearfix">
                    <div>
                        <div class="col-md-3">
                            @if ($product->imageThumbnail)
                                <img style="max-height: 120px;"
                                     class="img-responsive center-block"
                                     src="{{ asset($product->imageThumbnail->path) }}">
                            @elseif ($product->image)
                                <img style="max-height: 120px;"
                                     class="img-responsive center-block"
                                     src="{{ asset($product->image->path) }}">
                            @else
                                <img style="height: 80px;"
                                     class="img-responsive center-block"
                                     src="{{ asset('images/no-image.jpeg') }}">
                            @endif
                        </div>
                        <div class="col-md-9">
                            <span class="align-middle">{{$product->name}}</span><br>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>

</div>
<div style="display: flex; flex: 0 50%; width: 100%">
    <ul class="list-group connectedSortable" style="border: 1px dotted lightblue; width: 95%;">
        <li class="list-group-item potential clearfix list-group-item-info">
            <div class="col-md-8 col-md-offset-2">
                <h4>Potential synonyms</h4>
                <span class="align-middle">Drag here to remove synonym</span>
            </div>
        </li>
        @foreach($potentialSynonyms as $product)
            <li data-id="{{ $product->id }}" class="potential list-group-item clearfix">
                <div>
                    <div class="col-md-3">
                        @if ($product->imageThumbnail)
                            <img style="max-height: 120px;"
                                 class="img-responsive center-block"
                                 src="{{ asset($product->imageThumbnail->path) }}">
                        @elseif ($product->image)
                            <img style="max-height: 120px;"
                                 class="img-responsive center-block"
                                 src="{{ asset($product->image->path) }}">
                        @else
                            <img style="height: 80px;"
                                 class="img-responsive center-block"
                                 src="{{ asset('images/no-image.jpeg') }}">
                        @endif
                    </div>
                    <div class="col-md-9">
                        <span class="align-middle">{{$product->name}}</span><br>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
</div>
</div>
<script>
    $(document).ready(function () {

        $("ul.connectedSortable").sortable({
            connectWith: ".connectedSortable",
            helper: "clone",
            cursor: "move",
            start: function(e, ui){
                ui.placeholder.height(ui.item.height());
            },
            zIndex: 99999,
            items: "> li:not(:first)",
            receive: function (event, ui) {
                var synonyms = $("#synonyms");
                var url = synonyms.attr('data-href');
                var ul = $('ul.synonyms');
                var rows = ul.find('li');
                var data = [];
                for (var i = 0; i < rows.length; i++) {
                    if (rows[i].getAttribute('data-id')) {
                        data.push(rows[i].getAttribute('data-id'));
                    }
                }
                var sendData = $.param({'synonyms' : JSON.stringify(data), '_token' : synonyms.attr('data-token')});

                $.ajax({
                    data: sendData,
                    type: 'POST',
                    url: url,
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
        });
    });
</script>