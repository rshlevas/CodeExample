<h4 class="text-center"><strong>Month best working prices:</strong></h4>
<table class="table table-responsive table-hover">
    <thead class="text-uppercase success">
    <tr class="success">
        <th>N</th>
        <th>Month</th>
        <th>Year</th>
        <th>Price</th>
        <th>Manage <i class="icon icon-gears"></i></th>
    </tr>
    </thead>
    <tbody>
    @php $count = 1; @endphp
        @foreach($product->monthBestPrices as $monthPrice)
            <tr>
                <td><strong>{{ $count }}</strong></td>
                <td>{{ $monthPrice->updated_at->format('F') }}</td>
                <td>{{ $monthPrice->updated_at->year }}</td>
                <td>{{ $monthPrice->price }}</td>
                <td>
                    <a class="btn btn-danger"
                       href="#" data-toggle="modal" data-target="#confirm-delete"
                       data-href="{{ route('admin_month_price_delete', ['price' => $monthPrice->id]) }}">
                        <i class="icon-trash icon-large"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    @php $count++; @endphp
    </tbody>
</table>
<div class="modal fade" id="confirm-delete"
     tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    @include('admin.components.modal_window', ['target' => 'this month price'])
</div>