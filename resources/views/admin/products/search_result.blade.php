@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li><a href="{{ route('admin_search') }}">Search</a></li>
                            <li class="active">Search Result</strong></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        @if (count($products))
                            <div class="col-md-10 col-md-offset-1 text-center">
                                <h4>{{ $products->total() }} products were found</h4><br>
                            </div>
                            @foreach($products as $product)
                                <div class="col-lg-3 col-sm-4 col-xs-6">
                                    <div class="panel panel-primary" style="height: 220px;">
                                        <div class="panel-heading">{{ $product->brand->name }}</div>
                                        <div class="panel-body">
                                            @if ($product->imageThumbnail)
                                                <img style="height: 80px;" class="thumbnail img-responsive center-block"
                                                     src="{{ asset($product->imageThumbnail->path) }}">
                                            @elseif ($product->image)
                                                <img style="height: 80px;" class="thumbnail img-responsive center-block"
                                                     src="{{ asset($product->image->path) }}">
                                            @else
                                                <img style="height: 80px;" class="thumbnail img-responsive center-block"
                                                     src="{{ asset('images/no-image.jpeg') }}">
                                            @endif
                                            <div class="text-center">
                                                <a class="link-unstyled"
                                                   href="{{ route('admin_product_view', ['product' => $product->url_name]) }}">
                                                    {{ $product->model }}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @endforeach
                            <div class="col-md-10 col-md-offset-1 text-center">
                                <h4>{{ $products->links() }}</h4><br>
                            </div>
                        @else
                            <div class="col-md-8 col-md-offset-2">
                                <h4>Nothing was found</h4>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection