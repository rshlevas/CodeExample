@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li><a href="{{ route('admin_product_view', ['product' => $product->url_name]) }}">
                                    {{ $product->url_name }}</a></li>
                            <li><a href="{{ route('admin_product_proposition_view', ['product' => $product->url_name]) }}">
                                    Propositions for {{ $product->url_name }}</a></li>
                            <li class="active">Edit Proposition</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <h4 class="text-center">Fill the trade in proposition info</h4>
                        <form class="form-horizontal" method="POST" action="{{ route('admin_product_proposition_storage', [
                            'product' => $product->url_name
                        ]) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="propositionId" value="{{ $proposition->id }}">
                            <div class="form-group">
                                <label for="proposition" class="col-md-4 control-label">Proposition</label>
                                <div class="col-md-6">
                                    <select name="proposition_id" class="form-control selectpicker"  data-style="btn-primary" id="proposition">
                                        @foreach($mainPropositions as $item)
                                            <option value="{{ $item->id }}"
                                                {{ $item->id === $proposition->proposition_id ? 'selected' : '' }}
                                            >{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="price" class="col-md-4 control-label">Price</label>
                                <div class="col-md-6">
                                    <input id="price" type="text" class="form-control" name="price"
                                           value="{{ $proposition->price }}" required>
                                    @if ($errors->has('price'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('price') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="condition" class="col-md-4 control-label">Condition</label>
                                <div class="col-md-6">
                                    <select name="condition_key" class="form-control selectpicker"  data-style="btn-primary" id="condition">
                                        @foreach($conditions as $condition)
                                            <option value="{{ $condition->alias }}"
                                                {{ $condition->alias === $proposition->condition_key ? 'selected' : '' }}
                                            >{{ $condition->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection