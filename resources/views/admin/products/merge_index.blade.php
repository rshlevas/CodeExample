@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li>
                                <a href="{{ route('admin_product_view', ['product' => $baseProduct->url_name]) }}">
                                    {{ $baseProduct->name }}
                                </a>
                            </li>
                            <li class="active">Product relations</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        @if ($message)
                            @if ($status)
                                @include('admin.components.success_message', ['message' => $message])
                            @else
                                @include('admin.components.fault_message', ['message' => $message])
                            @endif
                        @endif
                        <h4 class="text-center"><strong>Synonyms of {{ $baseProduct->name }}:</strong></h4>
                        <div class="col-md-8 col-md-offset-2">
                            <form class="form-inline" method="POST" action="{{ route('admin_product_synonym_add', ['product' => $baseProduct->url_name]) }}">
                            {{ csrf_field() }}
                            <div class="input-group">
                                <input id="search" type="text" class="form-control" name="search" size="50"
                                       placeholder="Enter synonym name" required>
                                <div class="input-group-btn">
                                    <button id="add" type="submit" class="btn btn-info">+ Add Synonym</button>
                                </div>
                            </div>
                            </form>
                        </div>
                        <br>
                        <br>
                            @include('admin.products.components.merge_lists', [
                                'baseProduct' => $baseProduct,
                                'relatedProducts' => $relatedProducts,
                                'potentialSynonyms' => $potentialSynonyms
                            ])

                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $("#search").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "{{ route('admin_search_autocomplete') }}",
                            type: "POST",
                            dataType: "json",
                            data: {search: request.term},
                            success: function (data) {
                                response($.map(data, function (item) {
                                    return item;
                                }));
                            }
                        });
                    },
                    focus: function (event, ui) {
                        $("#search").val(ui.item.title);
                        return false;
                    },
                    select: function (event, ui) {
                        $("#search").val(ui.item.title);
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    var inner_html;
                    if (item.message) {
                        inner_html = '<div class="col-md-8 col-sm-offset-2 small">' +
                            item.message + '</div>';
                    } else {
                        inner_html = '<div class="col-md-3 col-sm-3 col-xs-3"><img style="height: 60px;"' +
                            ' class="img-responsive center-block" src="' + item.image +
                            '" ></div><div class="col-md-2 col-sm-2 col-xs-2"><h4 class="text-center small"><b>' +
                            '<i class="icon-' + item.currency + '"></i> ' + item.price +
                            '</b></h4></div><div class="col-md-6 col-sm-6 col-xs-6 small text-center">' +
                            item.name + '</div>';
                    }
                    return $("<li class=\"ui-menu-item\"></li>")
                        .data("item.autocomplete", item)
                        .append(inner_html)
                        .appendTo(ul);
                };
            });
        </script>
@endsection