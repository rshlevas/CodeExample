@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li><a href="{{ route('admin_product_view', ['product' => $product->url_name]) }}">{{ $product->name }}</a></li>
                            <li class="active text-capitalize">Edit</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <h4 class="text-center">Fill the produuct params you want to update</h4>
                        <form class="form-horizontal" method="POST"
                              action="{{ route('admin_product_store', ['product' => $product->url_name]) }}"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Name</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{ $product->name }}" required>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="url_name" class="col-md-4 control-label">Url name</label>
                                <div class="col-md-6">
                                    <input id="url_name" type="text" class="form-control" name="url_name"
                                           value="{{ $product->url_name }}" required>
                                    @if ($errors->has('url_name'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('url_name') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="category" class="col-md-4 control-label">Category</label>
                                <div class="col-md-6">
                                    <select name="category_id" class="form-control selectpicker"  data-style="btn-primary" id="category">
                                        @foreach($categories as $category)
                                            <option {{ $product->category_id == $category->id ? 'selected' : '' }}
                                                    value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection