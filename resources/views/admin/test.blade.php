@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="col-md-3">Base</th>
                        <th class="col-md-9">Synonyms</th>
                    </tr>
                </thead>
                <tbody>
                @php $baseCount = 0; $itemCount = 0; @endphp
                    @foreach ($products as $name => $synonyms)
                        <tr>
                            <td class="col-md-3">{{ $name }}</td>
                            <td class="col-md-9">
                                @foreach($synonyms as $synonym)
                                   {{ $synonym->id }} {{ $synonym->name }} Profiles: {{ count($synonym->profiles) }}<br>
                                    @php $itemCount++; @endphp
                                @endforeach
                            </td>
                        </tr>
                        @php $baseCount++; @endphp
                    @endforeach
                <tr><td>{{ $baseCount }}</td><td>{{ $itemCount }}</td></tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection