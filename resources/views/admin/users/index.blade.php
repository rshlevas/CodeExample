@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li class="active">Users</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        @if ($message)
                            @include('admin.components.success_message', ['message' => $message])
                        @endif
                        <h3 class="text-center">Users section managing</h3>
                        <a href="{{ route('admin_users_create') }}" class="btn btn-primary">+ Add new user</a>
                        <table class="table table-hover text-center">
                            <thead class="text-uppercase">
                                <tr>
                                    <th class="col-md-3 info text-center">Name</th>
                                    <th class="col-md-3 info text-center">Email</th>
                                    <th class="col-md-3 info text-center">Role</th>
                                    <th class="col-md-2 info text-center">Manage <i class="icon-fixed-width icon-cogs"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->role->name }}</td>
                                        <td>
                                            <a class="btn btn-success"
                                               href="{{ route('admin_users_update', ['user' => $user->id]) }}">
                                                <i class="icon-fixed-width icon-pencil"></i>
                                            </a>
                                            <a class="btn btn-danger"
                                               href="#" data-toggle="modal" data-target="#confirm-delete"
                                               data-href="{{ route('admin_users_delete', ['user' => $user->id]) }}">
                                                <i class="icon-trash icon-large"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                            <div class="modal fade" id="confirm-delete"
                                 tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                @include('admin.components.modal_window', ['target' => 'this user'])
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection