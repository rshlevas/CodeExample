@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li class="active">Import images</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        @if ($message)
                            @include('admin.components.success_message', ['message' => $message])
                        @endif
                        <h4 class="text-center">Upload zip file with images</h4>
                        <p>Images should be direct at archive without additional folders. The name of images should be
                        the same as url names of related entities (sites, brands, products)</p>
                        <form class="form-horizontal" method="POST" action="{{ route('admin_image_import_store') }}"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="group" class="col-md-4 control-label">Group</label>
                                <div class="col-md-6">
                                    <select name="group" class="form-control selectpicker"  data-style="btn-primary" id="group">
                                        <option value="sites">Sites</option>
                                        <option value="brands">Brands</option>
                                        <option value="products">Products</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('zip') ? ' has-error' : '' }}">
                                <label for="zip" class="col-md-4 control-label">Archive</label>

                                <div class="col-md-6">
                                    <input id="zip" type="file" class="form-control" name="zip" required>
                                    @if ($errors->has('zip'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('zip') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Import
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection