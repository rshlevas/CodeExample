@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                    </div>
                    <div class="panel-body">
                        <div>{!! Chart::display("id-highchartsnya", $charts) !!}</div>
                        <br>
                        @include('admin.components.reports_section', ['groups' => $groups, 'sitesWithoutGroup' => $sitesWithoutGroup])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection