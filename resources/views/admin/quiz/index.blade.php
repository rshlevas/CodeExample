@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li class="active">Quiz</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        @if ($message)
                            @include('admin.components.success_message', ['message' => $message])
                        @endif
                        @include('admin.quiz.components.questions_table', ['questions' => $questions])
                        <br>
                        @include('admin.quiz.components.conditions_table', ['conditions' => $conditions])
                        <div class="modal fade" id="confirm-delete"
                             tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            @include('admin.components.modal_window', ['target' => 'this question'])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/bootstrap-number-input.js') }}"></script>
    <script>
        $('.number-input').bootstrapNumber({
            upClass: 'danger',
            downClass: 'success',
            center: true
        });
    </script>
@endsection