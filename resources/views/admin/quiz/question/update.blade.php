@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li><a href="{{ route('admin_quiz_main') }}">Quiz</a></li>
                            <li class="active text-capitalize">Question Editing</li>
                        </ul>
                    </div>

                    <div class="panel-body">
                        <h4 class="text-center">Edit the question</h4>
                        <div class="col-md-offset-2">
                            <a class="btn btn-default"
                                href="{{ route('admin_question_translation_view', ['question' => $question->id]) }}">
                                    View translations ({{ count($question->children) }})
                            </a>
                        </div>
                        @include('admin.quiz.components.update_form', ['question' => $question])
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var max_fields = 7;
            var wrapper = $("#answers_section");
            var add_button = $(".add_form_field");

            var answerNumber = +($('.answer_number').val());
            var x = answerNumber;
            getAnswerInput = function (number) {
                var innerHtml = '<div class="answer-element"><h4 class="text-center">Answer' +
                    ' <a href="#" class="delete"><i class="icon-remove"></i></a></h4>' +
                    '<div class="form-group ">' +
                    '<label for="answer-' + number + '" class="col-md-4 control-label label-content">Content' +
                    '</label><div class="col-md-6">' +
                    '<input id="answer-' + number + '" type="text" class="form-control input-content" name="answer-' +
                    number + '" placeholder="Enter answer" required>' +
                    '</div></div><div class="form-group">' +
                    '<label for="answer-weight-' + number + '" class="col-md-4 control-label label-weight">' +
                    'Weight</label><div class="col-md-6"><input id="answer-weight-' + number +
                    '" type="number" class="form-control input-weight" name="answer-weight-' + number +
                    '" value="0" required></div></div>' +
                    '</div>';

                return innerHtml;
            };
            renameAnswerElement = function(length) {
                $('#answers_section').find('.answer-element').each(function (key, el) {
                    var order = key + 1;

                    $(el).find('.label-content').attr('for', 'answer-' + order);
                    $(el).find('.input-content').attr('id', 'answer-' + order);
                    $(el).find('.input-content').attr('name', 'answer-' + order);
                    $(el).find('.label-weight').attr('for', 'answer-weight-' + order);
                    $(el).find('.input-weight').attr('id', 'answer-weight-' + order);
                    $(el).find('.input-weight').attr('name', 'answer-weight-' + order);
                });
            };
            $(add_button).click(function (e) {
                e.preventDefault();
                if (x < max_fields) {
                    x++;
                    answerNumber++;
                    $(wrapper).append(getAnswerInput(answerNumber)); //add input box
                }
                else {
                    alert('You Reached the limits')
                }
            });

            $(wrapper).on("click", ".delete", function (e) {
                e.preventDefault();
                $(this).parent('h4').parent('div').remove();
                var count = $('#answers_section').find('.answer-element').length;
                console.log(count);
                renameAnswerElement(count);
                x--;
                answerNumber--;
            })
        });
    </script>

@endsection