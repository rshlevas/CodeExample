<h3 class="text-center">Quiz section managing</h3>
<a href="{{ route('admin_question_create') }}" class="btn btn-primary">+ Add new question</a>
<table class="table table-hover text-center">
    <thead class="text-uppercase">
    <tr>
        <th class="col-md-1 info text-center">№</th>
        <th class="col-md-6 info text-center">Questions</th>
        <th class="col-md-1 info text-center">Answers number</th>
        <th class="col-md-3 info text-center">Manage <i
                    class="icon-fixed-width icon-cogs"></i></th>
    </tr>
    </thead>
    <tbody class="sortable" data-url="{{ route('admin_question_order_store') }}">
    @php $number = 1; @endphp
    @foreach($questions as $question)
        <tr data-id="{{ $question->id }}">
            <td>{{ $number }}</td>
            <td>{{ $question->content }}</td>
            <td>{{ count($question->answers) }}</td>
            <td>
                <a class="btn btn-default"
                   href="{{ route('admin_question_translation_view', ['question' => $question->id]) }}">
                    Translations ({{ count($question->children) }})
                </a>
                <a class="btn btn-success"
                   href="{{ route('admin_question_update', ['question' => $question->id]) }}">
                    <i class="icon-fixed-width icon-pencil"></i>
                </a>
                <a class="btn btn-danger"
                   href="#" data-toggle="modal" data-target="#confirm-delete"
                   data-href="{{ route('admin_question_delete', ['question' => $question->id]) }}">
                    <i class="icon-trash icon-large"></i>
                </a>
            </td>
        </tr>
        @php $number++; @endphp
    @endforeach
    </tbody>
</table>

<script>
    $('.sortable').sortable({
        axis: 'y',
        update: function (event, ui) {
            var tbody = $(this).sortable();
            var url = tbody.data('url');
            var rows = $(this).find("tr");
            var data = [];
            var order = 1;
            for (var i = 0; i < rows.length; i++) {
                var question = { 'order' : order, 'id':rows[i].getAttribute('data-id')};
                data.push(question);
                order++;
            }
            var sendData = $.param({'questions' : JSON.stringify(data)});

            $.ajax({
                data: sendData,
                type: 'POST',
                url: url
            });
        }
    });
</script>