<form class="form-horizontal" method="POST" action="{{ route('admin_descriptions_store', ['type' => $type]) }}">
    {{ csrf_field() }}
    <input type="hidden" value="{{ $model->id }}" name="relationId">
    @if (isset($description))
        <input type="hidden" value="{{ $description->id }}" name="descriptionId">
    @endif
    <div class="form-group">
        <label for="language" class="col-md-2 control-label">Language <i class="icon-flag"></i></label>
        <div class="col-md-10">
            @if(isset($description))
                <select name="language" class="form-control selectpicker" data-style="btn-primary" id="language"
                        disabled>
                    <option value="{{ $description->language }}">{{ $languages[$description->language] }}</option>

                </select>
            @else
                <select name="language" class="form-control selectpicker" data-style="btn-primary" id="language">
                    @foreach($languages as $lang => $name)
                        <option value="{{ $lang }}">{{ $name }}</option>
                    @endforeach
                </select>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="content" class="col-md-2 control-label">Description</label>
        <div class="col-md-10">
            <textarea id="content" rows="10" class="summernote form-control" name="content"
                      placeholder="Enter the description"
                      required>{{ isset($description) ? $description->content : '' }}</textarea>
            @if ($errors->has('content'))
                <span class="help-block">
                <strong>{{ $errors->first('content') }}</strong>
            </span>
            @endif
        </div>

    </div>
    <div class="form-group">
        <div class="col-md-9 col-md-offset-2">
            <button type="submit" class="btn btn-primary">
                Save
            </button>
            @if(isset($description))
                <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#confirm-delete"
                   data-href="{{ route('admin_descriptions_delete', ['description' => $description->id]) }}">
                    <i class="icon-trash"></i> Delete
                </a>
                <button class="btn btn-primary cancel"
                        data-id="{{ $description->id }}"
                        type="button">
                    <i class="icon-ban-circle"></i> Cancel
                </button>
            @endif
        </div>
    </div>
</form>