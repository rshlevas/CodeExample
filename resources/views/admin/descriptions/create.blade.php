@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li>
                                <a href="
                                @if($type === 'site')
                                {{ route('admin_sites_view', ['site' => $model->url_name]) }}
                                @elseif($type === 'product')
                                {{ route('admin_product_view', ['product' => $model->url_name]) }}
                                @endif
                                        ">
                                    {{ $model->name }}
                                </a>
                            </li>
                            <li>
                                <a href="
                                @if($type === 'site')
                                {{ route('admin_descriptions_view', ['type' => $type, 'name' => $model->url_name]) }}
                                @elseif($type === 'product')
                                {{ route('admin_descriptions_view', ['type' => $type, 'product' => $model->url_name]) }}
                                @endif
                                        ">
                                    Descriptions
                                </a>
                            </li>
                            <li class="active">Create Description</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <h4 class="text-center">Fill the {{ $type }} description</h4>
                        <div class="col-md-12">
                                @include('admin.descriptions.form', [
                                    'languages' => $languages,
                                    'model' => $model,
                                ])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.js"></script>
    <script>

        $(document).ready(function() {

            $('.summernote').summernote({
                height: 200,
                codemirror: {
                    theme: 'paper'
                }
            });
        });

    </script>
@endsection