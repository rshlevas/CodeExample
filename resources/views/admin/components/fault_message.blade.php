<div class="text-center alert alert-danger">
    <span>
        <strong>Whoooops! {{ $message }}!</strong>
    </span>
</div>