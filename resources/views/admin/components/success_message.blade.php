<div class="text-center alert alert-success">
    <span>
        <strong>Success! {{ $message }}!</strong>
    </span>
</div>