<h3 class="text-center">Parsing reports</h3>
<a href="#" data-toggle="modal" data-target="#confirm-delete"
   data-href="{{ route('admin_reports_delete_all') }}"
   class="btn btn-primary"><i class="icon-trash icon-large"></i>
    Delete all reports
</a>
<table class="table table-hover text-center">
    <thead class="text-uppercase">
    <tr>
        <th class="col-md-3 info text-center">Site name</th>
        <th class="col-md-2 info text-center">Last date</th>
        <th colspan="2" class="col-md-4 info text-center">Parsing states</th>
        <th class="col-md-3 info text-center">Manage <i class="icon-fixed-width icon-cogs"></i></th>
    </tr>
    </thead>
    <tbody>
    @foreach($groups as $group)
        <tr>
            <td colspan="6"><i class="icon-group"></i> <strong>{{ $group->name }} group</strong>
            </td>
        </tr>
        @foreach($group->sites as $site)
            <tr>
                <td class="text-uppercase">
                    <a href="{{ route('admin_sites_view', ['site' => $site->url_name]) }}">
                        <strong>{{ $site->name }} ({{ count($site->allReports) }})</strong>
                    </a>
                </td>
                @if(count($site->allReports))
                    <td>{{ $site->last_parsing  }}</td>
                    <td>
                        @if(count($site->passedReports))
                            <a href="{{ route('admin_reports_view', ['site' => $site->url_name, 'type' => 'passed']) }}"
                               class="btn btn-success">Passed attempts
                                <strong>({{ count($site->passedReports) }})</strong></a>
                        @else
                            <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
                        @endif
                    </td>
                    <td>
                        @if(count($site->failedReports))
                            <a href="{{ route('admin_reports_view', ['site' => $site->url_name, 'type' => 'failed']) }}"
                               class="btn btn-danger">Failed attempts
                                <strong>({{ count($site->failedReports) }})</strong></a></td>
                @else
                    <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
                @endif
                <td>
                    <a href="{{ route('admin_reports_view', ['site' => $site->url_name, 'type' => 'all']) }}"
                       class="btn btn-default">
                        <i class="icon-fixed-width icon-eye-open"></i> View
                    </a>
                    <a href="#" data-toggle="modal" data-target="#confirm-delete"
                       data-href="{{ route('admin_reports_delete', ['site' => $site->url_name, 'type' => 'all']) }}"
                       class="btn btn-primary">
                        <i class="icon-trash icon-large"></i> Clear
                    </a>
                </td>
                @else
                    <td class="text-uppercase" colspan="3">You don't have reports for {{ $site->name }}</td>
                @endif

            </tr>
        @endforeach
    @endforeach
    @if (count($sitesWithoutGroup))
        <tr>
            <td colspan="6"><i class="icon-group"></i> <strong>Sites without group</strong>
            </td>
        </tr>
        @foreach($sitesWithoutGroup as $site)
            <tr>
                <td class="text-uppercase">
                    <a href="{{ route('admin_sites_view', ['site' => $site->url_name]) }}">
                        <strong>{{ $site->name }} ({{ count($site->allReports) }})</strong>
                    </a>
                </td>
                @if(count($site->allReports))
                    <td>{{ $site->last_parsing  }}</td>
                    <td>
                        @if(count($site->passedReports))
                            <a href="{{ route('admin_reports_view', ['site' => $site->url_name, 'type' => 'passed']) }}"
                               class="btn btn-success">Passed attempts
                                <strong>({{ count($site->passedReports) }})</strong></a>
                        @else
                            <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
                        @endif
                    </td>
                    <td>
                        @if(count($site->failedReports))
                            <a href="{{ route('admin_reports_view', ['site' => $site->url_name, 'type' => 'failed']) }}"
                               class="btn btn-danger">Failed attempts
                                <strong>({{ count($site->failedReports) }})</strong></a></td>
                @else
                    <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
                @endif
                <td>
                    <a href="{{ route('admin_reports_view', ['site' => $site->url_name, 'type' => 'all']) }}"
                       class="btn btn-default">
                        <i class="icon-fixed-width icon-eye-open"></i> View
                    </a>
                    <a href="#" data-toggle="modal" data-target="#confirm-delete"
                       data-href="{{ route('admin_reports_delete', ['site' => $site->url_name, 'type' => 'all']) }}"
                       class="btn btn-primary">
                        <i class="icon-trash icon-large"></i> Clear
                    </a>
                </td>
                @else
                    <td class="text-uppercase" colspan="3">You don't have reports for {{ $site->name }}</td>
                @endif

            </tr>
        @endforeach
    @endif
    </tbody>
</table>
<div class="modal fade" id="confirm-delete"
     tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    @include('admin.components.modal_window', ['target' => 'these reports'])
</div>