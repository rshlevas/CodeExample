@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li class="active">Propositions</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        @if ($message)
                            @include('admin.components.success_message', ['message' => $message])
                        @endif
                        <h3 class="text-center">Trade in propositions section managing</h3>
                        <a href="{{ route('admin_propositions_create') }}" class="btn btn-primary">+ Add new proposition</a>
                        <table class="table table-hover text-center">
                            <thead class="text-uppercase">
                                <tr>
                                    <th class="col-md-3 info text-center">Name</th>
                                    <th class="col-md-3 info text-center">Site</th>
                                    <th class="col-md-4 info text-center">Products</th>
                                    <th class="col-md-2 info text-center">Manage <i class="icon-fixed-width icon-cogs"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($propositions as $proposition)
                                    <tr>
                                        <td>{{ $proposition->name }}</td>
                                        <td>{{ $proposition->site->name }}</td>
                                        <td>@if (count($proposition->products))
                                                @foreach($proposition->products as $product)
                                                    <a href="{{ route('admin_product_view', ['product' => $product->url_name]) }}">
                                                        {{ $product->name }}
                                                    </a> <br>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-success" href="{{ route('admin_propositions_update', ['proposition' => $proposition->id]) }}">
                                                <i class="icon-fixed-width icon-pencil"></i>
                                            </a>
                                            <a class="btn btn-danger"
                                               href="#" data-toggle="modal" data-target="#confirm-delete"
                                               data-href="{{ route('admin_propositions_delete', ['proposition' => $proposition->id]) }}">
                                                <i class="icon-trash icon-large"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                            <div class="modal fade" id="confirm-delete"
                                 tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                @include('admin.components.modal_window', ['target' => 'this proposition'])
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection