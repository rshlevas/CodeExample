@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li><a href="{{ route('admin_propositions_main') }}">Trade in propositions</a></li>
                            <li class="active">Add Proposition</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <h4 class="text-center">Fill the trade in proposition info</h4>
                        <form class="form-horizontal" method="POST" action="{{ route('admin_propositions_storage') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Name</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name"
                                           placeholder="Enter title" required>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="link" class="col-md-4 control-label">Link</label>
                                <div class="col-md-6">
                                    <input id="link" type="text" class="form-control" name="link"
                                           placeholder="Enter link" required>
                                    @if ($errors->has('link'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('link') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="site" class="col-md-4 control-label">Site</label>
                                <div class="col-md-6">
                                    <select name="site_id" class="form-control selectpicker"  data-style="btn-primary" id="site">
                                        @foreach($sites as $site)
                                            <option value="{{ $site->id }}">{{ $site->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection