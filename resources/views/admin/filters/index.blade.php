@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li class="active">Filters</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        @if ($message)
                            @include('admin.components.success_message', ['message' => $message])
                        @endif
                        <h3 class="text-center">Filters section managing</h3>
                        <h4 class="text-center">Conditions</h4>
                        <a href="{{ route('admin_quiz_main') }}" class="btn btn-primary">Edit the conditions weight</a>
                        @include('admin.filters.filter_table', ['filters' => $conditions, 'type' => 'condition'])
                        <h4 class="text-center">Networks</h4>
                        @include('admin.filters.filter_table', ['filters' => $networks, 'type' => 'network'])


                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('.sortable').sortable({
            axis: 'y',
            items: "> tr:not(:first)",
            cursor: "move",
            update: function (event, ui) {
                var tbody = $(this).sortable();
                var url = tbody.data('url');
                var rows = $(this).find("tr");
                var data = [];
                var order = 1;
                for (var i = 0; i < rows.length; i++) {
                    var question = { 'order' : order, 'id':rows[i].getAttribute('data-id')};
                    data.push(question);
                    order++;
                }
                var sendData = $.param({'filters' : JSON.stringify(data), '_token' : tbody.attr('data-token')});

                $.ajax({
                    data: sendData,
                    type: 'POST',
                    url: url
                });
            }
        });
    </script>
@endsection