<table class="table table-hover">
    <thead class="text-uppercase">
    <tr>
        <th class="col-md-1 info text-center">Order</th>
        <th class="col-md-5 info text-center">Name</th>
        <th class="col-md-2 info text-center">Edit</th>
        <th class="col-md-2 info text-center">Disable/Enable <i class="icon-fixed-width icon-cogs"></i></th>
    </tr>
    </thead>
    <tbody class="sortable"
           data-token="{{ csrf_token() }}"
           data-url="{{ route('admin_filters_order_store', ['type' => $type]) }}">
    @php $number = 1; @endphp
    @foreach($filters as $filter)
        <tr data-id="{{ $filter->id }}" @if($number === 1) style="background-color: lightblue;" @endif>
            <td>{{ $number }}</td>
            <td>{{ $filter->name }}</td>
            <td>
                <a class="btn btn-success"
                   href="{{ route('admin_filters_update', ['type' => $type, 'filter' => $filter->id]) }}">
                    <i class="icon-fixed-width icon-pencil"></i>
                </a>
            </td>
            <td>
                @if($filter->is_active === 0)
                    <a class="btn btn-success"
                       href="{{ route('admin_filters_active_switch', ['type' => $type, 'filter' => $filter->id]) }}">
                        Enable
                    </a>
                @else
                    <a class="btn btn-danger @if($number === 1) {{ 'disabled' }} @endif"
                       href="{{ route('admin_filters_active_switch', ['type' => $type, 'filter' => $filter->id]) }}">
                        Disable
                    </a>
                @endif
            </td>
        </tr>
        @php $number++; @endphp
    @endforeach
    </tbody>
</table>
