@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li><a href="{{ route('admin_filters_main') }}">Filters</a></li>
                            <li class="active text-capitalize">Update {{ ucfirst($type) }}: <strong>{{ $filter->name }}</strong></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <h4 class="text-center">Fill the site params you want to update</h4>
                        <form class="form-horizontal" method="POST" action="{{ route('admin_filters_store') }}">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{ $filter->id }}" name="filterId">
                            <input type="hidden" value="{{ $type }}" name="type">
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Name</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{ $filter->name }}" required>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="is_active" class="col-md-4 control-label">Active</label>
                                <div class="col-md-6">
                                    <select name="is_active" class="form-control selectpicker" data-style="btn-primary"
                                            id="is_active" {{ $filter->order == 1 ? 'disabled' : '' }}>
                                        <option data-icon="icon-ban-circle"
                                                {{ $filter->is_active == 0 ? 'selected' : '' }}
                                                value="0">No
                                        </option>
                                        <option data-icon="icon-ok"
                                                {{ $filter->is_active == 1 ? 'selected' : '' }} value="1">Yes
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection