@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li><a href="{{ route('admin_sites_view', ['site' => $site->url_name]) }}">
                                    <strong>{{ $site->name }}</strong>
                                </a>
                            </li>
                            <li class="active">{{ ucfirst($type) }} parsing reports</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <h4 class="text-center">{{ strtoupper($site->name) }} {{ $type }} parsing reports ({{ count($reports) }})</h4><br>
                        <a href="#" data-toggle="modal" data-target="#confirm-delete"
                           data-href="{{ route('admin_reports_delete', ['site' => $site->id, 'type' => $type]) }}"
                        class="btn btn-primary"><i class="icon-trash icon-large"></i> Delete all</a>
                        @switch($type)
                            @case('all')
                                @include('admin.reports.all', ['reports' => $reports])
                                @break
                            @case('passed')
                                @include('admin.reports.passed', ['reports' => $reports])
                                @break
                            @case('failed')
                                @include('admin.reports.failed', ['reports' => $reports])
                            @break
                        @endswitch
                    </div>
                    <div class="modal fade" id="confirm-delete"
                         tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        @include('admin.components.modal_window', ['target' => 'this site'])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection