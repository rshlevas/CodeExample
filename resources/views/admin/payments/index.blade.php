@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li class="active">Payments methods and terms</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        @if ($message)
                            @include('admin.components.success_message', ['message' => $message])
                        @endif
                        <h3 class="text-center">Payments section managing</h3>
                        <table class="table table-hover">
                            <thead class="text-uppercase">
                            <tr>
                                <th class="col-md-4 info text-center">Name</th>
                                <th class="col-md-2 info text-center">Manage <i class="icon-fixed-width icon-cogs"></i></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-center"><i class="icon-money"></i>  <strong>Payment methods ({{ count($methods) }}):</strong></td>
                                <td>
                                    <a class="btn btn-primary"
                                       href="{{ route('admin_payments_create', ['type' => 'method']) }}">
                                        + Add New One
                                    </a>
                                </td>
                            </tr>
                            @foreach($methods as $method)
                                <tr>
                                    <td>{{ $method->name }}</td>
                                    <td class="text-right">
                                        <a class="btn btn-success"
                                           href="{{ route('admin_payments_update', ['type' => 'method', 'id' => $method->id]) }}">
                                            <i class="icon-fixed-width icon-pencil"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td class="text-center"><i class="icon-calendar"></i>  <strong>Payment terms ({{ count($terms) }}):</strong></td>
                                <td>
                                    <a class="btn btn-primary"
                                       href="{{ route('admin_payments_create', ['type' => 'term']) }}">
                                        + Add New One
                                    </a>
                                </td>
                            </tr>
                            @foreach($terms as $term)
                                <tr>
                                    <td>{{ $term->name }}</td>
                                    <td class="text-right">
                                        <a class="btn btn-success"
                                           href="{{ route('admin_payments_update', ['type' => 'term', 'id' => $term->id]) }}">
                                            <i class="icon-fixed-width icon-pencil"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection