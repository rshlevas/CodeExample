@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li><a href="{{ route('admin_categories_view') }}">Categories</a></li>
                            <li class="active text-capitalize">Update Category: <strong>{{ $category->name }}</strong></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <h4 class="text-center">Fill the category params you want to update</h4>
                        <form class="form-horizontal" method="POST" action="{{ route('admin_categories_storage') }}">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{ $category->id }}" name="category_id">
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Name</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{ $category->name }}" required>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="url_name" class="col-md-4 control-label">Url name*</label>
                                <div class="col-md-6">
                                    <input id="url_name" type="text" class="form-control" name="url_name"
                                           value="{{ $category->url_name }}" required>
                                    @if ($errors->has('url_name'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('url_name') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection