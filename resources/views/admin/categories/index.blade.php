@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard
                        <ul class="breadcrumb">
                            <li><a href="{{ route('admin_main') }}">Main</a></li>
                            <li class="active">Categories</li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        @if ($message)
                            @include('admin.components.success_message', ['message' => $message])
                        @endif
                        <h3 class="text-center">Categories section managing</h3>
                        <a href="{{ route('admin_categories_create') }}" class="btn btn-primary">+ Add new category</a>
                        <table class="table table-hover text-center">
                            <thead class="text-uppercase">
                                <tr>
                                    <th class="col-md-3 info text-center">Name</th>
                                    <th class="col-md-3 info text-center">Number of products</th>
                                    <th class="col-md-2 info text-center">Manage <i class="icon-fixed-width icon-cogs"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($categories as $category)
                                    <tr>
                                        <td>{{ $category->name }}</td>
                                        <td>{{ count($category->products) }}</td>
                                        <td>
                                            <a class="btn btn-success" href="{{ route('admin_categories_update', ['category' => $category->url_name]) }}">
                                                <i class="icon-fixed-width icon-pencil"></i>
                                            </a>
                                            @if ($category->id !== 1)
                                                <a class="btn btn-danger"
                                                    href="#" data-toggle="modal" data-target="#confirm-delete"
                                                    data-href="{{ route('admin_categories_delete', ['category' => $category->url_name]) }}">
                                                    <i class="icon-trash icon-large"></i>
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                            <div class="modal fade" id="confirm-delete"
                                 tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                @include('admin.components.modal_window', ['target' => 'this category'])
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection