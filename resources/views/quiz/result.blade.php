<div class="demo">
    <div class="demo__content">
        <h1 class="demo__heading text-center">Result</h1>
        <div class="demo__elems">
            <div class="demo__elem">
                <h4 class="text-center">Your phone has <strong>{{ $condition->name }}</strong> condition</h4>
                <br>
                <a class="btn btn-primary" href="{{ route('quiz_main') }}">Try again</a>
            </div>

        </div>
    </div>
</div>
