<div class="demo">
    <div class="demo__content">
        <h2 class="demo__heading">{{ $question->content }}</h2>
        <div class="demo__elems">
            @foreach($question->answers as $answer)
                <div class="demo__elem">{{ $answer->content }}</div>
            @endforeach
            @php $number = 1; @endphp
            @foreach($question->answers as $answer)
                <span data-url="{{ route('quiz_next_question', ['answer' => $answer->id, 'step' => $step]) }}"
                      class="answer demo__hover demo__hover-{{ $number }}"></span>
                @php $number++; @endphp
            @endforeach
            <div class="demo__highlighter">
                <div class="demo__elems">
                    @foreach($question->answers as $answer)
                        <div class="demo__elem">
                            {{ $answer->content }}
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

    </div>
</div>
<script>
    $(".answer").on('click', function() {
        var url = $(this).data('url');
        $.ajax({
            type: 'get',
            url: url,
            success: function (data) {
                $(".question_content").html(data.html);
            }
        });
    })
</script>