@extends('layouts.app')

@section('title', "Quiz")

@section('canonical-link', route('quiz_main'))

@section('content')
    <div class="container">
        <div class="row question_content">
            @include('quiz.question_content', ['question' => $question, 'step' => $step])
        </div>
    </div>
@endsection