@extends('layouts.app')

@section('title', '404')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <br>
                <br>
                <h2 class="text-center">@lang('errors.404')</h2>
                <br>
                <br>
            </div>
        </div>
    </div>
@endsection