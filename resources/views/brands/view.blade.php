@extends('layouts.app')

@section('title', trans('brands.title', ['brand' => $brand->name]))

@section('canonical-link', route('brand_view', ['brand' => $brand->url_name]))

@section('breadcrumbs')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{ route('home') }}">@lang('header.home')</a></li>
                    <li><a href="{{ route('brand_main') }}">@lang('header.brand')</a></li>
                    <li class="active text-capitalize"><strong>{{ $brand->name }}</strong></li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center">
                <h4>@lang('brands.phrase.view', ['number' => $products->total()])</h4><br>
            </div>
            @include('products.product_list_with_sizes', ['products' => $products])
        </div>
    </div>
@endsection