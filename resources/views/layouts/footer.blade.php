<footer class=" text-center">
    <div class="container">
        <div class="col-md-12 row">
            @foreach($posts as $post)
                <div class="col-md-4">
                    <a class="link-unstyled" href="/blog/{{ $post->post_name }}">
                        <h5>{{ $post->post_title }}</h5>
                    </a>
                        <div class="col-md-12">
                            @if(count($post->attachment))
                                <img class="img img-responsive center-block" src="{{ $post->attachment->first()->guid }}">
                            @else
                                <img class="thumbnail img-responsive center-block"
                                    src="{{ asset('images/no-image.jpeg') }}">
                            @endif
                        </div>
                        <div class="col-md-12 text-left">
                            {{ $post->post_short_content }}
                            <a class="link-unstyled" href="/blog/{{ $post->post_name }}">
                                <span class="btn btn-default">Read more</span>
                            </a>
                        </div>

                </div>
            @endforeach
            <br>
        </div>
    </div>
    <div class="col-md-12 page-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 footer-nav-header">
                    <section class="footer__nav-corporate text-left">
                        <h6 class="footer-h6 ">Top Pages</h6>
                        <ul>
                            <li class="footer-nav-list footer-nav-list--1">
                                <a href="{{ route('about_view') }}">About us</a>
                            </li>
                            <li class="footer-nav-list footer-nav-list--2">
                                <a href="#">FAQs</a>
                            </li>
                            <li class="footer-nav-list footer-nav-list--3">
                                <a href="/blog/">Resources</a>
                            </li>
                            <li class="footer-nav-list footer-nav-list--4">
                                <a href="{{ route('most_popular') }}">Most Popular (all)</a>
                            </li>
                            <li class="footer-nav-list footer-nav-list--5 indented">
                                <a href="{{ route('key_product_view', ['keyPhone' => 'galaxy']) }}">Samsung</a>
                            </li>
                            <li class="footer-nav-list footer-nav-list--6 indented">
                                <a href="{{ route('key_product_view', ['keyPhone' => 'iphone']) }}">iPhone</a>
                            </li>
                        </ul>
                    </section>
                </div>
                <div class="col-md-3 footer-nav-header">
                    <section class="footer__nav-corporate text-left">
                        <h6 class="footer-h6 ">Top Recyclers</h6>
                        <ul>
                            <li class="footer-nav-list footer-nav-list--1">
                                <a href="https://www.sellmyphone.org.uk/recyclers/mobile_cash_mate">Mobile Cash Mate</a>
                            </li>
                            <li class="footer-nav-list footer-nav-list--2">
                                <a href="https://www.sellmyphone.org.uk/recyclers/envirofone">Envirofone</a>
                            </li>
                            <li class="footer-nav-list footer-nav-list--3">
                                <a href="https://www.sellmyphone.org.uk/recyclers/music_magpie">Music Magpie</a>
                            </li>
                            <li class="footer-nav-list footer-nav-list--4">
                                <a href="https://www.sellmyphone.org.uk/recyclers/gecko_mobile">Gecko Recycling</a>
                            </li>
                        </ul>
                    </section>
                </div>
                <div class="col-md-3 footer-nav-header">
                    <section class="text-left">
                        <h6 class="footer-h6 ">Services</h6>
                        <ul>
                            <li class="footer-nav-list footer-nav-list--1">
                                <a href="/contact">Contact us</a>
                            </li>
                            <li class="footer-nav-list footer-nav-list--2">
                                <a href="/terms-of-use">Terms of Use</a>
                            </li>
                            <li class="footer-nav-list footer-nav-list--3">
                                <a href="/cookies-privacy">Cookies &amp; Privacy</a>
                            </li>
                            <li class="footer-nav-list footer-nav-list--4">
                                <a href="#">Press</a>
                            </li>
                        </ul>
                    </section>
                </div>


                <div class="col-md-3 text-left" id="copyrights">

                    <ul class="footer-social">
                        <li class="footer-nav-list--1">
                            <a href="https://www.facebook.com/sellmyphoneorguk" target="_blank"
                               rel="noopener noreferrer nofollow" class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="52" height="52"><title>facebook
                                        icon</title>
                                    <path fill="none" d="M-5-1h24v24H-5z"/>
                                    <path class="nav-social__icon" fill="#d0d0cb"
                                          d="M10.36 12.1h-2.9v9.03H3.18v-9.06l-2.27-.02v-3.8h2.24V4.36c0-1.8 1.97-3.5 3.3-3.5h3.88V4.7H8.28c-.6 0-.8.5-.8.9v2.75h3.6l-.72 3.76z"/>
                                </svg>
                            </a>
                        </li>
                        <li class="footer-nav-list--2">
                            <a href="https://twitter.com/sellmyphoneorguk" target="_blank"
                               rel="noopener noreferrer nofollow" class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="52" height="52"><title>twitter
                                        icon</title>
                                    <path fill="none" d="M-2-4h24v24H-2z"/>
                                    <path class="nav-social__icon" fill="#d0d0cb"
                                          d="M18.06 4.2v.48c0 5.07-4.17 10.92-11.82 10.92-2.35 0-4.53-.64-6.37-1.72.32.03.65.05 1 .05 1.94 0 3.73-.6 5.15-1.64-1.82-.04-3.35-1.15-3.88-2.68.25.05.5.07.78.07.38 0 .75-.06 1.1-.14C2.12 9.2.68 7.66.68 5.8v-.06c.56.3 1.2.46 1.9.48C1.44 5.54.7 4.36.7 3.02c0-.7.2-1.35.56-1.92 2.05 2.32 5.1 3.85 8.57 4-.07-.27-.1-.56-.1-.86C9.74 2.12 11.6.4 13.9.4c1.2 0 2.28.47 3.03 1.2.95-.16 1.84-.48 2.64-.92-.3.9-.97 1.65-1.82 2.12.84-.1 1.64-.3 2.38-.6-.55.77-1.26 1.44-2.07 1.98z"/>
                                </svg>
                            </a>
                        </li>
                        <li class="footer-nav-list--3">
                            <a href="https://www.youtube.com/user/sellmyphoneorguk" target="_blank"
                               rel="noopener noreferrer nofollow" class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="52" height="52"><title>youtube
                                        icon</title>
                                    <path fill="none" d="M-2-5h24v24H-2z"/>
                                    <path class="nav-social__icon" fill="#d0d0cb"
                                          d="M20.27 6.97c0 2.95-.3 4.65-.86 5.62-.55.96-1.15 1.15-2.36 1.22-1.2.08-4.26.1-6.9.1s-5.7-.02-6.9-.1C2 13.75 1.4 13.56.85 12.6S0 9.9 0 6.96C0 4 .3 2.3.86 1.33 1.42.37 2 .18 3.23.1 4.43.05 7.5 0 10.13 0s5.7.04 6.9.1c1.22.1 1.82.27 2.38 1.24s.87 2.67.87 5.62zM7.6 10.13l6.33-3.16L7.6 3.8v6.33z"/>
                                </svg>
                            </a>
                        </li>
                        <li class="footer-nav-list--4">
                            <a href="https://plus.google.com/1085635656349395/posts" target="_blank"
                               rel="noopener noreferrer nofollow" class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="52" height="52"><title>google+
                                        icon</title>
                                    <path fill="none" d="M0-4h24v24H0z"/>
                                    <path class="nav-social__icon" fill="#d0d0cb"
                                          d="M7.66 15.46c-4.2 0-7.62-3.42-7.62-7.62S3.46.22 7.66.22c1.98 0 3.78.75 5.13 2l-2.25 2.23C9.8 3.72 8.8 3.28 7.58 3.28c-2.43 0-4.4 2.04-4.4 4.56s1.97 4.56 4.4 4.56c2.13 0 3.64-1.04 4.13-3.03H7.67V6.3h7.15c.1.5.16 1 .16 1.54 0 4.6-3.1 7.62-7.3 7.62zM23.98 8.4h-2.64v2.64H19.4V8.4h-2.6V6.5h2.63V3.84h1.94v2.64h2.64V8.4z"/>
                                </svg>
                            </a>
                        </li>

                    </ul>

                    <!-- temp adding ssl  seal?
                                  <a href="https://ssl.comodo.com/ev-ssl-certificates.php" target="_blank" rel="nofollow">
                                  <img src="https://ssl.comodo.com/images/comodo_secure_seal_76x26_transp.png" alt="EV SSL Certificate" class="checkmend-logo"></a>-->

                    <a href="https://www.checkmend.com/uk/recycle" target="_blank"
                       title="Our providers use CheckMEND to detect stolen devices">
                        <img src="/images/checkmend-logo.png" alt="CheckMEND" class="checkmend-logo"></a><br/>
                    <br/>


                    <p>This website uses "cookies" to give you the best experience and to make it function correctly. To
                        learn more about cookies and their benefits, please read our Cookie policy. Your usage of this
                        website consents to your acceptance of our cookie policies.</p>

                    <p>Copyright &copy; <?php echo date("Y"); ?> Sell My Phone, a trading name of <a
                                href="https://ecompare.ltd" target="_blank">ecompare Ltd</a>.</p>

                    <p>Some patents pending.<br/><br/>

                        Registered Office: <br/>
                        Platt Barn, Bullen Farm<br/>
                        Tonbridge<br/>
                        TN12 5LX<br/>
                        United Kingdom<br/>
                        Registered Number: 11155291</p>
                </div>


            </div>

        </div>


    </div>
</footer>


<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(101102225); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="https//in.getclicky.com/101102225ns.gif" /></p></noscript>