<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('home') }}">
                <img src="/images/logo-standard.png"  class="website-header__logo__img--large" alt="Sell My Phone logo" />

            </a>
        </div>
        <div class="collapse navbar-collapse text-capitalize" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ route('about_view') }}">@lang('header.works')</a></li>
                <li><a href="{{ route('most_popular') }}">@lang('header.most.popular')</a></li>
                <li style="text-transform:none;"><a href="{{ route('key_product_view', ['keyPhone' => 'iphone']) }}">iPhone</a></li>
                <li><a href="{{ route('key_product_view', ['keyPhone' => 'galaxy']) }}">Samsung Galaxy</a></li>
             <!--    <li><a href="{{ route('best_prices_view') }}">@lang('header.prices')</a></li>-->
                <li><a href="{{ route('recyclers_list') }}">@lang('header.partners')</a></li>
                <li><a href="{{ route('brand_main') }}">@lang('header.brand')</a></li>
                {{--
                <li class="dropdown text-uppercase">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-flag"></i> {{ Config::get('languages')[App::getLocale()] }}
                    </a>
                    <ul class="dropdown-menu">
                        @foreach (Config::get('languages') as $lang => $language)
                    @if ($lang != App::getLocale())
                    <li>
                        <a href="{{ route('lang_switch', $lang) }}">{{$language}}</a>
                                        </li>
                                    @endif
                    @endforeach
                    </ul>
                </li>
                --}}
            </ul>
        </div>
    </div>
</nav>