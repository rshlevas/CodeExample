@yield('breadcrumbs')

<div class="jumbotron text-center">
    @if(Route::current() && Route::current()->getName() == 'home')
        <h1 id="home-lg">Compare phone prices &amp; get paid cash today</h1>
    @endif

    <form class="form-inline" method="POST" action="{{ route('search') }}">
        {{ csrf_field() }}
        <div class="input-group">
            <input id="search" type="text" class="form-control input-lg" name="search" size="50"
                   placeholder="@lang('header.search.placeholder')" required>
            @if (isset($errors) && $errors->has('search'))
                <span class="help-block">
                            <strong>{{ $errors->first('search') }}</strong>
                        </span>
            @endif

            <div class="input-group-btn">
                <button type="submit" class="btn btn-info btn-lg text-uppercase">@lang('buttons.search')</button>
            </div>
        </div>
    </form>
    <div class="text-center" style="font-size:small;"> Use the search box to find your device or <a href="{{ route('brand_main') }}"
        title="Phone brands">browse by manufacturer</a>.
    </div>
</div>
