<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<link rel="preconnect" href="https://ecompare-couponize.netdna-ssl.com">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>
    <meta name="description" content="@yield('meta-desc')">
    <meta name="trustpilot-one-time-domain-verification-id" content="uDQtqFFG7Eaf9qMpVbXH25Ge7oC0Gk2fb7o2v3G9"/>
    <link rel="canonical" href="@yield('canonical-link')"/>
    <link href="{{ asset('images/apple-touch-icon-1.png') }}" rel="apple-touch-icon-precomposed" sizes="114x114">
	<link href="{{ asset('images/apple-touch-icon-2.png') }}" rel="apple-touch-icon-precomposed" sizes="72x72">
	<link href="{{ asset('images/apple-touch-icon-precomposed.png') }}" rel="apple-touch-icon-precomposed">
	<link href="{{ asset('images/favicon.png') }}" rel="shortcut icon">
	<meta content="{{ asset('images/touch-icon-144x144.png') }}" name="msapplication-TileImage">
	<meta content="#40364D" name="msapplication-TileImage">
	<meta name="theme-color" content="#E9EFF1" />

    <meta property="og:locale" content="en_GB" />
    <meta property="og:title" content="@yield('title')" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="{{ asset('images/touch-icon-144x144.png') }}">
    <meta property="og:site_name" />
    <meta property="og:url" content="@yield('canonical-link')">
    <meta property="og:description" content="@yield('meta-desc')">

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="@yield('title')" />
    <meta name="twitter:description" content="@yield('meta-desc')" />
    <meta name="twitter:image" content="{{ asset('images/touch-icon-144x144.png') }}" />
    <meta name="twitter:url" content="@yield('canonical-link')" />
    
    <link href="/images/apple-touch-icon-1.png" rel="apple-touch-icon-precomposed" sizes="114x114">
	<link href="/images/apple-touch-icon-2.png" rel="apple-touch-icon-precomposed" sizes="72x72">
	<link href="/images/apple-touch-icon-precomposed.png" rel="apple-touch-icon-precomposed">
	<link href="/images/favicon.png" rel="shortcut icon">
	<meta content="/images/touch-icon-144x144.png" name="msapplication-TileImage">
	<meta content="#40364D" name="msapplication-TileImage">
	<meta name="theme-color" content="#E9EFF1" />

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    <div id="app">
        @include('layouts.header')

        @yield('content')
    </div>
    @include('layouts.footer')
    
    
    <!-- Scripts -->

    <script>
      /*$('#search').autocomplete({
            source: function(request, response){
                $.ajax({
                    url: "",
                    type: "POST",
                    dataType: "json",
                    data:{ search: request.term },
                    success: function(data){
                        response($.map(data, function(item){
                            return item;
                        }));
                    }
                });
            },
            minLength: 2
        });*/
        $(document).ready(function(){
            $("#search").autocomplete({
                minLength: 3,
                source: function(request, response){
                    $.ajax({
                        url: "{{ route('search_autocomplete') }}",
                        type: "POST",
                        dataType: "json",
                        data:{ search: request.term },
                        success: function(data){
                            response($.map(data, function(item){
                                return item;
                            }));
                        }
                    });
                },
                focus: function( event, ui ) {
                    //$( "#search" ).val( ui.item.name );
                    return false;
                },
                select: function( event, ui ) {
                    window.location.href = ui.item.url;
                }
            }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                var inner_html;
                if (item.message) {
                    inner_html = '<div class="col-md-8 col-sm-offset-2 small">' +
                        item.message + '</div>';
                } else {
                    inner_html = '<a class="link-unstyled" href="' + item.url +
                        '" title="' + item.title + '">' +
                        '<div class="col-md-3"><img style="height: 60px;"' +
                        ' class="img-responsive center-block" src="' + item.image +
                        '" ></div><div class="col-md-2"><h4 class="text-center small"><b>' +
                        '&pound; ' + item.price +
                        '</b></h4></div><div class="col-md-6 text-center small">' +
                        item.name + '<br><u class="small">' + item.compare +
                        '</u></div></a>';
                }
                return $( "<li class=\"ui-menu-item\"></li>" )
                    .data( "item.autocomplete", item )
                    .append(inner_html)
                    .appendTo( ul );
            };
        });
    </script>
    <script>
        function sortTable(n) {
            var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
            table = document.getElementById("productTable");
            switching = true;
            dir = "asc";
            while (switching) {
                switching = false;
                rows = table.getElementsByTagName("TR");
                for (i = 1; i < (rows.length - 1); i++) {
                    shouldSwitch = false;
                    x = rows[i].getElementsByTagName("TD")[n];
                    y = rows[i + 1].getElementsByTagName("TD")[n];
                    if (dir == "asc") {
                        if (parseFloat(x.getAttribute('data-number')) > parseFloat(y.getAttribute('data-number'))) {
                            //if so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }
                    } else if (dir == "desc") {
                        if (parseFloat(x.getAttribute('data-number')) < parseFloat(y.getAttribute('data-number'))) {
                            //if so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }
                    }
                }
                if (shouldSwitch) {
                    rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                    switching = true;
                    switchcount ++;
                } else {
                    if (switchcount == 0 && dir == "asc") {
                        dir = "desc";
                        switching = true;
                    }
                }
            }
        }
    </script>

</body>
</html>
