@foreach($products as $name => $productGroup)
    <div class="col-lg-4 col-sm-4 col-xs-6">
        <div class="panel panel-primary" style="height: 360px;">
            <div class="panel-heading">{{ $productGroup[0]->brand->name }}</div>
            <div class="panel-body">
                @if ($productGroup[0]->image)
                    <img style="height: 150px;" class="thumbnail img-responsive center-block"
                         src="{{ $productGroup[0]->image->source }}">
                @elseif ($productGroup[0]->imageThumbnail)
                    <img style="height: 150px;" class="thumbnail img-responsive center-block"
                         src="{{ $productGroup[0]->imageThumbnail->source }}">
                @else
                    <img style="height: 150px;" class="thumbnail img-responsive center-block"
                         src="{{ asset('images/no-image.jpeg') }}">
                @endif
                <div class="text-center"><strong>{{ $name }}</strong></div>
                <div class="text-center">
                    @if(count($productGroup) === 1)
                        <a class="btn btn-primary" href="{{ route('product_view', [
                            'brand' => $productGroup[0]->brand->url_name,
                            'category' => $productGroup[0]->category->url_name,
                            'product' => $productGroup[0]->url_name])
                        }}"
                            title="{{ $name }}">
                            Get prices
                        </a>
                    @elseif(count($productGroup) > 1)
                        @foreach($productGroup as $product)
                        <a class="btn btn-primary" href="{{ route('product_view', [
                            'brand' => $product->brand->url_name,
                            'category' => $product->category->url_name,
                            'product' => $product->url_name])
                        }}"
                           title="{{ $product->name }}">
                            {{ $product->size }}
                        </a>
                        @endforeach
                    @endif
                </div>
            </div>

        </div>
    </div>

@endforeach
<div class="col-md-10 col-md-offset-1 text-center">
    <h4>{{ $products->links() }}</h4><br>
</div>