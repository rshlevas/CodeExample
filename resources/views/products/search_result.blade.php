@extends('layouts.app')

@section('title', trans('products.search.result'))

@section('breadcrumbs')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{ route('home') }}">@lang('header.home')</a></li>
                    <li class="active text-capitalize"><strong>@lang('products.search.result')</strong></li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            @if (count($products))
                <div class="col-md-10 col-md-offset-1 text-center">
                    <h4>@lang('products.search.phrase', ['number' => $products->total()])</h4><br>
                </div>
                @include('products.product_list', ['products' => $products])
            @else
                <div class="col-md-8 col-md-offset-2">
                    <h4>@lang('products.search.failed')</h4>
                </div>
            @endif
        </div>
    </div>
@endsection