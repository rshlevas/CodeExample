@extends('layouts.app')

@section('title', trans('products.title', [
        'product' => $product->name, 'price' => round($product->best_price->price, 0, PHP_ROUND_HALF_DOWN)
    ]) . ', ⇒ Compare Recycler Prices')

@section('canonical-link', route('product_view', [
        'brand' => $product->brand->url_name,
        'category' => $product->category->url_name,
        'product' => $product->url_name
    ]))

@section('meta-desc', trans('products.meta.desc', [
        'bestPrice' => round($product->best_price->price, 0, PHP_ROUND_HALF_DOWN),
        'phone' => $product->name
    ]))

@section('breadcrumbs')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{ route('home') }}">@lang('header.home')</a></li>
                    <li>
                        <a href="{{ route('brand_main') }}">Phones</a>
                    </li>
                    <li>
                        <a href="{{ route('brand_view', ['brand' => $product->brand->url_name]) }}">{{ $product->brand->name }}</a>
                    </li>
                    <li class="active text-capitalize"><strong>{{ $product->name }}</strong></li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <h1 style="margin-left:20px;">Sell your mobile: {{ $product->name }}</h1>
            <div class="pull-left" id="product_info">
                <div class="col-md-3">
                    @include('products.components.general_info', ['product' => $product])
                </div>
                <div class="no-row">
                    @include('products.components.filters', ['product' => $product, 'filters' => $filters])
                    <br>
                    <br>
                    @if (count($sizes) > 1)
                        @include('products.components.sizes', ['product' => $product, 'sizes' => $sizes])
                    @endif
                </div>

                @if($bestPrice)
                    @include('products.components.best_price', ['price' => $bestPrice, 'updateMessage' => $updateMessage])
                @endif


                <br/>
                <br/>
                <br/>
                <br/>
                
              
                <div class="row col-md-9 pull-right" id="prices">
                    @include('products.components.product_site_table', [
                        'prices' => $prices,
                        'bestPrice' => $bestPrice,
                        'unknownSites' => $unknownSites,
                        'propositions' => $propositions
                    ])
                </div>
                <div class="row col-md-10 col-md-offset-2">
                    @include('products.components.explanation', ['product' => $product])
                    @if (count($relatedProducts))
                        @include('products.components.related', ['products' => $relatedProducts])
                    @endif
                </div>
            </div>

        </div>


    </div>

    <script>
        $('.selectpicker').selectpicker();

        $('.filters').change(function () {
            var url = window.location.href + '/filters';
            var condition = $('#condition').val();
            var network = $('#network').val();

            $.setParamsToUrl = function (network, condition) {
                var params = {};
                params['condition'] = condition;
                params['network'] = network;

                return $.param(params);
            };
            var str = $.setParamsToUrl(network, condition);
            if (str) {
                var bestPriceEndPoint = url + '/best-price?' + str;
                var pricesEndPoint = url + '/prices?' + str;
            } else {
                return false;
            }

            $.ajax({
                type: 'get',
                url: bestPriceEndPoint,
                success: function (data) {
                    $("#best_price").html(data.html);
                }
            });
            $.ajax({
                type: 'get',
                url: pricesEndPoint,
                success: function (data) {
                    $("#prices").html(data.html);
                }
            });
            return false;
        })
    </script>
    <script type="application/ld+json">
            {
                "@context": "http://schema.org",
                "@type": "Product",
                "name": "{{ $product->brand->name }} {{ $product->model }}",
                "offers": [{
                    "@type": "AggregateOffer",
                    "highPrice": "{{ $bestPrice->price }}",
                    "lowPrice": "2.50",
                    "offerCount": "5",
                    "priceCurrency": "GBP"
                }]
            }

    </script>
@endsection
