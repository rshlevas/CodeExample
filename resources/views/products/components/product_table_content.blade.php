<tr>
    <td class="col-sm-2 text-center">
        <a href="{{ route('recyclers_view', ['site' => $price->productProfile->site->parsing_name]) }}">
            @if ($price->productProfile->site->image)
                <img style="max-height: 90px;"
                     src="{{$price->productProfile->site->image->source}}"
                     class="img-site-resp">
            @else
                <img style="max-height: 120px;" src="{{ asset('images/no-image.jpeg') }}"
                     class="img img-thumbnail">
            @endif
        </a>
    </td>
    @if ($price->productProfile->site->trust_point and $price->productProfile->site->trust_point != 0.00)
    <td class="col-sm-1 text-center"
            style="vertical-align: middle;" data-number="{{ $price->productProfile->site->stars_number }}">
            <a href="{{ $price->productProfile->site->trustpilot_link }}" target="_blank">
                <img src="{{ asset("images/{$price->productProfile->site->stars_number}-stars.png") }}"
                     class="img img-thumbnail img-responsive">
            </a>
            <!-- <a href="{{ $price->productProfile->site->trustpilot_link }}" target="_blank"><p class="small">Trustpilot <i class="icon-li icon-external-link"></i></p></a> -->
    </td>
    @else
    <td class="col-sm-1 text-center"
            style="vertical-align: middle;" data-number="0">
            <i class="icon-fixed-width icon-ban-circle"
               style="color:red"></i> @lang('recyclers.no.rating')
    </td>
    @endif
 
    @if($price->productProfile->site->paymentTerm)
    <td class="col-sm-2 text-center"
            style="vertical-align: middle;" data-number="{{ $price->productProfile->site->paymentTerm->id }}">
        {{ $price->productProfile->site->paymentTerm->name }}</td>
    @else
    <td class="col-sm-2 text-center"
        style="vertical-align: middle;" data-number="0">
            <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
    </td>
    @endif
    <td class="col-sm-2 text-center"
            style="vertical-align: middle;">
        @if( count($price->productProfile->site->payments) > 0)
            @foreach($price->productProfile->site->payments as $payment)
                {{ $payment->name }}<br>
            @endforeach
        @else
            <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
        @endif
    </td>
    <td class="col-sm-1 text-center"
            style="vertical-align: middle;" data-number="{{ $price->productProfile->site->is_returnable }}">
        @if ($price->productProfile->site->is_returnable == 1)
            <i class="icon-li icon-ok" style="color:#06A09D;"></i>
        @else
            <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
        @endif
    </td>
    <td class="col-sm-1 text-center"
        style="vertical-align: middle;" data-number="{{ $price->productProfile->site->is_free_post }}">
        @if ($price->productProfile->site->is_free_post == 1)
            <i class="icon-li icon-ok" style="color:#06A09D;"></i>
        @else
            <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
        @endif
    </td>
    <td class="col-sm-2 text-center"
            style="vertical-align: middle;" data-number="{{ $price->price }}">
        <span class="price-table">
            &pound;{{ $price->price }}
        </span>
    </td>
    <td class="col-sm-2 text-left fix-right"
            style="vertical-align: middle;"><a class="btn btn-primary" rel="nofollow" target="_blank"
        href="{{ route('sell_link', ['link' => $price->productProfile->link->id]) }}">@lang('buttons.sell')</a>
    </td>
</tr>

@if(count($propositions))
    @if(isset($propositions[$price->productProfile->site->id]))
        <tr class="text-right">
            <td colspan="8">{{ $propositions[$price->productProfile->site->id]->first()->proposition->name }}
                <a href="{{ $propositions[$price->productProfile->site->id]->first()->proposition->link }}" target="_blank">
                    Get &pound;{{ round($propositions[$price->productProfile->site->id]->first()->price, 0) }}
                </a>
            </td>
        </tr>
    @endif
@endif
