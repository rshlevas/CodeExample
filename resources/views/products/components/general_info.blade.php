<span style="font-size:smaller;"><a href="/"><i class="icon-refresh icon-spacing"></i> Not your phone?</a></span>

<div class="sidebar">

<div class="module">


    @if ($product->image)
        <img class="img-responsive img-thumbnail main-product-image" src="{{ $product->image->source }}">
    @else
        <img class="img-responsive img-thumbnail main-product-image" src="{{ asset('images/no-image.jpeg') }}">
    @endif
   
    <!-- <span>@lang('brands.brand'): <a
                href="{{ route('brand_view', ['brand' => $product->brand->url_name]) }}">{{ $product->brand->name }}</a>
    </span> -->
    <p>@lang('products.compare', ['product' => $product->name])</p>
</div>

    <a class="btn btn-primary btn-quiz text-center" href="{{ route('quiz_main') }}">Get your phone's condition &rsaquo;</a>

  </div>