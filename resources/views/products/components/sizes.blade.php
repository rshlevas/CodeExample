<div class="form-group">
    @foreach($sizes as $size => $url_name)
        @if($product->size == $size)
            <a class="btn btn-info">{{ $size }}</a>
        @else
            <a href="{{ route('product_view', [
                'brand' => $product->brand->url_name,
                'category' => $product->category->url_name,
                'product' => $url_name
            ]) }}" class="btn btn-success">{{ $size }}</a>
        @endif
    @endforeach
</div>