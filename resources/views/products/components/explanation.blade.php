<section>
    <h2>How do I get the best price when selling my {{ $product->model }}?</h2>

    <ul>
        <li><strong>I want to sell my {{ $product->model }}, how much money can I get?</strong><br />
            Mobile Cash Mate and 5 other recyclers will pay you &pound;{{ $product->best_price->price }} for your used {{ $product->model }}. You can also check if they buy faulty/damaged {{ $product->model }}.</li>


        <li><strong>Can I sell my faulty/damaged {{ $product->model }} for cash?</strong><br />
            YES! Unsure of your phone's condition? Then take our 30 second quiz to find out. Many Mobile Phone Recyclers pay you top prices for your old mobile phone even if if it's damaged. You can simply select faulty/damaged and get paid top prices for your faulty/damaged {{ $product->model }}.</li>

        <li><strong>How many {{ $product->model }}s can I sell in total?</strong><br />
            As many as you want, please contact us for business/bulk enquiries.</li>

        <li><strong>How easy is it to sell my mobile to these phone recyclers?</strong><br />
            It's a very simple process, all you need to do is add your {{ $product->model }} to the basket and complete their checkout process to place an order. Compare unbiased prices for {{ $product->model }} Mobile Phones to check how much does Online Mobile Phone buyers will pay you today. Don't forget that Sell My Phone compares over 1000 prices several times a day for the best accuracy.</li>

        <li><strong>What stores buy used {{ $product->model }} phones for cash?</strong><br />
            Recyclers, like Giffgaff Recycle, O2 Recycle, EE Recycle, Music Magpie offer online trade-ins for {{ $product->model }} depending on condition. If you aren't sure on your phone's condition then take our 30 second quiz. You can get up to £{{ $product->best_price->price }} online if you recycle your {{ $product->model }} today with Mobile Cash Mate as they pay the top price, which indicates they have a large demand for these secondhand, used or even faulty {{ $product->model }} phones.</li>
    </ul>

</section>