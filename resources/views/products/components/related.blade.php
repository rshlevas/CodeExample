<div class="col-md-12">
    <h4 class="text-center">More {{ $product->brand->name }} phones</h4>
    <div class="list-group">
        @foreach($products as $product)
            <a href="{{ route('product_view', [
                    'brand' => $product->brand->url_name,
                    'category' => $product->category->url_name,
                    'product' => $product->url_name])
                }}" class="list-group-item">
                {{ $product->name }}
            </a>
        @endforeach
    </div>
</div>
