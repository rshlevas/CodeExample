@if ($bestPrice and count($prices) > 1)
    <table class="table table-hover text-center">
        <thead class="text-capitalize">
        
        <tr>
            <th class="text-center text-no-style success" colspan="8">
                <strong>
                    @lang('products.top.price.table.header', ['product' => $bestPrice->productProfile->product->name])
                </strong>
            </th>
        </tr>
        </thead>
        <tbody>
            @include('products.components.product_table_content', ['price' => $bestPrice, 'propositions' => $propositions, 'btn' => 'success'])
        </tbody>
    </table>
@endif
<table id="productTable" class="table table-hover text-center">
    <thead class="text-capitalize">
    <tr>
        <th class="col-sm-2 info text-center"></th>
        <th class="col-sm-1 info text-center" onclick="sortTable(1)"><span class="sortable">@lang('products.site.trust')</span>
           
        </th>
        <th class="col-sm-2 info text-center" onclick="sortTable(2)"><span class="sortable">@lang('recyclers.term')</span>
          
        </th>
        <th class="col-sm-2 info text-center">@lang('recyclers.method')</th>
        <th class="col-sm-1 info text-center" onclick="sortTable(4)"><span class="sortable">@lang('recyclers.return')</span>
         
        </th>
        <th class="col-sm-1 info text-center" onclick="sortTable(5)"><span class="sortable">@lang('recyclers.free.post')</span>
          
        </th>
        <th class="col-sm-2 info text-center" onclick="sortTable(6)"><span class="sortable">@lang('products.price')</span>
         
        </th>
        <th class="col-sm-2 info text-center"></th>
        
    </tr>
    </thead>
    <tbody>
    @if (count($prices))
        @foreach($prices as $price)
            @include('products.components.product_table_content', ['price' => $price, 'btn' => 'primary'])
        @endforeach
    @elseif(! count($prices) && $bestPrice)
        @include('products.components.product_table_content', ['price' => $bestPrice, 'propositions' => $propositions, 'btn' => 'primary'])
    @elseif (! count($prices) && ! $bestPrice)
        <tr>
            <td colspan="8"><h4>@lang('products.no-prices')</h4></td>
        </tr>
    @endif
    @if (count($unknownSites))
        @include('products.components.product_table_unknown', ['sites' => $unknownSites])
    @endif
    </tbody>
</table>