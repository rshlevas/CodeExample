@foreach($sites as $site)
    <tr class="no-prices">
        <td>
            @if ($site->image)
                <img style="max-height: 120px;"
                     src="{{ $site->image->source }}"
                     class="img-responsive img-thumbnail">
            @else
                <img style="max-height: 120px;" src="{{ asset('images/no-image.jpeg') }}"
                     class="img img-thumbnail">
            @endif
        </td>
        <td style="vertical-align: middle;">
            @if ($site->trust_point and $site->trust_point != 0.00)
                <a href="{{ $site->trustpilot_link }}">
                    <img src="{{ asset("images/{$site->stars_number}-stars.png") }}"
                         class="img img-thumbnail img-responsive">
                </a>
                <p class="small"><i class="icon-li icon-ok" style="color:green"></i> Trustpilot</p>
            @else
                <i class="icon-fixed-width icon-ban-circle"
                   style="color:red"></i> @lang('recyclers.no.rating')
            @endif
        </td>

        <td style="vertical-align: middle;">
            @if($site->paymentTerm)
                {{ $site->paymentTerm->name }}
            @else
                <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
            @endif
        </td>
        <td style="vertical-align: middle;">
            @if( count($site->payments) > 0)
                @foreach($site->payments as $payment)
                    {{ $payment->name }}<br>
                @endforeach
            @else
                <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
            @endif
        </td>
        <td style="vertical-align: middle;">
            @if ($site->is_returnable == 1)
                <i class="icon-li icon-ok" style="color:green"></i>
            @else
                <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
            @endif
        </td>
        <td style="vertical-align: middle;">
            @if ($site->is_free_post == 1)
                <i class="icon-li icon-ok" style="color:green"></i>
            @else
                <i class="icon-fixed-width icon-ban-circle" style="color:red"></i>
            @endif
        </td>
        <td style="vertical-align: middle;"></td>
        <td style="vertical-align: middle;"><button class="btn btn-primary" disabled>@lang('buttons.check.site')</button>
        </td>
    </tr>
@endforeach