<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Errors
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by errors messages.
    | .
    |
    */
    '404' => 'The page you’re looking for can’t be found',

];