<?php

return [
    'brand' => 'Brand',
    'phrase.index' => 'Listing :number brands',
    'phrase.view' => 'Listing :number phones',
    'products.count' => 'View all :count products',
    'products.count.single' => 'Check the product',
    'title' => 'Compare and sell phones from :brand',
];