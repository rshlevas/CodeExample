<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the different buttons.
    | Such as: search, view more, etc.
    |
    */
    'check.site' => 'No Offer',
    'filter.damaged' => 'Damaged',
    'filter.not.working' => 'Not working',
    'filter.working' => 'Working',
    'search' => 'Compare prices &rsaquo;',
    'sell' => 'Sell&rsaquo;',
    'sell.now' => 'Sell&rsaquo;',
    'view.more' => 'View More',


];