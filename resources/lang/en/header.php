<?php
 return [
     /*
      |--------------------------------------------------------------------------
      | Header Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used by the header.
      |
      */

     'brand' => 'Phones',
     'home' => 'Home',
     'most.popular' => 'Most Popular',
     'partners' => 'Recyclers',
     'prices' => 'Best prices',
     'search.placeholder' => 'Type your phone e.g. iPhone 7 or IMEI code',
     'works' => 'How it works',
 ];