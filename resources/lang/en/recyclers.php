<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Product Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the product view page.
    |
    */
    'about' => 'About :site',
    'free.post' => 'Free P&amp;P?',
    'method' => 'Payment methods',
    'no.rating' => 'No rating',
    'return' => 'Free returns?',
    'term' => 'Payment speed',
    'title.index' => 'We cooperate with...',
    'title.view' => 'Recyclers: About :site',
    'trust' => 'Trustpilot Rating',
    'visit' => 'Visit the :site website.',
    'website' => 'Recycler Website',


];