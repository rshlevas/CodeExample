<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Homepage Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by homepage of the application.
    |
    |
    */
    'best.prices' => 'Best prices',
    'best.prices.phrase' => 'Sell your phone for the best price',
    'brands' => 'Browse by manufacturer',
    'meta.desc' => 'Compare mobile phone prices with Sell My Phone. Get instant quotes from over 25 UK recycling companies, compare offers &amp; get paid cash today.',
    'most.popular' => 'Most popular',
    'most.popular.phrase' => 'Most Popular Traded-In Phones',
    'partners' => 'Comparing prices from...',
    'title' => '⇒ Sell My Phone || Compare prices from UK recyclers FREE',
    'works' => 'How it works',
];