<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Product Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the product view page.
    |
    */
    'click.sell' => 'Click to sell',
    'compare' => 'We compare all the leading mobile phone recyclers and buyers, so you\'ll get the highest price for your :product guaranteed. We do all the hard work so you don\'t have to check each website individually. <br><br>Comparing prices for used, damaged (including screen scratches) and more - if you aren\'t sure on the grading of your phone then click the button below and answer 4 simple questions...',
    'compare.prices.several' => 'Compare :number prices',
    'compare.prices.single' => 'View 1 price',
    'filter.condition' => 'Condition',
    'filter.network' => 'Network',
    'filter.unlocked' => 'Unlocked',
    'meta.desc' => 'Compare 28+ phone recycler quotes &amp; get up to &pound;:bestPrice for your :phone. → ◣ Use Sell My Phone for your mobile &amp; and get paid &pound;:bestPrice cash - completely FREE &amp; unbiased.',
    'model' => 'Model',
    'most-popular.title' => 'Compare and sell phones among the most popular items',
    'no-prices' => 'Sorry, but there is no info about current product',
    'price' => 'Price paid',
    'price.unavailable' => 'Price is unavailable now. Check site for more info',
    'search.failed' => 'We couldn\'t find any phone matching your search',
    'search.imei' => 'IMEI code should contains exactly 15 numbers',
    'search.phrase' => 'We couldn\'t find exactly what you were looking for, but found :number similar phones',
    'search.result' => 'Search result',
    'site' => 'Site',
    'site.trust' => 'Rating',
    'title' => 'Sell :product for up to &pound;:price',
    'top.price' => 'Today\'s Top Price',
    'top.price.table.header' => 'Today\'s best price for :product ...',
];