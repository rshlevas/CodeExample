<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Product Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the product view page.
    |
    */
    'click.sell' => 'Klickenzu verkaufen',
    'compare' => 'Wir vergleichen alle führenden Käufer, so erhalten Sie den garantiert besten Preis für Ihre :product',
    'compare.prices.several' => 'Vergleichen :number Preise',
    'compare.prices.single' => 'Überprüfen den Preis',
    'filter.condition' => 'Bedingung',
    'filter.network' => 'Netzwerk',
    'filter.unlocked' => 'Entsperrt',
    'model' => 'Modell',
    'most-popular.title' => 'Vergleichen und verkaufen Handys zu den beliebtesten Artikeln',
    'no-prices' => 'Entschuldigung, aber es gibt keine Informationen über das aktuelle Produkt',
    'price' => 'Preis',
    'price.unavailable' => 'Der Preis ist momentan nicht verfügbar. Überprüfen Sie die Website für weitere Informationen',
    'search.failed' => 'Wir konnten kein Telefon finden, das Ihrer Suche entspricht',
    'search.imei' => 'IMEI-Code sollte genau 15 Nummern enthalten',
    'search.phrase' => 'Wir konnten nicht genau das finden, was Sie gesucht haben, aber gefunden :number ähnliche Telefone',
    'search.result' => 'Suchergebnis',
    'site' => 'Webseite',
    'site.trust' => 'Rezension',
    'title' => 'Verkauf :product für bis zu :price',
    'top.price' => 'Top-Preis Bezahlt',
    'top.price.table.header' => 'Top Preis Für :product',


];