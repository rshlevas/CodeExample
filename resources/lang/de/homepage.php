<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Homepage Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by homepage of the application.
    |
    |
    */
    'best.prices' => 'Bester Preis',
    'best.prices.phrase' => 'Verkaufen Sie Ihr Handy zum besten Preis',
    'brands' => 'Hier finden Sie Handys der beliebtesten Marken',
    'most.popular' => 'Beliebteste',
    'most.popular.phrase' => 'Meistverkaufte Telefone',
    'partners' => 'Wir kooperieren mit',
    'works' => 'Wie es funktioniert',

];