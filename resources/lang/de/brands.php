<?php

return [
    'brand' => 'Marke',
    'phrase.index' => 'Wir haben :number Marken für Sie',
    'phrase.view' => 'Wir haben :number Telefone dieser Marke',
    'products.count' => 'Alle :count Produkte anzeigen',
    'products.count.single' => 'Überprüfen den Produkt',
    'title' => 'Vergleichen und verkaufen Handys von :brand',
];