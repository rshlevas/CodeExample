<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Product Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the product view page.
    |
    */
    'about' => 'Über  :site',
    'free.post' => 'Kostenloser Beitrag?',
    'method' => 'Zahlungsarten',
    'no.rating' => 'Keine Bewertung verfügbar!',
    'return' => 'Kostenlose Rückkehr?',
    'term' => 'Geschwindigkeit',
    'title.index' => 'Wir kooperieren mit...',
    'title.view' => 'Recycler: Über :site',
    'trust' => 'Trustpilot Bewertung',
    'visit' => 'Besuchen die Website :site.',
    'website' => 'Recycler Webseite',


];