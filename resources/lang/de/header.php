<?php
 return [
     /*
      |--------------------------------------------------------------------------
      | Header Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used by the header.
      |
      */
     'brand' => 'Marken',
     'home' => 'Zuhause',
     'most.popular' => 'Beliebtesten',
     'partners' => 'Partners',
     'prices' => 'Bester Preis',
     'search.placeholder' => 'Gib dein Handy',
     'works' => 'Wie es funktioniert',
 ];