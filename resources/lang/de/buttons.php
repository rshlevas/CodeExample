<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the different buttons.
    | Such as: search, view more, etc.
    |
    */
    'check.site' => 'Überprüfen Website',
    'filter.damaged' => 'Beschädigt',
    'filter.not.working' => 'Nicht Arbeiten',
    'filter.working' => 'Arbeiten',
    'search' => 'Suche',
    'sell' => 'Verkaufen',
    'sell.now' => 'Jetzt Verkaufen',
    'view.more' => 'Mehr Sehen',

];