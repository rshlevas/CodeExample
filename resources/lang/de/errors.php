<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Errors
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by errors messages.
    | .
    |
    */

    '404' => 'Die von Ihnen gesuchte Seite kann nicht gefunden werden',

];