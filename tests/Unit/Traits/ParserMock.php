<?php

namespace Tests\Unit\Traits;

use App\Models\Site;
use App\Parsers\Services\ModelService;

trait ParserMock
{
    /**
     * @param $className
     * @return mixed
     */
    protected function getParser($className)
    {
        return new $className($this->getSite(), $this->getModelService());
    }

    /**
     * @return mixed
     */
    protected function getSite()
    {
        return factory(Site::class)->make(['id' => 15]);
    }

    /**
     * @return mixed
     */
    protected function getModelService()
    {
        return $this->getMockBuilder(ModelService::class)
            ->disableOriginalConstructor()
            ->getMock();
    }
}