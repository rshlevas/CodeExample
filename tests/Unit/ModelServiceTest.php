<?php

namespace Tests\Unit;

use App\Models\Brand;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\ProductProfile;
use App\Models\SellLink;
use App\Parsers\Services\EntityChecker;
use App\Parsers\Services\EntityCreator;
use App\Parsers\Services\EntityDestroyer;
use App\Parsers\Services\ImageService;
use App\Parsers\Services\ModelService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ModelServiceTest extends TestCase
{
    private $brand;

    private $product;

    private $productProfile;

    private $price;

    private $link;

    public function setUp()
    {
        parent::setUp();
        $this->brand = $this->getBrand();
        $this->product = $this->getProduct();
        $this->productProfile = $this->getProductProfile();
        $this->price = $this->getPrice();
        $this->link = $this->getLink();
    }

    public function tearDown()
    {
        parent::tearDown();
        $this->brand = null;
        $this->product = null;
        $this->productProfile = null;
        $this->price = null;
        $this->link = null;
    }

    /**
     * @param $data
     * @param bool $imagable
     * @dataProvider runTestDataProvider
     */
    public function test_run_dublicates($data, $imagable = false)
    {
        $modelService = $this->getModelService($this->brand, $this->product, $this->productProfile, $this->price);
        $answer = $modelService->run($data, $imagable);

        $this->assertEquals($answer, 'nonChanged', 'Problem in duplicates parsing');
    }

    /**
     * @param $data
     * @param bool $imagable
     * @dataProvider runTestDataProvider
     */
    public function test_run_new_brand_insertion($data, $imagable = false)
    {
        $modelService = $this->getModelService();
        $answer = $modelService->run($data, $imagable);


        $this->assertEquals($answer, 'brand', 'Problem in brand insertion');
    }

    /**
     * @param $data
     * @param bool $imagable
     * @dataProvider runTestDataProvider
     */
    public function test_run_product_insertion($data, $imagable = false)
    {
        $modelService = $this->getModelServiceForProductInsertion();
        $answer = $modelService->run($data, $imagable);

        $this->assertEquals($answer, 'product', 'Problem in product insertion');
    }

    /**
     * @param $data
     * @param bool $imagable
     * @dataProvider runTestDataProvider
     */
    public function test_run_product_profile_insertion($data, $imagable = false)
    {
        $modelService = $this->getModelServiceForProductProfileInsertion();
        $answer = $modelService->run($data, $imagable);

        $this->assertEquals($answer, 'productProfile', 'Problem in product profile insertion');
    }

    /**
     * @param $data
     * @param bool $imagable
     * @dataProvider runTestDataProvider
     */
    public function test_run_price_insertion($data, $imagable = false)
    {
        $modelService = $this->getModelServiceForPriceInsertion();
        $answer = $modelService->run($data, $imagable);

        $this->assertEquals($answer, 'price', 'Problem in price insertion');
    }

    public function runTestDataProvider()
    {
        $data = [
            'brand' => [
                'name' => 'Nokia',
                'url_name' => 'nokia',
            ],
            'product' => [
                'name' => '1100',
                'model' => 'Model',
                'check_name' => '1100',
                'url_name' => '1100',
            ],
            'product_profile' => [
                'site_id' => 1,
            ],
            'link' => [
                'link' => 'link'
            ],
            'price' => [
                0 => [
                    'price' => '10.0',
                    'currency' => 'GBP',
                    'created_at' => '2017-10-01',
                    'condition' => 1,
                    'network' => 1,
                ],
            ],
            'images' => [
               0 => [],
            ]
        ];

        return [
                [$data],
            ];
    }


    protected function getModelService($brand = null, $product = null, $productProfile = null, $price = null)
    {
        return new ModelService(
            $this->getMockEntityChecker($brand, $product, $productProfile, $price),
            $this->getMockEntityCreator($this->brand, $this->product, $this->productProfile, $this->price),
            $this->getMockEntityDestroyer(),
            $this->getMockImageService()
        );
    }

    protected function getMockEntityDestroyer()
    {
        $service = $this->getMockBuilder(EntityDestroyer::class)
            ->disableOriginalConstructor()
            ->getMock();

        $service->expects($this->any())
            ->method('__call')
            ->with($this->equalTo('price')
            )->willReturn(
                true
            );

        return $service;
    }

    protected function getMockEntityChecker($brand = null, $product = null, $productProfile = null, $price = null)
    {
        return $this->getMockEntityService(
            EntityChecker::class,
            $brand,
            $product,
            $productProfile,
            $price
        );
    }

    protected function getMockEntityCreator($brand = null, $product = null, $productProfile = null, $price = null)
    {
        $service = $this->getMockBuilder(EntityCreator::class)
            ->disableOriginalConstructor()
            ->getMock();

        $service->expects($this->any())
            ->method('__call')
            ->withConsecutive(
                [$this->equalTo('brand')],
                [$this->equalTo('product')],
                [$this->equalTo('productProfile')],
                [$this->equalTo('link')],
                [$this->equalTo('price')]
            )->willReturnOnConsecutiveCalls(
                $brand,
                $product,
                $productProfile,
                $this->link,
                $price
            );

        return $service;
    }

    protected function getMockEntityService($className, $brand = null, $product = null, $productProfile = null, $price = null)
    {
        $service = $this->getMockBuilder($className)
            ->disableOriginalConstructor()
            ->getMock();

        $service->expects($this->any())
            ->method('__call')
            ->withConsecutive(
                [$this->equalTo('brand')],
                [$this->equalTo('product')],
                [$this->equalTo('productProfile')],
                [$this->equalTo('price')]
            )->willReturnOnConsecutiveCalls(
                $brand,
                $product,
                $productProfile,
                $price
            );

        return $service;
    }

    protected function getModelServiceForProductInsertion()
    {
        return new ModelService(
            $this->getMockEntityChecker($this->brand),
            $this->getMockEntityCreatorForProduct(),
            $this->getMockEntityDestroyer(),
            $this->getMockImageService()
        );
    }


    protected function getMockEntityCreatorForProduct()
    {
        $service = $this->getMockBuilder(EntityCreator::class)
            ->disableOriginalConstructor()
            ->getMock();

        $service->expects($this->any())
            ->method('__call')
            ->withConsecutive(
                [$this->equalTo('product')],
                [$this->equalTo('productProfile')],
                [$this->equalTo('link')],
                [$this->equalTo('price')]
            )->willReturnOnConsecutiveCalls(
                $this->product,
                $this->productProfile,
                $this->link,
                $this->price
            );

        return $service;
    }


    protected function getModelServiceForProductProfileInsertion()
    {
        return new ModelService(
            $this->getMockEntityChecker($this->brand, $this->product),
            $this->getMockEntityCreatorForProductProfile(),
            $this->getMockEntityDestroyer(),
            $this->getMockImageService()
        );
    }


    protected function getMockEntityCreatorForProductProfile()
    {
        $service = $this->getMockBuilder(EntityCreator::class)
            ->disableOriginalConstructor()
            ->getMock();

        $service->expects($this->any())
            ->method('__call')
            ->withConsecutive(
                [$this->equalTo('productProfile')],
                [$this->equalTo('link')],
                [$this->equalTo('price')]
            )->willReturnOnConsecutiveCalls(
                $this->productProfile,
                $this->link,
                $this->price
            );

        return $service;
    }

    protected function getModelServiceForPriceInsertion()
    {
        return new ModelService(
            $this->getMockEntityChecker($this->brand, $this->product, $this->productProfile),
            $this->getMockEntityCreatorForPrice(),
            $this->getMockEntityDestroyer(),
            $this->getMockImageService()
        );
    }


    protected function getMockEntityCreatorForPrice()
    {
        $service = $this->getMockBuilder(EntityCreator::class)
            ->disableOriginalConstructor()
            ->getMock();

        $service->expects($this->any())
            ->method('__call')
            ->withConsecutive(
                [$this->equalTo('price')]
            )->willReturnOnConsecutiveCalls(
                $this->price
            );

        return $service;
    }


    protected function getMockImageService()
    {
        $imageService = $this->getMockBuilder(ImageService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $imageService->expects($this->any())
            ->method('create')
            ->willReturn(true);

        return $imageService;
    }

    protected function getBrand()
    {
        $brand = factory(Brand::class)->make([
            'id' => 1,
            'name' => 'Nokia',
            'url_name' => 'nokia'
        ]);

        return $brand;
    }

    protected function getProduct()
    {
        $product = factory(Product::class)->make([
            'name' => '1100',
            'url_name' => '1100',
            'check_name' => '1100',
            'id' => 1,
            'brand_id' => $this->brand->id
        ]);

        return $product;
    }

    protected function getProductProfile()
    {
        $productProfile = factory(ProductProfile::class)->make([
            'product_id' => $this->product->id,
            'id' => 1
        ]);

        return $productProfile;
    }

    protected function getPrice()
    {
        $price = factory(ProductPrice::class)->make([
            'product_profile_id' => $this->productProfile->id,
            'id' => 1
        ]);

        return $price;
    }

    protected function getLink()
    {
        $link = factory(SellLink::class)->make([
            'product_profile_id' => $this->productProfile->id,
            'id' => 1
        ]);

        return $link;
    }
}
