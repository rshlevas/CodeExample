<?php

namespace Tests\Unit;

use App\Models\Brand;
use App\Parsers\Exceptions\EntityServiceException;
use App\Parsers\Services\EntityCreator;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EntityCreatorTest extends TestCase
{
    private $creator;

    private $params = [
        'brand' => Brand::class
    ];

    public function setUp()
    {
        parent::setUp();
        $this->creator = new EntityCreator();
    }

    public function tearDown()
    {
        parent::tearDown();
        $this->creator = null;
    }

    /**
     * Test the exceptions
     *
     * @dataProvider exceptionDataProvider
     * @return void
     */
    public function test_expect_exceptions($params = null, $arg1 = null, $arg2 = null)
    {
        $this->expectException(EntityServiceException::class);
        $this->creator->brand();
    }


    public function exceptionDataProvider()
    {
        return [
            [],
            [$this->params],
            [$this->params, 'foo', 'bar']

        ];
    }

    /**
     * @dataProvider createDataProvider
     */
    public function test_create_function($brandInfo)
    {
        $this->creator->setParams($this->params);
        $this->assertDatabaseMissing('brands', $brandInfo);
        $brand = $this->creator->brand($brandInfo);
        $this->assertEquals($brand->name, $brandInfo['name']);
        $this->assertDatabaseHas('brands', $brandInfo);
        $brand->delete();
    }

    public function createDataProvider()
    {
        $brandInfo = [
            'name' => 'Foo',
            'url_name' => 'foo',
        ];

        return [0 => [$brandInfo]];
    }
}
