<?php

namespace Tests\Unit;

use App\Parsers\Exceptions\ParserException;
use App\Parsers\JSONParser\CustomParsers\CarphoneWarehouseParser;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Unit\Traits\MethodInvoke;
use Tests\Unit\Traits\ParserMock;

class CustomParserTest extends TestCase
{
    use MethodInvoke, ParserMock;

    /**
     * @var CarphoneWarehouseParser
     */
    protected $parser;

    /**
     * Set up
     */
    public function setUp()
    {
        parent::setUp();
        $this->parser = $this->getParser(CarphoneWarehouseParser::class);
    }

    /**
     * Tear down
     */
    public function tearDown()
    {
        $this->parser = null;
        parent::tearDown();
    }

    /**
     * Testing of price preparation
     *
     * @param $date
     * @param $prices
     * @param $results
     * @dataProvider pricePreparationDataProvider
     * @return void
     */

    public function test_price_preparation($date, $prices, $results)
    {
        $this->invokeMethod($this->parser, 'setPriceDate', [$date]);
        $preparedPrices = $this->invokeMethod($this->parser, 'preparePrices', [$prices]);
        $this->assertEquals($results, $preparedPrices);
    }

    /**
     * DataProvider for test_price_preparation
     *
     * @return array
     */
    public function pricePreparationDataProvider()
    {
        $date = '1518420928';

        $pricesFirst = [
            "Working" => [
                "networks" => [
                    0 => [
                        "name" => "Unlocked",
                        "price" => "120.00"
                    ],
                    1 => [
                        "name" => "Vodafone",
                        "price" => "110.00"
                    ],
                ],
            ],
            "New" => [
                "networks" => [
                    0 => [
                        "name" => "Unlocked",
                        "price" => "240.00"
                    ],
                    1 => [
                        "name" => "Vodafone",
                        "price" => "220.00"
                    ],
                ],
            ],
            "Dead" => [
                "price" => "25.00"
            ],
        ];

        $resultFirst = [
            0 => [
                "price" => "120.00",
                "currency" => "GBP",
                "created_at" => $date,
                "condition" => 1,
                "network" => 1,
            ],
            1 => [
                "price" => "110.00",
                "currency" => "GBP",
                "created_at" => $date,
                "condition" => 1,
                "network" => 3
            ],
            2 => [
                "price" => "240.00",
                "currency" => "GBP",
                "created_at" => $date,
                "condition" => 4,
                "network" => 1,
            ],
            3 => [
                "price" => "220.00",
                "currency" => "GBP",
                "created_at" => $date,
                "condition" => 4,
                "network" => 3
            ],
            4 => [
                "price" => "25.00",
                "currency" => "GBP",
                "created_at" => $date,
                "condition" => 2,
                "network" => 1,
            ]
        ];

        $pricesSecond = [
            "Working" => [
                "networks" => [
                    0 => [
                        "name" => "Unlocked",
                        "price" => "120.00"
                    ],
                    1 => [
                        "name" => "Vodafone",
                        "price" => "110.00"
                    ],
                ],
            ],
            "Dead" => [
                "price" => "25.00"
            ],
            "New" => [
                "networks" => [
                    0 => [
                        "name" => "Unlocked",
                        "price" => "240.00"
                    ],
                    1 => [
                        "name" => "Vodafone",
                        "price" => "220.00"
                    ],
                ],
            ],
        ];

        $resultSecond = [
            0 => [
                "price" => "120.00",
                "currency" => "GBP",
                "created_at" => $date,
                "condition" => 1,
                "network" => 1,
            ],
            1 => [
                "price" => "110.00",
                "currency" => "GBP",
                "created_at" => $date,
                "condition" => 1,
                "network" => 3
            ],
            2 => [
                "price" => "25.00",
                "currency" => "GBP",
                "created_at" => $date,
                "condition" => 2,
                "network" => 1,
            ],
            3 => [
                "price" => "240.00",
                "currency" => "GBP",
                "created_at" => $date,
                "condition" => 4,
                "network" => 1,
            ],
            4 => [
                "price" => "220.00",
                "currency" => "GBP",
                "created_at" => $date,
                "condition" => 4,
                "network" => 3
            ],
        ];


        return [
            [$date, $pricesFirst, $resultFirst],
            [$date, $pricesSecond, $resultSecond]
        ];
    }

    /**
     * Testing of price preparation
     *
     * @param $date
     * @param $price
     * @param $result
     * @dataProvider singlePricePreparationDataProvider
     * @return void
     */
    public function test_single_price_preparation($date, $price, $result)
    {
        $this->invokeMethod($this->parser, 'setPriceDate', [$date]);
        $preparedPrice = $this->invokeMethod(
            $this->parser,
            'prepareSinglePrice',
            [$price, $price['condition']]);
        $this->assertEquals($result, $preparedPrice);
    }

    /**
     * Data provider for test_single_price_preparation
     *
     * @return array
     */
    public function singlePricePreparationDataProvider()
    {
        $date = '1518420928';

        $price = [
            "condition" => 'Working',
            "name" => "Unlocked",
            "price" => "120.00"
        ];

        $result = [
            "price" => "120.00",
            "currency" => "GBP",
            "created_at" => $date,
            "condition" => 1,
            "network" => 1,
        ];

        return [
            [$date, $price, $result],
        ];
    }

    /**
     * Test if custom parser throws exception if not proper filters were given
     *
     * @param $date
     * @param $price
     * @dataProvider singlePriceExceptionDataProvider
     * @return void
     */
    public function test_single_price_exception($date, $price)
    {
        $this->invokeMethod($this->parser, 'setPriceDate', [$date]);
        $this->expectException(ParserException::class);
        $preparedPrice = $this->invokeMethod(
            $this->parser,
            'prepareSinglePrice',
            [$price, $price['condition']]);

    }

    /**
     * DataProvider for test_single_price_exception
     *
     * @return array
     */
    public function singlePriceExceptionDataProvider()
    {
        $date = '1518420928';

        return [
            0 => [
                $date,
                [
                    "condition" => 'Working',
                    "name" => "FakeNetwork",
                    "price" => "120.00"
                ]
            ],
            1 => [
                $date,
                [
                    "condition" => 'FakeCondition',
                    "name" => "Unlocked",
                    "price" => "120.00"
                ]
            ],
        ];
    }

    /**
     * Test if getFilterValue function throws exception
     *
     * @param $type
     * @param $name
     * @dataProvider getFilterValueExceptionDataProvider
     * @return void
     */
    public function test_get_filter_value_function_exception($type, $name)
    {
        $this->expectException(ParserException::class);
        $this->invokeMethod($this->parser, 'getPriceFilterValue', [$name, $type]);
    }

    /**
     * DataProvider for test_get_filter_value_function_exception
     *
     * @return array
     */
    public function getFilterValueExceptionDataProvider()
    {
        return [
            ['condition', 'FakeCondition'],
            ['network', 'FakeNetwork']
        ];
    }

    /**
     * Checks that prepareBrandName function remove iphone from brand name
     *
     * @param $name
     * @param $result
     * @dataProvider prepareBrandNameDataProvider
     * @return void
     */
    public function test_prepare_brand_name($name, $result)
    {
        $preparedName = $this->invokeMethod($this->parser, 'prepareBrandName', [$name]);
        $this->assertEquals($result, $preparedName);
    }

    /**
     * DataProvider for test_prepare_brand_name
     *
     * @return array
     */
    public function prepareBrandNameDataProvider()
    {
        return [
            ['Iphone', 'Apple'],
            ['Nokia', 'Nokia'],
            ['Samsung', 'Samsung'],
            ['iphone', 'Apple'],
            ['iphONE', 'Apple']
        ];
    }
}
