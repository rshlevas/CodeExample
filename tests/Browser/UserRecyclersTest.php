<?php

namespace Tests\Browser;

use App\Models\Site;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserRecyclersTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testRecyclersSectionPages()
    {
        $sites = Site::all();

        $this->browse(function (Browser $browser) use ($sites) {
            $browser->visit('/recyclers')
                    ->assertSee('Partners');

            foreach ($sites as $site) {
                $this->shouldSeeSite($browser, $site);
            }
        });
    }

    /**
     * @param Browser $browser
     * @param Site $site
     * @return Browser
     */
    protected function shouldSeeSite(Browser $browser, Site $site)
    {
        $browser->assertSee($site->name)
            ->visit("/recyclers/{$site->parsing_name}")
            ->assertSee($site->name)
            ->visit("/recyclers");

        return $browser;
    }
}
