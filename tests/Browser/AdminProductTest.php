<?php

namespace Tests\Browser;

use App\Models\Description;
use App\Models\Product;
use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AdminProductTest extends DuskTestCase
{
    /**
     * No image product page
     *
     * @return void
     */
    public function testProductNoImage()
    {
        $product = Product::find(1);
        $user = User::find(1);

        $this->browse(function (Browser $browser) use ($product, $user) {
            $browser->loginAs($user)
                ->visit('/admin/no-image')
                ->assertSee($product->name)
                ->clickLink($product->name)
                ->assertPathIs("/admin/product/{$product->url_name}")
                ->assertSee($product->name)
                ->assertSee('Sites with available prices:');
        });
    }

    /**
     * Making popular test
     *
     * @return void
     */
    public function testMakingPopular()
    {
        $product = Product::find(1);
        $user = User::find(1);

        $this->browse(function (Browser $browser) use ($product, $user) {
            $browser->loginAs($user)
                ->visit('/admin/most-popular')
                ->assertDontSee($product->name)
                ->visit("/admin/product/{$product->url_name}")
                ->assertSee($product->name)
                ->clickLink('Make popular?')
                ->assertPathIs("/admin/product/{$product->url_name}")
                ->assertSee('Remove from popular?')
                ->visit('/admin/most-popular')
                ->assertSee($product->name)
                ->visit('/most-popular')
                ->assertSee($product->short_model)
                ->visit("/admin/most-popular/{$product->url_name}/remove")
                ->visit('/most-popular')
                ->assertDontSee($product->name);
        });
    }

    public function testProductDescription()
    {
        $product = Product::find(1);
        $user = User::find(1);
        $content = 'This is test descrioption';

        $this->browse(function (Browser $browser) use ($product, $user, $content) {
            $browser->loginAs($user)
                ->visit("/admin/product/{$product->url_name}")
                ->assertSee($product->name)
                ->clickLink('Add/Edit Description')
                ->assertPathIs("/admin/descriptions/view/product/{$product->url_name}")
                ->assertSee('Descriptions')
                ->clickLink('+ Add description')
                ->assertPathIs("/admin/descriptions/create/product/{$product->id}");
        });
    }
}
