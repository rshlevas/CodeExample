<?php

namespace Tests\Browser;

use App\Models\Product;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class KeyProductsTest extends DuskTestCase
{
    /**
     * Test key product pages
     *
     * @dataProvider keyProductsDataProvider
     * @param string $name
     * @return void
     */
    public function testKeyProductsPage($name)
    {
        $product = Product::with('brand')
            ->take(1)
            ->where('name', 'like', "%$name%")
            ->get()
            ->first();

        $this->browse(function (Browser $browser) use ($product, $name) {
            $browser->visit("/most-popular/{$name}")
                    ->assertSee($product->brand->name)
                    ->assertSee($product->model)
                    ->assertSee($product->best_price->price);
        });
    }

    /**
     * Data provider for testKeyProductsPage
     *
     * @return array
     */
    public function keyProductsDataProvider()
    {
        return [
            ['iphone'],
            ['galaxy'],
        ];
    }
}
