<?php

namespace Tests\Browser;

use App\Models\Brand;
use App\User;
use Tests\Browser\Traits\AuthorizationTest;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AdminBrandSectionTest extends DuskTestCase
{
    use AuthorizationTest;

    /**
     * Data provider for AuthorizationTest
     *
     * @return array
     */
    public function AuthorizationLinksDataProvider()
    {
        return [
            ['/admin/brands'],
            ['/admin/brands/create'],
        ];
    }

    /**
     * Test brand section page
     *
     * @return void
     */
    public function testAdminBrandSection()
    {
        $user = User::find(1);
        $brand = Brand::find(1);

        $this->browse(function (Browser $browser) use ($user, $brand) {
            $browser->loginAs($user)
                ->visit('/admin/brands')
                ->assertSee('Brands section managing')
                ->assertSee($brand->name);
        });
    }

    /**
     * Test brand updating
     *
     * @return void
     */
    public function testAdminBrandUpdateSection()
    {
        $user = User::find(1);
        $brand = Brand::take(1)
            ->orderBy('name', 'asc')
            ->get()
            ->first();
        $otherBrand = Brand::take(2)
            ->orderBy('name', 'asc')
            ->get()
            ->last();
        $currentName = $brand->name;
        $newName = 'AAAAA';
        $otherBrandName = $otherBrand->name;

        $this->browse(function (Browser $browser) use ($user, $currentName, $newName, $otherBrandName, $brand) {
            $browser->loginAs($user)
                ->visit('/admin/brands')
                ->assertSee('Brands section managing')
                ->assertVisible('.icon-pencil')
                ->click('.icon-pencil')
                ->assertPathIs('/admin/brands/update/' . $brand->url_name)
                ->assertSee("Update Brand: ". ucfirst($currentName))
                ->type('name', $otherBrandName)
                ->press('Update')
                ->assertSee('The name has already been taken.')
                ->type('name', $newName)
                ->press('Update')
                ->assertPathIs('/admin/brands')
                ->assertSee($newName)
                ->click('.icon-pencil')
                ->type('name', $currentName)
                ->press('Update')
                ->assertSee($currentName);
        });
    }

    /**
     * Test brand creating validation
     *
     * @return void
     */
    public function testAdminBrandCreateValidationSection()
    {
        $user = User::find(1);
        $brand = Brand::find(1);
        $brandName = $brand->name;

        $this->browse(function (Browser $browser) use ($user, $brandName) {
            $browser->loginAs($user)
                ->visit('/admin/brands')
                ->assertSee('Brands section managing')
                ->clickLink('+ Add new brand')
                ->assertPathIs('/admin/brands/create')
                ->assertSee('Add Brand')
                ->type('name', $brandName)
                ->press('Create')
                ->assertSee('The name has already been taken.');
        });
    }

    /**
     * Test site creating
     *
     * @return void
     */
    public function testAdminBrandCreatingSection()
    {
        $user = User::find(1);
        $brandName = 'Novel';

        $this->browse(function (Browser $browser) use ($user, $brandName) {
            $browser->loginAs($user)
                ->visit('/admin/brands')
                ->assertSee('Brands section managing')
                ->clickLink('+ Add new brand')
                ->assertPathIs('/admin/brands/create')
                ->assertSee('Add Brand')
                ->type('name', $brandName)
                ->press('Create')
                ->assertPathIs('/admin/brands')
                ->assertSee($brandName);
        });
    }

    /**
     * Test site deleting
     *
     * @return void
     */
    public function testAdminBrandDeletingSection()
    {
        $user = User::find(1);
        $brand = Brand::where('name', 'Novel')->get()->first();

        $this->browse(function (Browser $browser) use ($user, $brand) {
            $browser->loginAs($user)
                ->visit('/admin/brands')
                ->assertSee('Brands section managing')
                ->assertSee($brand->name)
                ->visit("/admin/brands/delete/{$brand->url_name}")
                ->assertPathIs('/admin/brands')
                ->assertDontSee($brand->name);
        });
    }
}
