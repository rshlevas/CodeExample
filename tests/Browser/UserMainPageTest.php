<?php

namespace Tests\Browser;

use App\Models\Brand;
use App\Models\ProductPrice;
use App\Models\Site;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserMainPageTest extends DuskTestCase
{
    /**
     * User main page test.
     *
     * @return void
     */
    public function testExample()
    {
        $brand = Brand::find(1);
        $bestPrice = ProductPrice::take(1)
            ->orderBy('price', 'asc')
            ->orderBy('created_at', 'desc')
            ->get()
            ->first();

        $this->browse(function (Browser $browser) use ($brand, $bestPrice) {
            $browser->visit('/')
                    ->assertSee('MobileSearch')
                    ->assertSee($brand->name)
                    ->assertSee($bestPrice->productProfile->product->model);
        });
    }
}
